const express       = require('express')
const logger        = require('morgan')
const bodyParser    = require('body-parser')
const moment        = require('moment');
const cors = require('cors')


/**
 * ENV File
 */
 require('dotenv').config();


 /**
 * Express
 */
const app = express()


/**
 * CORS
 */
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", ["*"]);
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// app.use(cors({ origin: '*' }))

/**
 * Setting up
 */
 app.use(logger('dev'))
 app.use(bodyParser.urlencoded({extended: false}))
 app.use(bodyParser.json())


 /**
 * Database connection
 */
const connection = require('knex')({
    client: 'mysql',
    connection: {
        host        : process.env.DB_HOST,
        user        : process.env.DB_USER,
        password    : process.env.DB_PASS,
        database    : process.env.DB_NAME,
        //timezone    : 'GMT+08:00',
        timezone    : 'GMT+01:00',
        typeCast    : function (field, next) {
            if (field.type == 'DATETIME') {
                return moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
            }
            else if (field.type == 'DATE') {
                return moment(field.string()).format('YYYY-MM-DD');
            }
            return next();
        }
    },
    debug: true,
});

global.qb = connection;


/**
 * IMPORT ROUTES
 */
const user          = require('./routes/user.js');
const quiz          = require('./routes/quiz.js');
const admin          = require('./routes/admin.js');
const client          = require('./routes/client.js');

app.use(user);
app.use(quiz);
app.use(admin);
app.use(client);


app.get("/", (req, res) => {
    res.send("Root route!")
})

// Error handler
app.use((err, req, res, next) => {
    res.status(401).send(err + '')
})

module.exports = app

const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Listening on port ${port}...`))