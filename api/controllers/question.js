const utils = require('../utils/output');
const quizModel = require('../models/quiz');

var Controller = {
    getQuestion: async function (req, res) {
        let id  = req.query.id;
        let queryOutput = {};

        try{
            queryOutput.question  = await quizModel.getQuiz(id);
        } catch{
            sendResponseError(req, res);
        }

        if(queryOutput.question.hasOwnProperty('data')){
            if(queryOutput.question.data){
                queryOutput.questions   = await quizModel.getQuestions(null, id);
            }
        }
        
        utils.controllerOutput(req, res, queryOutput);
    },

    getQuestions: async function (req, res) {
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	{ column: req.query.orderBy, direction: req.query.orderDirection },
            filter: 	{ keyword: req.query.filter, columns: ['name'] },
            limit: 		req.query.limit,
            offset: 	req.query.offset,
        }
        
        try{
            queryOutput.questions = await quizModel.getQuestions(dataTableOptions);
        } catch{
            sendResponseError(req, res);
        }

        queryOutput.questions.forEach((question)=> {
            question.choices = await questionModel.getChoices();
        })

        utils.controllerOutput(req, res, queryOutput);
    },
    
}

module.exports = Controller;