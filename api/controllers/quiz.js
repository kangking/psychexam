const { controllerOutput, sendResponseError } = require('../utils/output');
const { clientDetailsFromAccessToken } = require('../utils/user');
const quizModel = require('../models/quiz');
const answerSheetModel = require('../models/answerSheet');
const questionModel = require('../models/question');

const { getChoices } = require('../models/question');
const {
    analyzeQuizAnswers,
    eliminateDuplicateAnswer,
    interpretationRandomizer,
    personalityA,
    personalityB,
    personalityC,
    iqTestA } = require('../utils/quiz');

var Controller = {
    getQuizSet: async function (req, res) {
        let quizSetId  = req.query.quizSetId;
        let queryOutput = {};

        try{
            queryOutput.quizSets = await quizModel.getQuizSets({id: quizSetId});
        } catch (error) {
            sendResponseError(req, res, {error, message: "Unable to fetch quiz set: " + error});
        }

        controllerOutput(req, res, queryOutput);
    },

    // Checks and requests permission
    checkClientQuizSet: async function (req, res) {
        let quizSetId  = req.query.quizSetId;
        let queryOutput = {};
        let checkRequestExist = false;
        let checkExist = false;

        let { id: clientId } = await clientDetailsFromAccessToken(req);

        try {
            let request = await quizModel.getClientQuizSetRequests({clientId, quizSetId});
            console.log(request);
            checkRequestExist = request.rowcount > 0 ? true : false;
        } catch (error) {
            sendResponseError(req, res, {error, message: "Unable to fetch quiz set request row: " + error});
        }

        try {
            let clientQuizSet = await quizModel.checkClientQuizSet({clientId, quizSetId});
            checkExist = clientQuizSet.rowcount > 0 ? true : false;
        } catch (error) {
            sendResponseError(req, res, {error, message: "Unable to fetch quiz set: " + error});
        }

        if (!checkRequestExist && !checkExist) {
            try {
                let payload = {
                    fk_client_id: clientId,
                    fk_quiz_set_id: quizSetId
                };
                queryOutput.permission = await quizModel.addClientQuizSetRequest(payload);
            } catch (error) {
                sendResponseError(req, res, {error, message: "Could not request for permission: " + error});
            }
        }

        queryOutput.allowAccess = checkExist;
        controllerOutput(req, res, queryOutput);
    },

    getClientQuizSet: async function (req, res) {
        let quizSetId  = req.query.quizSetId;
        let queryOutput = {};
        let rawQuizSets = null;

        let { id: clientId } = await clientDetailsFromAccessToken(req);

        let filter = {
            quizSetId: quizSetId,
            clientId, clientId
        };

        try{
            rawQuizSets = await quizModel.getClientQuizSet(filter);
            console.log('test', rawQuizSets)
            if (rawQuizSets.data){
                let quizSets = [];
                
                for(let i=0; i<rawQuizSets.data.length; i++){
                    let rawSet = rawQuizSets.data[i];

                    let tempQuizSet = {
                        "client_quiz_set_id": rawSet.client_quiz_set_id,
                        "fk_quiz_set_id": rawSet.fk_quiz_set_id,
                        "fk_client_id": rawSet.fk_client_id,

                        "quiz_set_id": rawSet.quiz_set_id,
                        "quiz_set_name": rawSet.quiz_set_name,
                        "quiz_set_items": []
                    };

                    let found = quizSets.find((set) => {
                        return rawSet.quiz_set_id === set.quiz_set_id;
                    });

                    let tempQuizSetItem = {
                        "quiz_set_quiz_id": rawSet.quiz_set_quiz_id,
                        "fk_quiz_id": rawSet.fk_quiz_id,
                        "date_time_start": rawSet.date_time_start,
                        "date_time_end": rawSet.date_time_end,
                        
                        "quiz_id": rawSet.quiz_id,
                        "quiz_name": rawSet.quiz_name,
                        "quiz_instruction": rawSet.quiz_instruction,
                        "quiz_slug": rawSet.quiz_slug,
                        "quiz_duration": rawSet.quiz_duration,
                        "quiz_is_enabled": rawSet.quiz_is_enabled,

                        "client_answer_sheet_id": null,
                        "client_answer_sheet_value": null,
                        "client_answer_sheet_is_done": null,
                        "client_answer_sheet_is_deleted": null,
                    }

                    try {
                        answerSheet = await answerSheetModel.getAnswerSheet({quizSetItemId: rawSet.quiz_set_quiz_id});
                        console.log({answerSheet});
                        if (answerSheet.data) {
                            tempQuizSetItem["client_answer_sheet_id"] = answerSheet.data.client_answer_sheet_id;
                            tempQuizSetItem["client_answer_sheet_value"] = answerSheet.data.client_answer_sheet_value;
                            tempQuizSetItem["client_answer_sheet_is_done"] = answerSheet.data.client_answer_sheet_is_done;
                            tempQuizSetItem["client_answer_sheet_is_deleted"] = answerSheet.data.client_answer_sheet_is_deleted;
                        }
                    } catch (error){
                        console.log(error);
                    }

                    if (found) {
                        found.quiz_set_items.push(tempQuizSetItem);
                    } else {
                        tempQuizSet.quiz_set_items.push(tempQuizSetItem);
                        quizSets.push(tempQuizSet);
                    }
                }

                queryOutput.clientQuizSets = {data: quizSets};
            } else {
                queryOutput.clientQuizSets = rawQuizSets;
            }

        } catch (error) {
            sendResponseError(req, res, {error, message: "Unable to fetch quiz set: "+error});
        }
        
        // if(queryOutput.quizSets.hasOwnProperty('data')){
        //     queryOutput.quizSets.data.forEach((quizSet, index) => {

        //     });
        // }

        controllerOutput(req, res, queryOutput);
    },

    getClientQuiz: async function (req, res) {
        //quiz, answersheet, is_done
    },

    getQuiz: async function (req, res) {
        let id  = req.query.id;
        let slug  = req.query.slug;
        let quizSetItemId = Number(req.query.quizSetItemId);

        let canTake = false;
        let message = '';
        let queryOutput = {};
        let questions = null;
        
        let { id: clientId }    = await clientDetailsFromAccessToken(req);

        // Check Quiz if allowed during this time
        // Must this section
        try{
            quiz = await quizModel.getQuizSetItem({quizSetItemId});

            if(quiz.data){
                let qData = quiz.data;
                let currDate = new Date();
                message = 'You can not take this test. Wait for the assigned schedule.';
                
                if (qData.date_time_start != null) {
                    if (currDate.getTime() >= qData.date_time_start.getTime()){
                        canTake = true;
                        message = 'You can now take the test.';

                        if (qData.date_time_end != null || true) {
                            if (currDate.getTime() <= qData.date_time_end.getTime()){
                                canTake = true;
                            } else {
                                canTake = false;
                                message = 'You can no longer take this test.';
                            }
                        }
                    } else {
                        canTake = false;
                    }
                }
            }

        } catch (error) {
            sendResponseError(req, res, {error, message: 'Failed to check quiz schedule. ' + error});
        }
        // End of checking section
        
        if (canTake) {
            queryOutput.quiz  = await quizModel.getQuiz({id: id, slug: slug});
            
            if(queryOutput.quiz.hasOwnProperty('data')){
                if(queryOutput.quiz.data){

                    //single result
                    if(queryOutput.quiz.data.quiz_id){
                        let { questions: q, answerSheet: as } = await insertAnswersInEachQuestions({quizData: queryOutput.quiz.data, clientId: clientId, quizSetItemId: quizSetItemId});

                        questions = q;
                        queryOutput.answerSheet = as;
                    }
                    
                    //multiple result
                    if(Array.isArray(queryOutput.quiz.data)){
                        for(i=0; i<queryOutput.quiz.data.length; i++) {

                            quiz = queryOutput.quiz.data[i];
                            quiz.quiz_is_done = false;

                            answerSheet = await answerSheetModel.getAnswerSheet({quizSetItemId: quizSetItemId, clientId: clientId });

                            if (answerSheet.hasOwnProperty('data')) {
                                if (answerSheet.data) {
                                    quiz.quiz_is_done = answerSheet.data.client_answer_sheet_is_done;
                                }
                            }

                        };
                    }
                }
            }

            if(id != null && id != '' || slug != null && slug != ''){
                queryOutput.questions = await appendChoicesToQuestions(questions.data);
            }
        }

        queryOutput.canTake = canTake;
        controllerOutput(req, res, queryOutput);
    },

    checkQuizSchedule: async function (req, res) {
        let quizSetItemId = req.query.quizSetItemId;

        let queryOutput = {};
        let message = 'Server error';
        let success = false;
        let canTake = false;
        let quiz = null;

        try{
            quiz = await quizModel.getQuizSetItem({quizSetItemId});

            if(quiz.data){
                let qData = quiz.data;
                let currDate = new Date();
                message = 'You can not take this test. Wait for the assigned schedule.';
                
                if (qData.date_time_start != null) {
                    if (currDate.getTime() >= qData.date_time_start.getTime()){
                        canTake = true;
                        message = 'You can now take the test.';

                        if (qData.date_time_end != null || true) {
                            if (currDate.getTime() <= qData.date_time_end.getTime()){
                                canTake = true;
                            } else {
                                canTake = false;
                                message = 'You can no longer take this test.';
                            }
                        }
                    } else {
                        canTake = false;
                    }
                }

                success = true;
            }

            queryOutput.quizSchedule = {
                data: quiz.data,
                canTake: canTake,
                message: message,
                success: success
            };

            controllerOutput(req, res, queryOutput);

        } catch (error) {
            sendResponseError(req, res, {error, message: 'Failed to check quiz schedule. ' + error});
        }
    },

    addQuiz: async function (req, res) {
        let data                = req.body;
        let queryOutput         = {};

        let quizPayload = {
            "quiz_name": data.name,
            "quiz_instruction": data.instruction,
            "quiz_slug": data.slug,
            "quiz_duration": data.duration
        };

        queryOutput.quiz  = await quizModel.addQuiz(quizPayload);

        let quizId = queryOutput.quiz.data;
        
        if(data.questions.length > 0){
            let questions = data.questions;

            for(let i=0; i < questions.length; i++){
                let points = 1;
                if(questions[i].hasOwnProperty("points")){
                    points = questions[i].points
                }

                let questionPayload = {
                    "fk_quiz_id": quizId,
                    "question_value": questions[i].question,
                    "question_answer": questions[i].answer,
                    "question_number": questions[i].number,
                    "question_points": points,
                }

                queryOutput.questions = queryOutput.questions ? queryOutput.questions : [];
                queryOutput.questions.push(await questionModel.addQuestion(questionPayload));

                let questionId = queryOutput.questions[i].data;
                
                let metaPayload = {
                    "fk_question_id": questionId,
                    "question_meta_type":  questions[i].hasOwnProperty("factor") ? "factor" : "",
                    "question_meta_value": questions[i].hasOwnProperty("factor") ? questions[i].factor : "",
                }
                
                queryOutput.questions[i].meta = await questionModel.addQuestionMeta(metaPayload);

                if(questions[i].choices.length > 0){
                    let choices = questions[i].choices;

                    for(let j=0; j < choices.length; j++){
                        let choicePayload = {
                            "fk_question_id": questionId,
                            "choice_legend": choices[j].legend,
                            "choice_value": choices[j].answer,
                            "choice_points": choices[j].hasOwnProperty("points") ? choices[j].points : null
                        }

                        queryOutput.questions[i].choices = queryOutput.questions[i].choices ? queryOutput.questions[i].choices : [];
                        queryOutput.questions[i].choices.push(await questionModel.addChoice(choicePayload));
                        
                        let choiceId = queryOutput.questions[i].choices[j].data;

                        if(data.questions[i].answer === choices[j].legend){
                            let answerPayload = {
                                "fk_question_id": questionId,
                                "fk_choice_id": choiceId,
                            };
                            
                            queryOutput.questions[i].answer = await questionModel.addAnswer(answerPayload);
                        }
                    }
                }
            }
        }
        
        controllerOutput(req, res, queryOutput);
    },

    submitQuiz: async function (req, res) {
        let data                = req.body;
        let queryOutput         = {};
        let messageOutput       = null
        let checkAnswerSheetExistsAndDone = null;
        let { id: clientId }    = await clientDetailsFromAccessToken(req);

        console.log('submitQuiz ClientID', clientId, typeof clientId);

        data.answers = eliminateDuplicateAnswer(data.answers);

        let quiz = await quizModel.getQuiz({slug: data.quizSlug});
        let quizSetItemId = data.quizSetItemId;

        let answerSheetPayload = {
            client_answer_sheet_value: JSON.stringify(data),
            client_answer_sheet_is_done: data.isDone,
            fk_client_id: clientId,
            fk_quiz_set_quiz_id: quizSetItemId
        };
        
        try{
            checkAnswerSheetExistsAndDone = await answerSheetModel.getAnswerSheet({quizSetItemId: quizSetItemId, clientId: clientId})
        } catch(error){
            sendResponseError(req, res, {error, message: "Unable to check answer sheet in database: " + error});
            // return res.status(401).send({
            //     success: false,
            //     message: 'Unable to check answer sheet in database.',
            // });
        }
        
        if(checkAnswerSheetExistsAndDone){
            if(checkAnswerSheetExistsAndDone.hasOwnProperty('data')){
                if(checkAnswerSheetExistsAndDone.data){
                    if(Array.isArray(checkAnswerSheetExistsAndDone.data)){
                        if(checkAnswerSheetExistsAndDone.data.length > 0){
                            sendResponseError(req, res, {message: "Avoid duplicate entry of answer sheet"});
                            // return res.status(401).send({
                            //     success: false,
                            //     message: 'Avoid duplicate entry of answer sheet.',
                            // });
                        }
                    }
                    else{
                        if(checkAnswerSheetExistsAndDone.data.hasOwnProperty('client_answer_sheet_id')){
                            sendResponseError(req, res, {message: "Avoid duplicate entry of answer sheet"});
                            // return res.status(401).send({
                            //     success: false,
                            //     message: 'Avoid duplicate entry of answer sheet.',
                            // });
                        }
                    }
                }
            }
        }

        try{
            await answerSheetModel.addAnswerSheet(answerSheetPayload);
        } catch(err){
            sendResponseError(req, res, {message: "Something wrong with the answersheet"});
            // return res.status(401).send({
            //     access: false,
            //     success: false,
            //     message: 'Something wrong with the answersheet',
            // });
        }
        
        controllerOutput(req, res, queryOutput, messageOutput);
    },

    analyzeQuizResult: async function(req, res){
        let data                = req.body;
        let queryOutput         = {};
        let messageOutput       = null
        
        try {
            let analysis = await analyzeQuizAnswers(data);
            queryOutput.interpretations = analysis.result;
            queryOutput.details = analysis.details;
        } catch (error) {
            sendResponseError(req, res, {message: 'Cannot analyze result.', error})
        }
        
        controllerOutput(req, res, queryOutput, messageOutput);
    }
}

async function appendChoicesToQuestions(questionList){
    let result = [];
    let questions = questionList;

    for(let i=0; i<questions.length; i++){
        let question = questions[i];
        let payload = {...question}

        let choices = await questionModel.getChoices({questionId: question.question_id});

        payload.choices = choices.data;
        result.push(payload);
    }

    return new Promise ((resolve,reject) => {
        resolve(result)
        reject(false)
    })
}

const insertAnswersInEachQuestions = async ({quizData, clientId, quizSetItemId}) => {
    quizData.quiz_is_done = false;
    
    questions = await questionModel.getQuestions({quizId: quizData.quiz_id});
    answerSheet = await answerSheetModel.getAnswerSheet({clientId: clientId, quizSetItemId: quizSetItemId });
    
    if (answerSheet.hasOwnProperty('data')) {
        
        if (answerSheet.data) {
            let rawAnswerSheet = answerSheet.data.client_answer_sheet_value;
            let answers = JSON.parse(unescape(rawAnswerSheet));

            quizData.quiz_is_done = true;
            
            questions.data.forEach((question, index) => {
                if (answers.answers){
                    question.answer = null;   
                    answers.answers.forEach((answer) => {
                        if (answer.number == question.question_id){
                            question.answer = answer.value;   
                        }
                        question.question_number2 = index + 1;
                        // console.log(answer.number, '=', question.question_id, answer.number == question.question_id, '-------', question.answer);
                    })
                }
                else {
                    question.answer = null;    
                    question.question_number2 = index + 1;
                } 
            });
        }
        else {
            questions.data.forEach((question, index) => {
                question.answer = null;    
                question.question_number2 = index + 1;
            });
        }
    }
    else {
        questions.data.forEach((question, index) => {
            question.answer = null;    
            question.question_number2 = index + 1;
        });
    }

    return new Promise((resolve, reject) => {
        resolve({questions, answerSheet});
        reject(new Error('yeah'));
    });
}

module.exports = Controller;