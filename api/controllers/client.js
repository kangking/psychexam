const { controllerOutput, sendResponseError } = require('../utils/output');
const { getDateNow } = require('../utils/common');
const { generateToken } = require('../utils/jwt');
const { clientDetailsFromAccessToken } = require('../utils/user');

const moment        = require('moment');
const bcrypt        = require('bcrypt');
const Mailer        = require('../mailer');
const saltRounds    = 10;

const clientModel = require('../models/client');
const quizModel = require('../models/quiz');
const sessionModel = require('../models/session');
const adminModel = require('../models/admin');
const answerSheetModel = require('../models/answerSheet');

var Controller = {
    login: async function (req, res){
        let data = req.body;
        let queryOutput = {};
        let client = null;

        let accessCode = await adminModel.getAccessCode(null, data.accessCode);

        if(!accessCode.data) res.send({
            message: "Invalid cccess code.",
            success: false,
        })

        try{
            client = await clientModel.getClient({email: data.email})
        } catch{
            res.send({
                message: "Email not found.",
                success: false,
            })
        }

        if(!client && !client.data) {
            res.send({
                message: "Email not found.",
                success: false,
            })
        }

        if(client.data == undefined || client.data == null) {
            res.send({
                message: "Email not found.",
                success: false,
            })
        } else {
            if(Array.isArray(client.data)) client.data = client.data[0];
            let payload = { accountType: 'client', 
                id: client.data.client_id, 
                firstName: client.data.client_first_name, 
                lastName: client.data.client_last_name, 
                email: client.data.client_email, 
                timeStamp: getDateNow(),
                accountType: 'client'
            };

            queryOutput.client = client;
            queryOutput.accessToken = await generateToken(payload);
        }

        controllerOutput(req, res, queryOutput);
    },

    getClientTable: async function (req, res) {
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }

        try {
            queryOutput.client = await clientModel.getClientTable(dataTableOptions);
        } catch (error) {
            sendResponseError(req, res, {error, message: 'Something went wrong', status: 400});
        }

        controllerOutput(req, res, queryOutput);
    },

    getClient: async function (req, res) {
        let queryOutput = {};

        let { id: clientId } = await clientDetailsFromAccessToken(req);
        queryOutput.client  = await clientModel.getClient({id: clientId});

        controllerOutput(req, res, queryOutput);
    },

    addClient: async function (req, res) {
        let data                = req.body;
        let queryOutput         = {client:{}};
        let message             = '';
        let success             = true;

        let encryptedPassword   = await bcrypt.hash(data.password, saltRounds);

        let clientPayload = {
            "client_first_name": data.firstName,
            "client_last_name": data.lastName,
            "client_middle_name": data.middleName,
            "client_email": data.email.toLowerCase(),
            "client_mobile_number": data.mobileNumber,
            "client_birthdate": data.dateOfBirth,
            "client_gender": data.gender,
            "client_civil_status": data.civilStatus,
            "client_address_line": data.homeAddress,
            "client_password": encryptedPassword
        };

        let client = {};
        let accessCode = null;
        
        try{
            client = await clientModel.getClient({email:data.email});
        } catch(error) {
            sendResponseError(req, res, {error, message: "Server error: Unable to check email. Please Retry."});
        }

        if (client.hasOwnProperty('data') && client.data) {
            if(client.data.client_id) {
                if(!client.data.client_is_deleted) {
                    sendResponseError(req, res, {message: "Email already exists"});
                }
            }
        } else {

            // Access Code
            try {
                accessCode = await adminModel.getAccessCode(null, data.accessCode);
            } catch (error) {
                sendResponseError(req, res, {error, message: "Server error: Unable to check Access Code. Please Retry."});
            }
            
            if(accessCode.data){
                
                let accessCodeId = accessCode.data.access_code_id;
                // let quizSetId = accessCode.data.fk_quiz_set_id;
                
                if(accessCodeId){
                    let clientId = null;

                    try {
                        clientId = await clientModel.addClient(clientPayload);
                        clientId = clientId.data;
                    } catch (error) {
                        sendResponseError(req, res, {error, message: "Server error: Unable to check Access Code. Please Retry."});
                    }

                    if(clientId){
                        queryOutput.clientId = clientId;
                        queryOutput.client.data = clientPayload;
                        queryOutput.client.data.client_id = clientId;
                        queryOutput.accessToken = await generateToken({accountType: 'client', id: clientId, firstName: data.firstName, lastName: data.lastName, email: data.email, timeStamp: getDateNow()});
                        queryOutput.code = await adminModel.updateAccessCode({"access_code_is_valid": false}, accessCodeId);
                        
                        // CLIENT QUIZ SET
                        // let clientQuizSetPayload = {
                        //     fk_client_id: clientId,
                        //     fk_quiz_set_id: quizSetId
                        // }
                        // queryOutput.clientQuizSet = await quizModel.addClientQuizSet(clientQuizSetPayload)

                        // SESSION
                        sessionData = {
                            session_client_user_agent: req.headers['user-agent'],
                            session_client_token: queryOutput.accessToken,
                            session_client_login_timestamp: new Date(),
                            session_client_ip_address: req.headers['x-forwarded-for'] || process.env.DUMMYIP,
                            fk_client_id: clientId
                        }

                        await sessionModel.addClientSession(sessionData);
                    }
                    else{
                        message = 'Invalid form/data';
                        success = false;
                    }
                }
                
                else{
                    message = 'Invalid Code';
                    success = false;
                }
            }

            else{
                message = 'Invalid Code';
                success = false;
            }
        }

        
        
        controllerOutput(req, res, queryOutput, message, success);
    },

    getClientAnswerSheet: async function (req, res) {
        let data = req.query;
        let queryOutput = {};

        let { id: clientId } = await clientDetailsFromAccessToken(req);

        try{
            queryOutput.answerSheet = await answerSheetModel.getAnswerSheet({clientId: clientId, quizId: data.quizId});
        } catch{
            sendResponseError(req, res);
        }
        
        controllerOutput(req, res, queryOutput);
    },

    checkClientAnswerSheetExistandDone: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let answerSheet = null;
        let isDone = false;
        let isExist = false;

        let { id: clientId } = await clientDetailsFromAccessToken(req);

        try{
            answerSheet = await answerSheetModel.getAnswerSheet({clientId: clientId, quizId: data.quizId});
        } catch{
            sendResponseError(req, res);
        }

        if(answerSheet){
            if(answerSheet.hasOwnProperty('data')) {
                isExist = true;
                if(answerSheet.data){
                    isDone = answerSheet.data.client_answer_sheet_is_done;
                }
            }
        }

        queryOutput.answerSheetStatus = {isDone, isExist};
        
        controllerOutput(req, res, queryOutput);
    },

    deleteClientAnswerSheet: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let isDeleted = false;
        let deleted = null;

        console.log(data);

        let payload = {
            client_answer_sheet_deleted_at: new Date(),
            client_answer_sheet_is_deleted: true
        };

        try{
            deleted = await answerSheetModel.updateAnswerSheet(payload, {answerSheetId: data.answerSheetId});
        } catch{
            res.send({
                success: false,
                status: 400,
                message: "Server error.",
            });
        }

        if(deleted){
            if(deleted.hasOwnProperty('data')) {
                if(deleted.data)
                    isDeleted = true;
            }
        }

        queryOutput.deleted = deleted;
        queryOutput.deleteStatus = {isDeleted};
        controllerOutput(req, res, queryOutput);

    },




    /**
     * For Admin Calls
     */

    getClientByQuizSetTable: async function (req, res) {
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        let options = JSON.parse(req.query.options);
        let results = null;

        try {
            results = await clientModel.getClientByQuizSetTable(dataTableOptions, options);
        } catch (error) {
            sendResponseError(req, res, {error, message: 'Something went wrong', status: 400});
        }

        let tempClientList = [];

        const quizTakenTemplate = (client) => {
            return {
                client_answer_sheet_created_at: client.client_answer_sheet_created_at,
                client_answer_sheet_created_by: client.client_answer_sheet_created_by,
                client_answer_sheet_deleted_at: client.client_answer_sheet_deleted_at,
                client_answer_sheet_deleted_by: client.client_answer_sheet_deleted_by,
                client_answer_sheet_id: client.client_answer_sheet_id,
                client_answer_sheet_is_deleted: client.client_answer_sheet_is_deleted,
                client_answer_sheet_is_done: client.client_answer_sheet_is_done,
                client_answer_sheet_result: client.client_answer_sheet_result,
                client_answer_sheet_updated_at: client.client_answer_sheet_updated_at,
                client_answer_sheet_updated_by: client.client_answer_sheet_updated_by,
                client_answer_sheet_value: client.client_answer_sheet_value,
                date_time_end: client.date_time_end,
                date_time_start: client.date_time_start,
                quiz_duration: client.quiz_duration,
                quiz_id: client.quiz_id,
                quiz_instruction: client.quiz_instruction,
                quiz_is_enabled: client.quiz_is_enabled,
                quiz_name: client.quiz_name,
                quiz_slug: client.quiz_slug,
                quiz_set_id: client.quiz_set_id,
            }
        }
        
        results.data.forEach((client) => {
            if (tempClientList.length > 0) {
                let found = tempClientList.find((tempClient) => {
                    return tempClient.client_id == client.client_id;
                })
    
                if (found) {
                    let payload = quizTakenTemplate(client);
                    found.client_quizzes_taken.push(payload)
                } else {
                    let payload = {
                        ...client,
                        client_quizzes_taken: [quizTakenTemplate(client)]
                    }
                    tempClientList.push(payload);
                }
            } else {
                let payload = {
                    ...client,
                    client_quizzes_taken: [quizTakenTemplate(client)]
                }
                tempClientList.push(payload);
            }
        })

        queryOutput.client = results;
        queryOutput.client.data = tempClientList;

        controllerOutput(req, res, queryOutput);
    },
    
    getClientAsAdmin: async function (req, res) {
        let queryOutput = {};

        queryOutput.client  = await clientModel.getClient({id: req.query.clientId});
        controllerOutput(req, res, queryOutput);
    },

    getClientQuizAsAdmin: async function (req, res) {
        let queryOutput = {};
        let filter = {
            clientId: req.query.clientId,
            quizSetId: req.query.quizSetId
        }

        queryOutput.answerSheet  = await answerSheetModel.getAnswerSheetAndQuiz(filter);

        if(queryOutput.answerSheet.data){
            if(Array.isArray(queryOutput.answerSheet.data)){
                if(queryOutput.answerSheet.data.length > 0) {
                    queryOutput.answerSheet.data.forEach(item => {
                        item.client_answer_sheet_value = JSON.parse(unescape(item.client_answer_sheet_value));
                    })
                }
            }
        }

        controllerOutput(req, res, queryOutput);
    },
}

module.exports = Controller;