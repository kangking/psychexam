const utils = require('../utils/output');
const quizModel = require('../models/quiz');

var Controller = {
    getQuiz: async function (req, res) {
        let user = await utils.fetchUserDetails(req);
        let id  = req.query.id;
        let queryOutput = {};

        queryOutput.quiz  = await customerModel.getQuiz(id);
        if(queryOutput.quiz.hasOwnProperty('data')){
            if(queryOutput.quiz.data){
                queryOutput.questions   = await customerModel.getQuestions(null, id);
            }
        }
        
        utils.controllerOutput(req, res, queryOutput);
    },
}

module.exports = Controller;