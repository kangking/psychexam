const bcrypt        = require('bcrypt');
const otpGenerator = require('otp-generator')
const saltRounds    = 10;
const Mailer        = require('../utils/mailer');
require('dotenv').config();

const { generateToken } = require('../utils/jwt');
const { controllerOutput, sendResponseError } = require('../utils/output');
const { clientDetailsFromAccessToken } = require('../utils/user');
const adminModel = require('../models/admin');
const quizModel = require('../models/quiz');
const clientModel = require('../models/client');
const answerSheetModel = require('../models/answerSheet');

const {
    analyzeQuizAnswers,
    eliminateDuplicateAnswer,
    interpretationRandomizer,
    personalityA,
    personalityB,
    personalityC,
    iqTestA } = require('../utils/quiz');

var Controller = {
    login: async function(req, res){
        let data        = req.body;
        let queryOutput = {};
        let found       = null;
        let newToken    = null;
        
        try{
            queryOutput.login   = await adminModel.searchAdminAccount(data.username);
            found = queryOutput.login.data;
        } catch(error) {
            sendResponseError(req, res, {message: 'Something wrong', error});
        }

        if(found != null){
            compareHash = await bcrypt.compare(data.password, found.admin_account_password);
            message     = compareHash ? 'Correct password' : 'Incorrect password';

            if(compareHash){
                delete found.admin_account_password;
                delete found.admin_account_id;

                token       = await generateToken({accountType: 'admin', username: found.admin_account_username});
                //saveToken   = await user.update({token: token}, found.username);

                res.send({
                    message: message,
                    admin: {username: found.admin_account_username},
                    accessToken: token,
                    success: true,
                    status: 200,
                    statusText: "OK"
                });
            }
            else{
                res.send({
                    message: message,
                    success: false,
                    status: 400,
                    statusText: "OK"
                });
            }
        }
        else{
            res.send({
                message: "User does not exist.",
                success: false,
                status: 400,
                statusText: "OK"
            });
        }
        
    },

    getAdmin: async function (req, res) {
        let queryOutput = {};

        let { id: adminId } = await clientDetailsFromAccessToken(req);
        queryOutput.admin  = await adminModel.getAdmin({id: adminId});

        controllerOutput(req, res, queryOutput);
    },

    addAdminAccount: async function(req, res){
        let data = req.body;
        let queryOutput = {};

        let hash = await bcrypt.hash(data.password, saltRounds);
        let payload = {
            admin_account_username: data.username,
            admin_account_password: hash
        }

        queryOutput.account = adminModel.addAdminAccount(payload);
        controllerOutput(req, res, queryOutput, '');

    },

    getAccessCodeTable: async function (req, res) {
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        console.log(dataTableOptions.orderBy, req.query.orderBy);

        queryOutput.accessCodes  = await adminModel.getAccessCodeTable(dataTableOptions);

        controllerOutput(req, res, queryOutput);
    },

    addAccessCode: async function(req, res){
        const size = req.body.quantity ? req.body.quantity : 1;
        const length = process.env.ACCESS_CODE_LENGTH;
        
        let queryOutput = {};
        let codes = [];

        for(i=0; i<size; i++) {
            let code = {
                access_code_value: Number(otpGenerator.generate(length, { upperCase: false, specialChars: false })),
                access_code_created_at: new Date()
            };
            codes.push(code);
        }

        queryOutput.codes = await adminModel.addAccessCode(codes);
        controllerOutput(req, res, queryOutput, '');
    },

    deleteAccessCode: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let isDeleted = false;
        let deleted = null;

        let payload = {
            access_code_deleted_at: new Date(),
            access_code_is_deleted: true
        };

        try{
            deleted = await adminModel.updateAccessCode(payload, data.accessCodeId);
        } catch{
            res.send({
                success: false,
                status: 400,
                message: "Server error.",
            });
        }

        if(deleted){
            if(deleted.hasOwnProperty('data')) {
                if(deleted.data)
                    isDeleted = true;
            }
        }

        queryOutput.deleted = deleted;
        queryOutput.deleteStatus = {isDeleted};
        controllerOutput(req, res, queryOutput);
    },

    getLogoFile: async function(req, res){
        let data = req.body;
        let queryOuput = {};
        let filepath = 'logo.png';
        let root = 'assets';

        // const r = fs.createReadStream('../assets/logo.png') // or any other way to get a readable stream
        // const ps = new stream.PassThrough() // <---- this makes a trick with stream error handling
        // stream.pipeline(
        //     r,
        //     ps, // <---- this makes a trick with stream error handling
        //     (err) => {
        //     if (err) {
        //     console.log(err) // No such file or any other kind of error
        //     return res.sendStatus(400); 
        //     }
        // })
        // ps.pipe(res) // <---- this makes a trick with stream error handling

        res.sendFile(filepath, {root: root});
    },

    getExamTable: async function(req, res){
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        console.log(dataTableOptions.orderBy, req.query.orderBy);

        queryOutput.accessCodes  = await adminModel.getAccessCodeTable(dataTableOptions);

        controllerOutput(req, res, queryOutput);
    },

    getQuizSetTable: async function(req, res){
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        console.log(dataTableOptions.orderBy, req.query.orderBy);

        queryOutput.quizSet  = await adminModel.getQuizSetTable(dataTableOptions);

        controllerOutput(req, res, queryOutput);
    },

    addQuizSet: async function(req, res){
        let queryOutput = {};
        
        let payload = {
            quiz_set_name: req.body.name
        };

        try{
            queryOutput.quizSet = await adminModel.addQuizSet(payload);
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to add quiz set to database.', error})
        }
        controllerOutput(req, res, queryOutput, '');
    },

    updateQuizSet: async function (req, res){
        let data = req.body;
        let queryOutput = {};

        let payload = {
            quiz_set_name: data.name,
            quiz_set_updated_at: new Date()
        };

        try{
            queryOutput.quizSet = await adminModel.updateQuizSet(payload, {quizSetId: data.quizSetId});
        } catch (error){
            sendResponseError(req, res, {message: 'Unable to updated quiz set', error});
        }

        controllerOutput(req, res, queryOutput);

    },

    getQuizSet: async function (req, res) {
        let id  = req.query.quizSetId;
        let queryOutput = {};
        
        try{
            queryOutput.quizSet  = await adminModel.getQuizSet({id: id});
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to fetch quiz set from database.', error})
        }

        // try{
        //     queryOutput.quizItems  = await adminModel.getQuizSetQuizItems({quizSetId: id});
        // } catch (error) {
        //     sendResponseError(req, res, {message: 'Unable to fetch quiz set from database.', error})
        // }

        controllerOutput(req, res, queryOutput);
    },

    getQuizSetQuizItems: async function (req, res) {
        let id  = req.query.examSetId;
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        
        try{
            queryOutput.quiz  = await adminModel.getQuizSetQuizItemsTable({quizSetId: id});
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to fetch quiz set from database.', error})
        }

        controllerOutput(req, res, queryOutput);
    },

    getQuizTable: async function(req, res){
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        console.log(dataTableOptions.orderBy, req.query.orderBy);

        queryOutput.quiz  = await adminModel.getQuizTable(dataTableOptions);

        controllerOutput(req, res, queryOutput);
    },

    //maybe ununsed
    getQuizSetQuizTable: async function(req, res){
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        console.log(dataTableOptions.orderBy, req.query.orderBy);

        try{
            queryOutput.quiz  = await adminModel.getQuizSetQuizItemsTable(dataTableOptions);
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to get quiz set items table to database.', error})
        }

        controllerOutput(req, res, queryOutput);
    },

    addQuizSetQuizItems: async function(req, res){
        let queryOutput = {};
        let checkExist = false;
        
        let payload = {
            fk_quiz_id: req.body.quizId,
            fk_quiz_set_id: req.body.quizSetId
        };

        try {
            let params = {
                quizId: req.body.quizId,
                quizSetId: req.body.quizSetId
            };
            let quizItem = await adminModel.getQuizSetQuizItem(params);
            checkExist = quizItem.rowcount > 0 ? true : false;
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to check quiz item from database.', error});
        }

        if (!checkExist) {
            try {
                queryOutput.quizSet = await adminModel.addQuizSetQuizItems(payload);
            } catch (error) {
                sendResponseError(req, res, {message: 'Unable to add quiz set to database.', error})
            }
        } else {
            sendResponseError(req, res, {message: 'Quiz item already exist in the set'})
        }
        controllerOutput(req, res, queryOutput, '');
    },

    deleteQuizSet: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let quizList = null;

        let payloadQuizSet = {
            quiz_set_deleted_at: new Date(),
            quiz_set_is_deleted: true
        };

        let payloadQuizItems = {
            quiz_set_quiz_deleted_at: new Date(),
            quiz_set_quiz_is_deleted: true
        };

        try{
            queryOutput.quizSet = await adminModel.updateQuizSet(payloadQuizSet, {quizSetId: data.quizSetId});
        } catch (error){
            sendResponseError(req, res, {status: 400, error, message: "Failed to delete"})
        }

        // quizList.data.forEach((item) => {
        //     try{
        //         quizList = await adminModel.getQuizSetQuizItems({quizSetId: data.quizSetId});
        //     } catch (error){
        //         sendResponseError(req, res, {status: 400, error, message: "Failed to delete"})
        //     }
        // });
        
        try{
            queryOutput.quizSetItems = await adminModel.updateQuizSetQuizItems(payloadQuizItems, {quizSetId: data.quizSetId});
        } catch (error){
            sendResponseError(req, res, {status: 400, error, message: "Failed to delete"})
        }

        controllerOutput(req, res, queryOutput);

    },

    deleteQuizSetQuizItems: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let isDeleted = false;
        let deleted = null;

        console.log(data);

        let payload = {
            quiz_set_quiz_deleted_at: new Date(),
            quiz_set_quiz_is_deleted: true
        };

        try{
            deleted = await adminModel.updateQuizSetQuizItem(payload, {quizId: data.quizId, quizSetId: data.quizSetId});
        } catch (error){
            res.sendResponseError(req, res, { error, message: "Server error: " + error })
        }

        if(deleted){
            if(deleted.hasOwnProperty('data')) {
                if(deleted.data)
                    isDeleted = true;
            }
        }

        queryOutput.deleted = deleted;
        queryOutput.deleteStatus = {isDeleted};
        controllerOutput(req, res, queryOutput);

    },

    updateQuizSetQuizItem: async function (req, res){
        let data = req.body;
        let queryOutput = {};
        
        let payload = {
            date_time_start: data.startTime,
            date_time_end: data.endTime
        };

        console.log(payload);

        try{
            queryOutput.quizSetItem = await adminModel.updateQuizSetQuizItem(payload, {quizId: data.quizId, quizSetId: data.quizSetId});
        } catch (error){
            sendResponseError(req, res, {status: 400, error, message: "Failed to update"})
        }

        controllerOutput(req, res, queryOutput);

    },

    sendEmailAccessCode: async function(req, res){
        let queryOutput = {};
        let recipientEmail = req.body.email;
        let leadSetId = req.body.leadSetId;

        const mailerSendEmailAccessCode = new Mailer();
        const mailerParams = {
            from:       process.env.EMAIL_DONOTREPLY3,
            to:         '',
            subject:    'Registration Access Code',
            template:   'index',
            context:    {
                firstName: '',
                link: '',
                code: '',
            }
        };

        mailerSendEmailAccessCode.setTemplate({template: 'registration-accesscode'});

        if (!recipientEmail && leadSetId) {
            try {
                let results = [];
                let leadSetItems = await adminModel.getLead({leadSetId: leadSetId});

                if(leadSetItems.data){
                    if(leadSetItems.data.length > 0){

                        for(let i = 0; i < leadSetItems.data.length; i++) {
                            let accessCode = null;
                            try{
                                accessCode = await generateAccessCode();
                            } catch (error) {
                                console.log(error);
                                //sendResponseError(req, res, {message: 'Unable to generate Access Code', error});
                            }
                            try {
                                mailerParams.to = leadSetItems.data[i].lead_email;
                                mailerParams.context.firstName = leadSetItems.data[i].lead_email;
                                mailerParams.context.link = `${process.env.APP_BASEURL}/register?accessCode=${accessCode}`;
                                mailerParams.context.code = accessCode;
                                let tempEmailStatus = await mailerSendEmailAccessCode.sendMail(mailerParams);

                                results.push({
                                    lead_id: leadSetItems.data[i].lead_id,
                                    data:  tempEmailStatus,
                                    error: null
                                });
                            } catch (error) {
                                results.push({
                                    leadId: leadSetItems.data[i].lead_id,
                                    data: null,
                                    error: error
                                });
                            }
                        }

                        queryOutput.emailStatus = results;
                    }
                }
            } catch (error) {
                sendResponseError(req, res, {message: 'Unable to send email. 1', error});
            }
        } else {
            let accessCode = null;

            try{
                accessCode = await generateAccessCode();
            } catch (error) {
                sendResponseError(req, res, {message: 'Unable to generate Access Code', error});
            }
            try{
                mailerParams.to = recipientEmail;
                mailerParams.context.firstName = recipientEmail;
                mailerParams.context.link = `${process.env.APP_BASEURL}/register?accessCode=${accessCode}`;
                mailerParams.context.code = accessCode;
                
                let emailStatus = await mailerSendEmailAccessCode.sendMail(mailerParams);
                queryOutput.emailStatus = emailStatus;
            } catch (error) {
                sendResponseError(req, res, {message: 'Unable to send email. ' + error, error});
            }
        }

        controllerOutput(req, res, queryOutput, '');
    },

    getLeadSet: async function (req, res) {
        let id  = req.query.leadSetId;
        let queryOutput = {};
        
        try{
            queryOutput.leadSet  = await adminModel.getLeadSet({id: id});
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to fetch quiz set from database.', error})
        }
        controllerOutput(req, res, queryOutput);
    },

    getLeadSetTable: async function(req, res){
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        console.log(dataTableOptions.orderBy, req.query.orderBy);

        queryOutput.leadSets  = await adminModel.getLeadSetTable(dataTableOptions);

        controllerOutput(req, res, queryOutput);
    },

    getLeadTable: async function (req, res) {
        let id  = req.query.leadSetId;
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        
        try{
            queryOutput.leads  = await adminModel.getLeadTable(dataTableOptions, {leadSetId: id});
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to fetch leads from database.', error});
        }

        // try {

        // } catch (error) {
        //     sendResponseError(req, res, {message: 'Unable to fetch leads from database.', error});
        // }

        controllerOutput(req, res, queryOutput);
    },

    addLeadSet: async function(req, res){
        const name = req.body.name;
        
        let queryOutput = {};
        let payload = {
            lead_set_name: name
        };

        try {
            queryOutput.leadSet = await adminModel.addLeadSet(payload);
        } catch (error) {
            sendResponseError(req, res, {message: 'Failed to add lead set to database.', error})
        }
        
        controllerOutput(req, res, queryOutput, '');
    },

    addLead: async function(req, res){
        const email = req.body.email;
        const setId = req.body.leadSetId;
        
        let queryOutput = {};

        if(setId == null || setId == undefined){
          sendResponseError(req, res, {message: 'Lead Set is not supplied.', error: new Error('Lead Set is not supplied.')});
        } else {
            try {
                const payload = {
                    lead_email: email,
                    fk_lead_set_id: setId
                };
                queryOutput.lead = await adminModel.addLead(payload);
            } catch (error) {
                sendResponseError(req, res, {message: 'Failed to add lead to database.', error});
            }
        }
        controllerOutput(req, res, queryOutput, '');
    },

    deleteLeadSet: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let leadList = null;

        let payloadLeadSet = {
            lead_set_deleted_at: new Date(),
            lead_set_is_deleted: true
        };

        let payloadLeadItems = {
            lead_deleted_at: new Date(),
            lead_is_deleted: true
        };

        try{
            queryOutput.leadSet = await adminModel.updateLeadSet(payloadLeadSet, {leadSetId: data.leadSetId});
        } catch (error){
            sendResponseError(req, res, {status: 400, error, message: "Failed to delete"})
        }
        
        try{
            queryOutput.leadSetItems = await adminModel.updateLeads(payloadLeadItems, {leadSetId: data.leadSetId});
        } catch (error){
            sendResponseError(req, res, {status: 400, error, message: "Failed to delete"})
        }

        controllerOutput(req, res, queryOutput);
    },

    deleteLead: async function (req, res){
        let data = req.query;
        let queryOutput = {};
        let isDeleted = false;
        let deleted = null;

        console.log(data);

        let payload = {
            lead_deleted_at: new Date(),
            lead_is_deleted: true
        };

        try{
            deleted = await adminModel.updateLead(payload, {leadId: data.leadId});
        } catch (error){
            res.sendResponseError(req, res, { error, message: "Server error: " + error })
        }

        if(deleted){
            if(deleted.hasOwnProperty('data')) {
                if(deleted.data)
                    isDeleted = true;
            }
        }

        queryOutput.deleted = deleted;
        queryOutput.deleteStatus = {isDeleted};
        controllerOutput(req, res, queryOutput);

    },

    getClientQuizSetRequestTable: async function(req, res){
        let queryOutput = {};
        let dataTableOptions 	= {
            orderBy:	JSON.parse(req.query.orderBy),
            limit: 		req.query.limit,
            offset: 	req.query.offset,
            filter: 	JSON.parse(req.query.filter)
        }
        let requests = null;

        try {
            requests  = await adminModel.getClientQuizSetRequestTable(dataTableOptions);
        } catch (error) {
            sendResponseError(req, res, {message: 'Something went wrong.', error});
        }

        for(let i = 0; i < requests.data.length; i++) {
            let item = requests.data[i]
            let payload = {
                quizSetId: item.fk_quiz_set_id,
                clientId: item.fk_client_id
            }
            try {
                let exists = await quizModel.checkClientQuizSet(payload);
                item.client_quiz_set_request_is_allowed = exists.rowcount > 0 ? true : false;
            } catch (error) {
                sendResponseError(req, res, {message: 'Something went wrong.', error});
            }
            
        }

        queryOutput.requests = requests;
        controllerOutput(req, res, queryOutput);
    },

    addClientQuizSet: async function(req, res){
        const clientId = req.body.clientId;
        const quizSetId = req.body.quizSetId;

        let queryOutput = {};

        if(quizSetId == null || quizSetId == undefined){
          sendResponseError(req, res, {message: 'Quiz Set is not supplied.', error: new Error('Quiz Set is not supplied.')});
        } else {
            try {
                const payload = {
                    fk_client_id: clientId,
                    fk_quiz_set_id: quizSetId
                };
                queryOutput.clientQuizSet = await quizModel.addClientQuizSet(payload);
            } catch (error) {
                sendResponseError(req, res, {message: 'Failed to add client quiz set to database.', error});
            }
        }
        controllerOutput(req, res, queryOutput, '');
    },

    

     getClientAnswerSheetandAnalysis: async function(req, res){
        let queryOutput = {};

        let filter = {
            clientId: req.query.clientId,
            quizSetId: req.query.quizSetId,
        }
        let answerSheetAndQuiz = null;
        let client = null;
        let analysisArray = { data: [] };

        try {
            client = await clientModel.getClient({id: filter.clientId});
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to fetch client data', error});
        }

        try {
            answerSheetAndQuiz = await answerSheetModel.getAnswerSheetAndQuiz(filter);
            console.log('AnQ:', answerSheetAndQuiz);
            if(answerSheetAndQuiz.data){
                if(Array.isArray(answerSheetAndQuiz.data)){
                    if(answerSheetAndQuiz.data.length > 0) {
                        answerSheetAndQuiz.data.forEach(item => {
                            item.client_answer_sheet_value = JSON.parse(unescape(item.client_answer_sheet_value));
                        })
                    }
                }
            } else {
                sendResponseError(req, res, {message: 'Answer sheet does not exist.', error});
            }
        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to fetch answer sheet', error});
        }
        
        try {
            // Loop answersheets
            for(let i = 0; i < answerSheetAndQuiz.data.length; i++) {
                let analysis = await analyzeQuizAnswers(answerSheetAndQuiz.data[i].client_answer_sheet_value);

                let result = {
                    details: analysis.details,
                    interpretations: analysis.result,
                    answerSheet: answerSheetAndQuiz.data[i].client_answer_sheet_value,
                    exam: answerSheetAndQuiz.data[i]
                }

                analysisArray.data.push(result);
            }

        } catch (error) {
            sendResponseError(req, res, {message: 'Unable to analyze answer sheet' + error, error});
        }

        queryOutput.client = client;
        queryOutput.results = analysisArray;

        controllerOutput(req, res, queryOutput);
     }

    // CLIENT QUIZ SET
                        // let clientQuizSetPayload = {
                        //     fk_client_id: clientId,
                        //     fk_quiz_set_id: quizSetId
                        // }
                        // queryOutput.clientQuizSet = await quizModel.addClientQuizSet(clientQuizSetPayload)
};

async function emailAccessCode(recipientEmail){
    const mailerSendEmailAccessCode = new Mailer();

    new Promise (async (resolve, reject) => {
        try{
            let emailStatus = await mailerSendEmailAccessCode.sendMail({
                from:       process.env.EMAIL_DONOTREPLY2,
                to:         recipientEmail,
                subject:    'Registration Access Code',
                template:   'index',
                context:    {
                    firstName: 'king',
                    link: 'sample access code here',
                }
            });
            //console.log(emailStatus);
            resolve(emailStatus);
        } catch (error) {
            resolve(error);
            throw error;
        }
    })
};

async function generateAccessCode () {
    const length = Number(process.env.ACCESS_CODE_LENGTH);

    return new Promise(async (resolve, reject) => {
        let accessCode = otpGenerator.generate(length, { upperCase: false, specialChars: false });
        console.log(accessCode);
        let code = {
            access_code_value: accessCode,
            access_code_created_at: new Date()
        };
        codeResult = await adminModel.addAccessCode(code);
        
        if (codeResult) {
            resolve(accessCode);
        } else {
            reject(new Error);
        }
    });
    
}

module.exports = Controller;