const { verifyToken }   = require('../util/jwt');
const sessionModel      = require('../models/session');

const Token = {
    checkToken: function(req, res, next){
        let token = req.headers['x-access-token'] || req.headers['authorization'];

        if (token){
            let { isValid } = verifyToken(token);

            if (isValid){
                let session = await sessionModel.getUserSession(token);

                if (session.data) next();
                else {
                    session = await sessionModel.deleteUserSession(token);
                    return res.status(401).send({
                        access: false,
                        message: 'Session expired. Please login again.',
                    });
                }
            }
            else {
                return res.status(401).send({
                    access: false,
                    message: 'Token is not valid.'
                });
            }
        }
        else {
            return res.status(401).send({
                access: false,
                message: 'Auth token is not supplied'
            });
        }
    },

    checkAdminToken: function(req, res, next){
        let token = req.headers['x-access-token'] || req.headers['authorization'];

        if (token){
            let { isValid } = verifyToken(token);

            if (isValid){
                next();
            }
            else {
                return res.status(401).send({
                    access: false,
                    message: 'Token is not valid.'
                });
            }
        }
        else {
            return res.status(401).send({
                access: false,
                message: 'Auth token is not supplied'
            });
        }
    }
}


module.exports = Token;