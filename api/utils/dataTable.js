const dataTable = function (queryBuilder, dataTableOptions) {
    if(dataTableOptions.orderBy != null){
        if(dataTableOptions.orderBy.hasOwnProperty('column') && dataTableOptions.orderBy.hasOwnProperty('direction'))
            if(dataTableOptions.orderBy.column != null && dataTableOptions.orderBy.direction != null)
                queryBuilder.orderBy(dataTableOptions.orderBy.column, dataTableOptions.orderBy.direction)
    }
    if(dataTableOptions.limit != null){
        queryBuilder.limit(Number(dataTableOptions.limit))
    }
    if(dataTableOptions.offset != null){
        queryBuilder.offset(Number(dataTableOptions.offset < 0 ? 0 : dataTableOptions.offset))
    }
    if(dataTableOptions.filter != null){
        if(dataTableOptions.filter.hasOwnProperty('columns') && dataTableOptions.filter.hasOwnProperty('keyword')){
            if(dataTableOptions.filter.columns != null && dataTableOptions.filter.keyword != null){
                dataTableOptions.filter.columns.forEach((column)=>{
                    queryBuilder.andWhere(function(){this.orWhere(column, 'like', `%${dataTableOptions.filter.keyword}%`)})
                })
            }
        }
    }
}

module.exports = { dataTable };