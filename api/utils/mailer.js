const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')

require('dotenv').config();

class Mailer {
    constructor(){
        this.template = null;
        this.transporter = nodemailer.createTransport({
            //service: 'gmail',
            host: 'smtpout.secureserver.net',
            port: 465,
            //secure: false,
            auth: {
                user: process.env.EMAIL_DONOTREPLY3,
                pass: process.env.EMAIL_DONOTREPLY_PASS3
            }
        })
    }

    setTemplate(options) {
        this.template = options.template;
        this.transporter.use('compile', hbs({
            viewEngine: {
                partialsDir: './templates/email/'+this.template,
                layoutsDir: './templates/email/'+this.template,                
              },
            viewPath: './templates/email/'+this.template,
        }));
        
    }

    sendMail(mailOptions) {
        return new Promise (
            (resolve, reject) => {
                this.transporter.sendMail(mailOptions, (err, data) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(data)
                });
            }
        )
    }
}


module.exports = Mailer