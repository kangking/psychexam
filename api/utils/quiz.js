const questionModel = require('../models/question');
const scoreRanges = require('../json/personalityTest/testA/scoreRanges');
const strengths = require('../json/personalityTest/testA/strengths');
const weaknesses = require('../json/personalityTest/testA/weaknesses');
const neutrals = require('../json/personalityTest/testA/neutrals');

const testBInterpretation = require('../json/personalityTest/testB/interpretation');
const testCInterpretation = require('../json/personalityTest/testC/interpretation');

const iqTestAInterpretation = require('../json/iqTest/testA/interpretation');

const analyzeQuizAnswers = async (data) => {
        let result = {};
        let details = {};
        
        data.answers = eliminateDuplicateAnswer(data.answers);
        
        if(data.quizSlug == 'personality-test-a'){
            let rawScore = await personalityA.compareAnswer(data);
            let scoreRange = await personalityA.determineScoreRange(rawScore);
            let interpretations = await personalityA.determineInterpretationConclusion(scoreRange);
            let keys = Object.keys(interpretations);

            result = personalityA.buildResultPersonalityA({keys, interpretations});
            details = {
                rawScore,
                scoreRange,
                interpretations,
            }
        }

        else if(data.quizSlug == 'personality-test-b'){
            let rawScore = await personalityB.firstStep(data);
            let grade = personalityB.secondStep(rawScore);
            
            result = personalityB.thirdStep(grade);
            details = {
                rawScore,
                grade
            }
        }

        else if(data.quizSlug == 'personality-test-c'){
            let rawScore = await personalityC.firstStep(data);
            let grade = personalityC.secondStep(rawScore);

            result = personalityC.thirdStep(grade);
            details = {
                rawScore,
                grade
            }
        }

        else if(data.quizSlug == 'intelligence-qoutient-test'){
            let rawScore = await iqTestA.firstStep(data);

            result = iqTestA.secondStep(rawScore);
            details = {
                rawScore
            }
        }

        else{
            result = {};
            messageOutput = "Quiz not found. No result is given.";
        }

        return new Promise ((resolve, reject) => {
            if (result && reject){
                resolve({ result, details });
            } else {
                reject(new Error);
            }
        })

}

const eliminateDuplicateAnswer = function(answers){
    let filtered = answers.reduce((filtered, item) => {
        if( !filtered.some(filteredItem => JSON.stringify(filteredItem) == JSON.stringify(item)) )
            filtered.push(item)
        return filtered
    }, [])
    
    return filtered;
}

const interpretationRandomizer = function(messages) {
    let rand = Math.floor(Math.random() * messages.length);
    return messages[rand];
}

const personalityA = {
    compareAnswer: async (data) => {
        let { answers, gender } = data;
    
        let result = {};
        
        for(let i=0; i < answers.length; i++){
            let question = await questionModel.getQuestion({id: answers[i].number});
            let ref = {}
            let feed = {}
            // console.log(Array.isArray(question.data));
            if(question.data) {
                ref = {
                    number: answers[i].number,
                    factor: question.data.question_meta_value,
                    answer: question.data.question_answer,
                    points: question.data.question_points,
                }

                feed = {
                    number: answers[i].number,
                    answer: answers[i].value,
                }

                if(!result.hasOwnProperty(ref.factor)){
                    result[ref.factor] = { score: 0 }
                }

                if(ref.answer == feed.answer){
                    result[ref.factor].score += ref.points;
                }
            }
        }

        return new Promise((resolve, reject) => {
            resolve(result)
            reject(false)
        })
    },

    determineScoreRange: async (rawScores) => {
        let scores = rawScores;
        //let scoreRanges = await quizModel.getScoreRanges();
        let result = [];

        let factors = Object.keys(scores);

        factors.forEach((factor)=>{
            let score = scores[factor].score;
            
            for (let j=0; j<scoreRanges.length; j++){
                let scoreRange = scoreRanges[j];
                let payload = { factor: factor, level: "" };

                if(factor == scoreRange.factor){
                    if(score >= scoreRange.highScore.min && score <= scoreRange.highScore.max){
                        payload.level = "+";
                    }
                    else if(score >= scoreRange.lowScore.min && score <= scoreRange.lowScore.max){
                        payload.level = "-";
                    }
                    else {
                        payload.level = "";
                    }
                    payload.value = `${factor}${payload.level}`;
                    result.push(payload)
                }
            }
        })

        return new Promise((resolve, reject) => {
            resolve(result)
            reject(false)
        })
    },

    determineInterpretationConclusion: async (scoreLevels) => {
        let result = { strengths: [], weaknesses: [], neutrals: [] };
        let traits = personalityA.traitCombiner();
        
        scoreLevels.forEach((scoreItem)=>{
            let found = traits.find((trait)=>{
                return trait.factor == scoreItem.factor && trait.scoreLevel == scoreItem.level;
            })

            if(found){
                let payload = {
                    factor: scoreItem.factor,
                    level: scoreItem.level,
                    message: interpretationRandomizer(found.messages),
                };
                result[found.category].push(payload)
            }
        })

        return new Promise((resolve, reject) => {
            resolve(result)
            reject(false)
        })
    },

    traitCombiner: () => {
        let strList = strengths.breakdown;
        let wkList = weaknesses.breakdown;
        let neutList = neutrals.breakdown;

        strList.forEach((item)=>{
            item.category = "strengths";
        })

        wkList.forEach((item)=>{
            item.category = "weaknesses";
        })

        neutList.forEach((item)=>{
            item.category = "neutrals";
        })

        return [...strList, ...wkList, ...neutList]
    },

    buildResultPersonalityA: ({keys, interpretations}) => {
        let result = {};

        keys.forEach(key=>{
            result[key] = result[key] ? result[key] : [];
            interpretations[key].forEach((item)=>{
                let payload = {message: item.message};
                if(item.factor == "G" && item.factor == "-"){
                    payload.priority = true;
                }
                result[key].push(payload)
            })
        })

        return result;
    }
}

const personalityB = {
    firstStep: async (data) => {
        const { answers, quizId } = data;
        let result = {};
        let arr = []; 
        
        for(let i=0; i < answers.length; i++){
            let choices = await questionModel.getChoicesByQuestionByQuiz({/* quizId: quizId, */questionId: answers[i].number /*, questionNumber: answers[i].number */});
            if(choices.data.length > 0) {
                let found = choices.data.find((choice)=>{
                    return choice.choice_legend.toLowerCase() === answers[i].value.toLowerCase()
                })
                
                if(found){
                    if(result == null) result = {};
                    if(typeof result[found.choice_points] !== 'number'){
                        result[found.choice_points] = 0;
                    }
                    result[found.choice_points] += 1;
                    arr.push(found.choice_points);
                }
            }
        }

        if(Object.keys(result).length <= 0) result = null;

        return new Promise((resolve, reject) => {
            resolve(result)
            reject(false)
        })
    },

    secondStep: (rawScore) => {
        let priorityConstant = [7, 6, 2, 9, 5, 1, 8, 3, 4];
        let result = null;
        
        if(rawScore != null){
            let keys = Object.keys(rawScore);
            let values = Object.values(rawScore);
            let maxValue = Math.max(...values);

            let highestKeys = [];

            keys.forEach( key => {
                if(rawScore[key] == maxValue){
                    highestKeys.push(key)
                }
            })

            for(i=0; i < priorityConstant.length; i++){
                let found = highestKeys.find(key => priorityConstant[i] == key )
                if(found) {
                    result = found;
                    break;
                }
            }
        }

        return result;
    },

    thirdStep: (grade) => {
        let message = 'Incorrect input. No results given.';
        let { breakdown } = testBInterpretation;

        if(grade != null){
            let found = breakdown.find((interp)=>{
                return interp.scoreMap == grade;
            })

            message = interpretationRandomizer(found.messages);
        }

        return { message };
    }
}

const personalityC = {
    firstStep: async (data) => {
        const { answers } = data;
        let result = {};
        
        for(let i=0; i < answers.length; i++){
            if(typeof result[answers[i].value] !== 'number'){
                result[answers[i].value] = 0;
            }
            result[answers[i].value] += 1;
        }

        if(Object.keys(result).length <= 0) result = null;

        return result;
    },

    secondStep: (rawScore) => {
        let priorityConstant = ['A','B','C'];
        let result = null;
        
        if(rawScore != null){
            let keys = Object.keys(rawScore);
            let values = Object.values(rawScore);
            let maxValue = Math.max(...values);
            
            let highestKeys = [];

            keys.forEach( key => {
                if(rawScore[key] == maxValue){
                    highestKeys.push(key)
                }
            });

            for(i=0; i < priorityConstant.length; i++){
                let found = highestKeys.find(key => priorityConstant[i].toLowerCase() == key.toLowerCase() );
                if(found) {
                    result = found;
                    break;
                }
            }
        }

        return result;
    },

    thirdStep: (grade) => {
        let message = 'Incorrect input. No results given.';
        let { breakdown } = testCInterpretation;

        if(grade != null){
            let found = breakdown.find((interp)=>{
                return interp.letter.toLowerCase() == grade.toLowerCase();
            })

            message = interpretationRandomizer(found.messages);
        }

        return { message };
    }
}

const iqTestA = {
    firstStep: async (data) => {
        const { answers, quizId } = data;
        let result = 0; // score
        
        for(let i=0; i < answers.length; i++){
            let question = await questionModel.getQuestion({id: answers[i].number});
            //console.log(question.data)
            if(question.data) {
                ref = {
                    answer: question.data.question_answer,
                    points: question.data.question_points,
                }

                feed = {
                    questionId: answers[i].number,
                    answer: answers[i].value,
                }

                if(ref.answer == feed.answer){
                    result += ref.points;
                }
            }
        }

        return new Promise((resolve, reject) => {
            resolve(result)
            reject(false)
        })
    },

    secondStep: (grade) => {
        let message = 'Incorrect input. No results given.';
        let { breakdown } = iqTestAInterpretation;

        if(grade != null){
            let found = breakdown.find((interp)=>{
                if(grade >= interp.min && grade <= interp.max){
                    return interp;
                }
                // return interp.letter.toLowerCase() == grade.toLowerCase();
            })

            message = found.messages;
        }

        return { message };
    }
}

module.exports = { 
    analyzeQuizAnswers,
    eliminateDuplicateAnswer,
    interpretationRandomizer,
    personalityA,
    personalityB,
    personalityC,
    iqTestA
};