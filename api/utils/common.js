const moment = require('moment');
const jwt   = require('jsonwebtoken');

var __ = {};
__.getDateNow = function (promise = false) {
    if(!promise)
        return moment().utcOffset(8).format('YYYY-MM-DD HH:mm:ss');
    else{
        return new Promise(
            resolve => {
                resolve(moment().utcOffset(8).format('YYYY-MM-DD HH:mm:ss'));
            }
        )
    }
}
module.exports = __;