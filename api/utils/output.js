const moment = require('moment');
const jwt   = require('jsonwebtoken');

var __ = {};

__.modelOutput = function(rows, error, options = { multirow: true, forceSingleRow: false }){
        
    if(error){
        return {error: error}
    }
    else{
        if(rows != null){ 
            /**
             * If 'rows' is an object. {rows: [ARRAY], totalRows: [OBJECT]}
             */
            if(rows.hasOwnProperty('rows')){
                if(rows.rows.length <= 1 && options.multirow === false || options.forceSingleRow === true){ 
                    return {data: rows.rows[0], totalRows: rows.countRows[0].count, error: error}
                }
                else{
                    return {data: rows.rows,  totalRows: rows.countRows[0].count, error: error, rowcount: rows.length}
                }
            }
            else{
                if(rows.length <= 1 && options.multirow === false || options.forceSingleRow === true){
                    return {data: rows[0], error: error}
                }
                else{
                    return {data: rows, error: error, rowcount: rows.length}
                }
            }
        }
        else{
            return {data: [], error: error}
        }
    }
    
}

__.controllerOutput = function (req, res, output, message = '', success = true, option = {}){
    let keys = Object.keys(output);

    if(keys){
        keys.forEach(key => {
            if (output[key].error != null) success = false;
        });

        output.success = success;
        output.message = (message != '' || message != null) ? message : '';
    }

    else{
        output.success = false;
        output.message = (message != '' || message != null) ? message : 'Incomplete supplied data.';
    }
    

    if (option.showRows == false) delete output.data;
    //console.log(output)
    res.send(output);
}

__.controllerMultipleOutput = function (req, res, outputArray, message = '', success = true, option = {}) {
    let output = {};
    let counter = 0;

    outputArray.forEach(item => {
      let keys = Object.keys(item);
      keys.forEach(key => {
        if (item[key].error != null) success = false;
      });

      item.success = success;
      item.message = message != "" || message != null ? message : "";

      if(success) counter++

      if (option.showRows == false) delete item.data;
    });
    
    output.data = outputArray;
    output.success = counter > 0 ? true : false;
    res.send(output);
}

__.sendResponseError = function (req, res, body = {status: 500, message: null, error: null}) {
    res.send({
        message: body.message,
        error: body.error,
        success: false,
    });
}

module.exports = __;