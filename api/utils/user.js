const jwt = require('jsonwebtoken');

const __ = {};

__.clientDetailsFromAccessToken = function(req){
    return new Promise(
        async (resolve, reject) => {
            let decoded         = null;
            let clientId        = null;
            let token           = req.headers['x-access-token'] || req.headers['authorization'];
            let isTokenValid    = false;

            if(token){
                if (token.startsWith('Bearer ')) {
                    token = token.slice(7, token.length);
                }

                decoded = jwt.decode(token);

                resolve(decoded);
            }
            else{
                reject(new Error('no token provided.'));
            }

        }
    )
}

module.exports = __;