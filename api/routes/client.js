const express       = require('express');
const router        = express.Router();
const client          = require('../controllers/client');
const quiz            = require('../controllers/quiz');
//const authenticate  = require('../middleware/authenticate.js');
//const validator     = require('../middleware/quiz/validation');

/**
 * REST
 */
router.post(`/api/client/login`, client.login);

router.get(`/api/client`, client.getClient);
router.post(`/api/client`, client.addClient);
router.get(`/api/client-answer-sheet`, client.getClientAnswerSheet);
router.get(`/api/client-answer-sheet-check-exist-done`, client.checkClientAnswerSheetExistandDone);
router.get(`/api/quiz-sets`, quiz.getQuizSet);
router.get(`/api/client-quiz-set`, quiz.getClientQuizSet);
// router.get(`/api/client-quiz-set`, quiz.getClientQuizSet);
router.get(`/api/allow-quiz-set`, quiz.checkClientQuizSet);

//Admin
router.get(`/api/admin/client-table`, client.getClientTable);
router.get(`/api/admin/client-quiz-set-table`, client.getClientByQuizSetTable);
router.get(`/api/admin/client`, client.getClientAsAdmin);
router.get(`/api/admin/client-quiz`, client.getClientQuizAsAdmin); //quiz and answer(sheet)
router.delete(`/api/admin/answer-sheet`, client.deleteClientAnswerSheet);


module.exports = router