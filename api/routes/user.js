const express       = require('express')
const router        = express.Router()
// const user          = require('../controllers/user');
// const validator     = require('../middlewares/user/validation');
// const permission     = require('../middlewares/shop/permission');
// const authenticate  = require('../middlewares/authenticate.js');

/**
 * REST
 */

// router.post(`/api/user/signup`,     validator.signup, validator.validate, user.create);
// router.post(`/api/user/register`,   validator.register, validator.validate, user.addUser);
// router.post(`/api/user/login`,      validator.login, validator.validate, user.login)
// router.post(`/api/user/logout`,     user.logout)
// router.get(`/api/get-user`,         authenticate.checkToken, user.getUser)
// router.put(`/api/edit-user`,        authenticate.checkToken, permission.checkShop, validator.updateUser, validator.validate, user.updateUser)
// router.put(`/api/change-password`,  authenticate.checkToken, permission.checkShop, validator.changePassword, validator.validate, user.changePassword)

// router.get(`/api/send-email`,               user.sendEmail)
// router.post(`/api/user/password-recovery`,  user.requestRecovery)
// router.post(`/api/user/password-reset`,     user.processRecovery)

// router.post(`/api/user/email-verification`,  user.requestVerification)
// router.post(`/api/user/verify-email`,       user.processVerification)

// router.post(`/api/check-username-exist`, validator.checkUsername, validator.validate, user.checkUsernameExist);
// router.post(`/api/check-email-exist`, validator.checkEmail, validator.validate, user.checkEmailExist);


module.exports = router