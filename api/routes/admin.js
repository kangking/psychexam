const express       = require('express');
const router        = express.Router();
const admin          = require('../controllers/admin');
//const authenticate  = require('../middleware/auth.js');
//const validator     = require('../middleware/quiz/validation');

/**
 * REST
 */

router.get(`/api/admin`, admin.getAdmin);

router.get(`/api/admin/get-logo-file`, admin.getLogoFile);
router.get(`/api/admin/access-codes-table`, admin.getAccessCodeTable);
router.get(`/api/admin/quiz-set-table`, admin.getQuizSetTable);
router.get(`/api/admin/quiz-set`, admin.getQuizSet);
router.get(`/api/admin/quiz-table`, admin.getQuizTable);
router.get(`/api/admin/quiz-set-items-table`, admin.getQuizSetQuizTable);
router.get(`/api/admin/lead-set-table`, admin.getLeadSetTable);
router.get(`/api/admin/lead-set`, admin.getLeadSet);
router.get(`/api/admin/lead-table`, admin.getLeadTable);
router.get(`/api/admin/client-quiz-set-request-table`, admin.getClientQuizSetRequestTable);
router.get(`/api/admin/client-answer-sheet-and-analysis`, admin.getClientAnswerSheetandAnalysis); // new

router.post(`/api/admin/register`, admin.addAdminAccount);
router.post(`/api/admin/login`, admin.login);
router.post(`/api/admin/access-code`, admin.addAccessCode);
router.post(`/api/admin/quiz-set`, admin.addQuizSet);
router.post(`/api/admin/quiz-set-items`, admin.addQuizSetQuizItems);
router.post(`/api/admin/lead-set`, admin.addLeadSet);
router.post(`/api/admin/lead`, admin.addLead);
router.post(`/api/admin/client-quiz-set`, admin.addClientQuizSet);

router.put(`/api/admin/quiz-set`, admin.updateQuizSet);
router.put(`/api/admin/quiz-set-item`, admin.updateQuizSetQuizItem);

router.delete(`/api/admin/quiz-set`, admin.deleteQuizSet);
router.delete(`/api/admin/quiz-set-items`, admin.deleteQuizSetQuizItems);
router.delete(`/api/admin/access-code`, admin.deleteAccessCode);
router.delete(`/api/admin/lead-set`, admin.deleteLeadSet);
router.delete(`/api/admin/lead`, admin.deleteLead);

router.post(`/api/admin/email-registration-accesscode`, admin.sendEmailAccessCode);

module.exports = router