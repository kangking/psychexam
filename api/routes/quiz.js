const express       = require('express');
const router        = express.Router();
const quiz          = require('../controllers/quiz');
//const authenticate  = require('../middleware/authenticate.js');
//const validator     = require('../middleware/quiz/validation');

/**
 * REST
 */

router.get(`/api/quiz`, quiz.getQuiz);
router.get(`/api/quiz-schedule`, quiz.checkQuizSchedule);

router.post(`/api/quiz`, quiz.addQuiz);
router.post(`/api/quiz-answers`, quiz.submitQuiz);
router.post(`/api/analyze-result`, quiz.analyzeQuizResult);

module.exports = router;