const output = require('../utils/output');

var Model = {

    getQuestion: function({id, number}) {
        return new Promise (
            resolve => {
                qb('question')
                .select('*')
                .join('question_meta', 'question.question_id', '=', 'question_meta.fk_question_id')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('question_id', '=', id)
                    }
                    if(number != null){
                        queryBuilder.where('question_number', '=', number)
                    }
                })
                .then((rows) => {
                    resolve(output.modelOutput(rows, null, { multirow: id != null ? false : true }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getQuestions: function({id=null, quizId=null}) {
        return new Promise (
            resolve => {
                qb('question')
                .select('*')
                .join('question_meta', 'question.question_id', '=', 'question_meta.fk_question_id')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('question_id', '=', id)
                    }
                    if(quizId != null){
                        queryBuilder.where('fk_quiz_id', '=', quizId)
                    }
                })
                .then((rows) => {
                    resolve(output.modelOutput(rows, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getChoices: function({id=null, questionId}) {
        return new Promise (
            resolve => {
                qb('choice')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('choice_id', '=', id)
                    }
                    else{
                        queryBuilder.where('fk_question_id', '=', questionId)
                    }
                })
                .then((rows) => {
                    resolve(output.modelOutput(rows, null, { multirow: id != null ? false : true }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getChoicesByQuestionByQuiz: function({id=null, questionId=null, quizId=null, questionNumber=null}) {
        return new Promise (
            resolve => {
                qb('choice')
                .select('*')
                .join('question', 'question.question_id', '=', 'choice.fk_question_id')
                .join('quiz', 'quiz.quiz_id', '=', 'question.fk_quiz_id')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('choice_id', '=', id)
                    }
                    if (questionId != null) {
                        queryBuilder.where('fk_question_id', '=', questionId)
                    }
                    if (quizId != null) {
                        queryBuilder.where('fk_quiz_id', '=', quizId)
                    }
                    if (questionNumber != null) {
                        queryBuilder.where('question_number', '=', questionNumber)
                    }
                })
                .then((rows) => {
                    resolve(output.modelOutput(rows, null, { multirow: id != null ? false : true }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    addQuestion: function(data){
        return new Promise (
            resolve => {
                qb('question')
                .insert(data)
                .then((id) => {
                    resolve(output.modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    addQuestionMeta: function(data){
        return new Promise (
            resolve => {
                qb('question_meta')
                .insert(data)
                .then((id) => {
                    resolve(output.modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    addChoice: function(data){
        return new Promise (
            resolve => {
                qb('choice')
                .insert(data)
                .then((id) => {
                    resolve(output.modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    addAnswer: function(data){
        return new Promise (
            resolve => {
                qb('answer')
                .insert(data)
                .then((id) => {
                    resolve(output.modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )                
    },
    
    
};


module.exports = Model;