const { modelOutput } = require('../utils/output');
const { dataTable } = require('../utils/dataTable');

var Model = {

    getClientTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'client_first_name', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {}
    ) {

        //console.log(dataTableOptions);
        return new Promise ( 
            resolve => {
                qb('client')
                .select('*')
                .modify((queryBuilder)=>{
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'client_last_name', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('client')
                    .select(qb.raw('count(`client_id`) as count'))
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getClientByQuizSetTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'client_first_name', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = { quizSetId: null }
    ) {

        //console.log(dataTableOptions);
        return new Promise ( 
            resolve => {
                qb('client_quiz_set')
                .select('*')
                .leftJoin('client', 'client.client_id', '=', 'client_quiz_set.fk_client_id')
                .leftJoin('quiz_set', 'quiz_set.quiz_set_id', '=', 'client_quiz_set.fk_quiz_set_id')
                .leftJoin('client_answer_sheet', 'client_answer_sheet.fk_client_id', '=', 'client.client_id')
                .leftJoin('quiz_set_quiz', 'quiz_set_quiz.quiz_set_quiz_id', '=', 'client_answer_sheet.fk_quiz_set_quiz_id')
                .leftJoin('quiz', 'quiz.quiz_id', '=', 'quiz_set_quiz.fk_quiz_id')
                .andWhere('client_quiz_set.client_quiz_set_is_deleted', '=', false)
                .andWhere('quiz_set.quiz_set_is_deleted', '=', false)
                .andWhere('client_answer_sheet.client_answer_sheet_is_deleted', '=', false)
                .andWhere('quiz_set_quiz.quiz_set_quiz_is_deleted', '=', false)
                .modify((queryBuilder)=>{
                    dataTable(queryBuilder, dataTableOptions);
                    if (options.quizSetId) {
                        queryBuilder.andWhere('quiz_set.quiz_set_id', '=', options.quizSetId)
                        queryBuilder.andWhere('client_quiz_set.fk_quiz_set_id', '=', options.quizSetId)
                        queryBuilder.andWhere('quiz_set_quiz.fk_quiz_set_id', '=', options.quizSetId)
                    }
                })
                .orderBy(dataTableOptions.orderBy.column || 'client_last_name', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('client_quiz_set')
                    .select(qb.raw('count(`client_quiz_set_id`) as count'))
                    .modify((queryBuilder)=>{
                        if (options.quizSetId) {
                            queryBuilder.andWhere('client_quiz_set.fk_quiz_set_id', '=', options.quizSetId)
                        }
                    })
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getClient: function({id=null, email=null}){
        return new Promise ((resolve, reject) => {
            qb('client')
            .select('*')
            .modify((queryBuilder)=>{
                if(id != null){
                    queryBuilder.where('client_id', '=', id)
                }
                if(email != null){
                    queryBuilder.where('client_email', '=', email)
                }
                queryBuilder.where('client_is_deleted', '=', false)
            })
            .then((rows) => {
                resolve(modelOutput(rows, null, { multirow: id != null || email != null ? false : true }));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                reject(new Error(error));
            })
        })
    },

    getClient2: function({id=null, email=null}){
        let result = null;
        let error = null;

        qb('client')
        .select('*')
        .modify((queryBuilder)=>{
            if(id != null){
                queryBuilder.where('client_id', '=', id)
            }
            if(email != null){
                queryBuilder.where('client_email', '=', email)
            }
            queryBuilder.where('client_is_deleted', '=', false)
        })
        .then((rows) => {
            result = modelOutput(rows, null, { multirow: id != null || email != null ? false : true });
        })
        .catch((err) => {
            error = err;
        })

        return new Promise ((resolve, reject) => {
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        })
    },

    addClient: function(data){
        return new Promise (
            (resolve, reject) => {
                qb('client')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },
    
};


module.exports = Model;