const { modelOutput } = require('../utils/output');

var Model = {
    getAnswerSheet: function({answerSheetId=null, clientId=null, quizId=null, quizSetItemId=null}) {
        return new Promise (
            resolve => {
                qb('client_answer_sheet')
                .select('*')
                .modify((queryBuilder)=>{
                    if(answerSheetId != null){
                        queryBuilder.where('client_answer_sheet', '=', answerSheetId)
                    }
                    if(clientId != null){
                        queryBuilder.andWhere('fk_client_id', '=', clientId)
                    }
                    if(quizSetItemId != null){
                        queryBuilder.andWhere('fk_quiz_set_quiz_id', '=', quizSetItemId)
                    }
                    queryBuilder.where('client_answer_sheet_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: answerSheetId != null || clientId != null || quizSetItemId != null ? false : true, forceSingleRow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    // getAnswerSheetAndQuiz: function({id=null, clientId=null}){
    //     return new Promise (
    //         resolve => {
    //             qb('client_answer_sheet')
    //             .select('*')
    //             .leftJoin('quiz_set_quiz', 'quiz.quiz_id', '=', 'client_answer_sheet.fk_quiz_id')
    //             .leftJoin('quiz', 'quiz.quiz_id', '=', 'client_answer_sheet.fk_quiz_id')
    //             .modify((queryBuilder)=>{
    //                 if(clientId != null){
    //                     queryBuilder.where('fk_client_id', '=', clientId)
    //                 }
    //                 queryBuilder.where('client_answer_sheet_is_deleted', '=', false)
    //             })
    //             .then((rows) => {
    //                 resolve(modelOutput(rows, null, { multirow: id != null ? false : true }));
    //             })
    //             .catch((error) => {
    //                 resolve(modelOutput(null, error));
    //                 throw error
    //             })
    //         }
    //     )
    // },

    getAnswerSheetAndQuiz: function({clientId=null, quizSetId=null}) {
        console.log({clientId: clientId});
        return new Promise (
            resolve => {
                qb('client_answer_sheet')
                .select('*')
                .leftJoin('quiz_set_quiz', 'quiz_set_quiz.quiz_set_quiz_id', '=', 'client_answer_sheet.fk_quiz_set_quiz_id')
                .leftJoin('quiz_set', 'quiz_set.quiz_set_id', '=', 'quiz_set_quiz.fk_quiz_set_id')
                .leftJoin('client_quiz_set', 'client_quiz_set.fk_quiz_set_id', '=', 'quiz_set.quiz_set_id')
                .leftJoin('quiz', 'quiz.quiz_id', '=', 'quiz_set_quiz.fk_quiz_id')
                .leftJoin('client', 'client.client_id', '=', 'client_quiz_set.fk_client_id')
                .modify((queryBuilder)=>{
                    if(clientId != null){
                        queryBuilder.where('client_answer_sheet.fk_client_id', '=', clientId)
                        queryBuilder.where('client_quiz_set.fk_client_id', '=', clientId)
                    }
                    if(quizSetId != null){
                        queryBuilder.where('quiz_set.quiz_set_id', '=', quizSetId)
                        queryBuilder.where('quiz_set_quiz.fk_quiz_set_id', '=', quizSetId)
                        queryBuilder.where('client_quiz_set.fk_quiz_set_id', '=', quizSetId)
                    }
                    //queryBuilder.andWhere('client_quiz_set.client_quiz_set_is_deleted', '=', false)
                    //queryBuilder.andWhere('quiz_set.quiz_set_is_deleted', '=', false)
                    queryBuilder.andWhere('quiz_set_quiz.quiz_set_quiz_is_deleted', '=', false)
                    queryBuilder.andWhere('client_answer_sheet.client_answer_sheet_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    addAnswerSheet: function(data){
        console.log(data);
        return new Promise (
            resolve => {
                qb('client_answer_sheet')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    updateAnswerSheet: function(data, {answerSheetId}){
        return new Promise ((resolve, reject) => {
            qb('client_answer_sheet')
            .update(data)
            .where('client_answer_sheet_id', '=', answerSheetId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                reject(modelOutput(null, error));
                throw error
            })
        });
    },

};


module.exports = Model;