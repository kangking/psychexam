const { modelOutput } = require('../utils/output');

var Model = {

    getClientSession: function(token){
        return new Promise (
            resolve => {
                qb('session')
                .select('*')
                .where('token', '=', token)
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    addClientSession: function(data){
        return new Promise (
            resolve => {
                qb('session')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )             
    },

    deleteClientSession: function(token) {
        return new Promise(
            resolve => {
                qb('session')
                .where('token', '=', token)
                .del()
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

};


module.exports = Model;