const output = require('../utils/output');

var Model = {

    getQuiz: function(id) {
        return new Promise (
            resolve => {
                qb('quiz')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('quiz_id', '=', id)
                    }
                })
                .then((rows) => {
                    resolve(output.modelOutput(rows, null, { multirow: id != null ? false : true }));
                })
                .catch((error) => {
                    resolve(output.modelOutput(null, error));
                    throw error
                })
            }
        )
    },
    
    
};


module.exports = Model;