const { modelOutput } = require('../utils/output');

var Model = {
    getQuiz: function({id=null, slug=null}) {
        return new Promise (
            resolve => {
                qb('quiz')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('quiz_id', '=', id)
                    }
                    if(slug != null){
                        queryBuilder.where('quiz_slug', '=', slug)
                    }
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: id != null || slug != null ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getQuizSets: function({id=null}) {
        return new Promise (
            resolve => {
                qb('quiz_set')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('quiz_set_id', '=', id)
                    }
                    queryBuilder.andWhere('quiz_set.quiz_set_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: id != null ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    checkClientQuizSet: function({quizSetId=null, clientId=null}) {
        return new Promise (
            resolve => {
                qb('client_quiz_set')
                .select('*')
                .modify((queryBuilder)=>{
                    if(quizSetId != null){
                        queryBuilder.where('client_quiz_set.fk_quiz_set_id', '=', quizSetId)
                    }
                    if(clientId != null){
                        queryBuilder.where('client_quiz_set.fk_client_id', '=', clientId)
                    }
                    queryBuilder.andWhere('client_quiz_set.client_quiz_set_is_deleted', '=', false)
                    //queryBuilder.orWhereIn('client_answer_sheet.client_answer_sheet_is_deleted', [0, null, undefined, false])
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getClientQuizSet: function({quizSetId=null, clientId=null, quizId=null}) {
        return new Promise (
            resolve => {
                qb('client_quiz_set')
                .select('*')
                .leftJoin('quiz_set', 'client_quiz_set.fk_quiz_set_id', '=', 'quiz_set.quiz_set_id')
                .leftJoin('quiz_set_quiz', 'quiz_set.quiz_set_id', '=', 'quiz_set_quiz.fk_quiz_set_id')
                .leftJoin('quiz', 'quiz.quiz_id', '=', 'quiz_set_quiz.fk_quiz_id')
                //.leftJoin('client_answer_sheet', 'client_answer_sheet.fk_quiz_set_quiz_id', '=', 'quiz_set_quiz.quiz_set_quiz_id')
                .modify((queryBuilder)=>{
                    if(quizSetId != null){
                        queryBuilder.where('client_quiz_set.fk_quiz_set_id', '=', quizSetId)
                    }
                    if(clientId != null){
                        queryBuilder.where('client_quiz_set.fk_client_id', '=', clientId)
                    }
                    queryBuilder.andWhere('client_quiz_set.client_quiz_set_is_deleted', '=', false)
                    queryBuilder.andWhere('quiz_set.quiz_set_is_deleted', '=', false)
                    queryBuilder.andWhere('quiz_set_quiz.quiz_set_quiz_is_deleted', '=', false)
                    //queryBuilder.orWhereIn('client_answer_sheet.client_answer_sheet_is_deleted', [0, null, undefined, false])
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    addQuiz: function(data){
        return new Promise (
            resolve => {
                qb('quiz')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    addClientQuizSet: function(data){
        return new Promise (
            resolve => {
                qb('client_quiz_set')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getQuizSetItem: function({quizSetItemId=null}) {
        console.log(quizSetItemId);
        return new Promise (
            resolve => {
                qb('quiz_set_quiz')
                .select('*')
                .modify((queryBuilder)=>{
                    if(quizSetItemId != null){
                        queryBuilder.where('quiz_set_quiz_id', '=', quizSetItemId)
                    }
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: quizSetItemId != null ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    /**
     * 
     * @param {clientSetId} Id of client
     * @param {quizSetId} Id of quiz set 
     * @returns requests row
     */
    addClientQuizSetRequest: function(data){
        return new Promise (
            resolve => {
                qb('client_quiz_set_request')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    /**
     * 
     * @param {clientId} Id of client
     * @param {quizSetId} Id of quiz set 
     * @returns requests row
     */
    getClientQuizSetRequests: function({id=null, clientId=null, quizSetId=null}) {
        return new Promise (
            resolve => {
                qb('client_quiz_set_request')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('client_quiz_set_request_id', '=', id)
                    }
                    if(clientId != null){
                        queryBuilder.where('fk_client_id', '=', clientId)
                    }
                    if(quizSetId != null){
                        queryBuilder.where('fk_quiz_set_id', '=', quizSetId)
                    }
                    queryBuilder.andWhere('client_quiz_set_request.client_quiz_set_request_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: id != null ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },











    addQuiz2: function(data){
        let errors = {quiz: [], questions: []};
        let outputObj = {};
        return new Promise(
            async (resolve) => {
                const trxProvider = qb.transactionProvider();
                const trx = await trxProvider();
                
                // Quiz
                const quizId = await trx('quiz').insert(data.quiz).then((id) =>{return id}).catch((error) =>{errors.quiz = errors})
                outputObj.quiz = quizId[0];

                // Questions
                outputObj.questions = [];
                data.questions.forEach(async (question) => {
                    question["fk_quiz_id"] = quizId[0];
                    let questionPayload = {...question};
                    delete questionPayload.choices;
                    const questionId = await trx('question')
                        .insert(questionPayload)
                        .then((id) =>{
                            outputObj.questions.push(questionId);
                            console.log("q-inserted");
                            return id 
                        })
                        .catch((error) =>{
                            errors.questions = errors.questions.length > 0 ? errors.questions : [];
                            errors.questions.push(error)
                        })
                    
                    // Choices
                    // question.choices.forEach(async (choice)=>{
                    //     choice["fk_question_id"] = questionId[0];
                    //     const choiceId = await trx('choice').insert(choice)
                    // })
                    //trx2.commit();
                    //console.log(trx2.isCompleted());
                })        
                trx.commit();
                resolve(modelOutput(outputObj, null, { multirow: false }))
            }
        )

    },
    
    addQuiz3: function(data){
        return new Promise (
            resolve => {
                
                qb.transaction((trx) => {
                    return qb('quiz')
                    .transacting(trx)
                    .insert(data.quiz)
                    .then((quizId) => {
                        //  qb('question')
                        // .modify((queryBuilder)=>{
                        //     data.questions.forEach((question)=>{
                        //         question["fk_quiz_id"] = quizId[0];
                        //         queryBuilder.insert(question)
                        //         .then(function(questionId) {
                        //              qb('choice')
                        //             .modify((queryBuilder2)=>{
                        //                 question.choices.forEach((choice)=>{
                        //                     choice["fk_question_id"] = questionId[0];
                        //                     queryBuilder2.insert(choice)
                        //                     .then(function(choiceId) {
                        //                         //trx.commit();
                        //                     })
                        //                     .catch(function(e) {
                        //                         trx.rollback();
                        //                     });
                        //                 })
                        //             })
                        //         })
                        //         .catch(function(e) {
                        //             trx.rollback();
                        //         });
                        //     })
                        // })
                        resolve(modelOutput(quizId, null, { multirow: false }));
                        
                    })
                    .then(trx.commit())
                    .catch((e) => {
                        trx.rollback();
                        resolve(modelOutput(null, error));
                        throw e;
                    })
                })
                .then(() => {
                // it worked
                })
                .catch((error) => {
                // it failed
                    throw error
                });
                
            },

            reject => {
                reject("reject: error")
            }
        )                
    },

    


};


module.exports = Model;