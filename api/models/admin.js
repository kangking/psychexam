const { modelOutput } = require('../utils/output');
const { dataTable } = require('../utils/dataTable');

var Model = {

    getAdmin: function({id=null, email=null}){
        return new Promise ((resolve, reject) => {
            qb('admin_account')
            .select('*')
            .modify((queryBuilder)=>{
                if(id != null){
                    queryBuilder.where('admin_account_id', '=', id)
                }
                if(email != null){
                    queryBuilder.where('admin_account_email', '=', email)
                }
                queryBuilder.where('admin_account_is_deleted', '=', false)
            })
            .then((rows) => {
                resolve(modelOutput(rows, null, { multirow: id != null || email != null ? false : true }));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                reject(new Error(error));
            })
        })
    },
    
    getAccessCode: function(id, code, isValidRequired=true) {
        return new Promise (
            resolve => {
                qb('access_code')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('access_code_id', '=', id)
                    }
                    if(code != null){
                        queryBuilder.where('access_code_value', '=', code)
                    }
                    if(isValidRequired){
                        queryBuilder.andWhere('access_code_is_valid', '=', true)
                    }
                    queryBuilder.andWhere('access_code_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: id != null || code != null ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getAccessCodeTable: function(
            dataTableOptions = {
                orderBy: {
                    column: 'access_code_created_at', direction: 'DESC'
                },
                limit: null,
                offset: null,
                filter: {
                    keyword: null, columns: null
                }
            }, options = {}
        ) {

        return new Promise ( 
            resolve => {
                qb('access_code')
                .select('*')
                .modify((queryBuilder)=>{
                    // queryBuilder.leftJoin('quiz_set', 'quiz_set.quiz_set_id', '=', 'access_code.fk_quiz_set_id')
                    queryBuilder.andWhere('access_code_is_deleted', '=', false)
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'access_code_created_at', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('access_code')
                    .select(qb.raw('count(`access_code_id`) as count'))
                    .andWhere('access_code_is_deleted', '=', false)
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
                
                //console.log(qb);
            }
        )
    },

    addAccessCode: function(data){
        return new Promise (
            resolve => {
                qb('access_code')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    updateAccessCode: function(data, id){
        return new Promise (
            resolve => {
                qb('access_code')
                .update(data)
                .where('access_code_id', '=', id)
                .then((rows) => {
                    resolve(modelOutput(rows));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )         
    },

    searchAdminAccount: function(username, multirow = false){
        return new Promise (
            resolve => {
                qb('admin_account')
                .select('*')
                .where('admin_account_username', '=', username)
                //.leftJoin('shop', 'user.shop_idshop', '=', 'shop.idshop')
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: multirow }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },
    
    addAdminAccount: function(data){
        return new Promise (
            resolve => {
                qb('admin_account')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    getQuizSetTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'quiz_set_created_at', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {}
    ) {

        return new Promise ( 
            resolve => {
                qb('quiz_set')
                .select('*')
                .modify((queryBuilder)=>{
                    queryBuilder.andWhere('quiz_set_is_deleted', '=', false)
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'quiz_set_created_at', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('quiz_set')
                    .select(qb.raw('count(`quiz_set_id`) as count'))
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw new Error(error)
                })
            }
        )
    },

    addQuizSet: function(data){
        return new Promise (
            resolve => {
                qb('quiz_set')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    updateQuizSet: function(data, {quizSetId}){
        return new Promise ((resolve, reject) => {
            qb('quiz_set')
            .update(data)
            .where('quiz_set_id', '=', quizSetId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    getQuizSet: function({id=null}) {
        return new Promise (
            resolve => {
                qb('quiz_set')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('quiz_set_id', '=', id)
                    }
                    queryBuilder.andWhere('quiz_set_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: id ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getQuizTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'quiz_slug', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {}
    ) {

        return new Promise ( 
            resolve => {
                qb('quiz')
                .select('*')
                .modify((queryBuilder)=>{
                    // queryBuilder.andWhere('quiz_is_deleted', '=', false)
                    // add id filter
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'quiz_slug', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('quiz')
                    .select(qb.raw('count(`quiz_id`) as count'))
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw new Error(error)
                })
            }
        )
    },

    getQuizSetQuizItem: function({quizId, quizSetId}) {
        return new Promise (
            resolve => {
                qb('quiz_set_quiz')
                .select('*')
                .leftJoin('quiz', 'quiz.quiz_id', '=', 'quiz_set_quiz.fk_quiz_id')
                .andWhere('quiz_set_quiz.fk_quiz_set_id', '=', quizSetId)
                .andWhere('quiz.quiz_id', '=', quizId)
                .andWhere('quiz_set_quiz.quiz_set_quiz_is_deleted', '=', 0)
                .then((rows) => {
                    resolve(modelOutput(rows, null));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getQuizSetQuizItems: function({quizSetId}) {
        return new Promise (
            resolve => {
                qb('quiz_set_quiz')
                .select('*')
                .leftJoin('quiz', 'quiz.quiz_id', '=', 'quiz_set_quiz.fk_quiz_id')
                .andWhere('quiz_set_quiz.fk_quiz_set_id', '=', quizSetId)
                .andWhere('quiz.quiz_is_deleted', '=', 0)
                .then((rows) => {
                    resolve(modelOutput(rows, null));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getQuizSetQuizItemsTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'quiz_slug', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {quizSetId: null}
    ) {

        return new Promise ( 
            resolve => {
                qb('quiz_set_quiz')
                .select('*')
                .modify((queryBuilder)=>{
                    // add id filter
                    queryBuilder.leftJoin('quiz', 'quiz.quiz_id', '=', 'quiz_set_quiz.fk_quiz_id')
                    queryBuilder.andWhere('quiz_set_quiz_is_deleted', '=', 0)
                    //queryBuilder.andWhere('quiz_is_deleted', '=', 0)
                    if(options.quizSetId){
                        queryBuilder.where('fk_quiz_set_id', '=', options.quizSetId)
                    }
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'quiz_slug', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('quiz_set_quiz')
                    .select(qb.raw('count(`fk_quiz_id`) as count'))
                    .andWhere('quiz_set_quiz_is_deleted', '=', 0)
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw new Error(error)
                })
            }
        )
    },

    addQuizSetQuizItems: function(data){
        return new Promise (
            resolve => {
                qb('quiz_set_quiz')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: false }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    updateQuizSet: function(data, {quizSetId}){
        return new Promise ((resolve, reject) => {
            qb('quiz_set')
            .update(data)
            .where('quiz_set_id', '=', quizSetId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    updateQuizSetQuizItems: function(data, {quizSetId}){
        return new Promise ((resolve, reject) => {
            qb('quiz_set_quiz')
            .update(data)
            .andWhere('fk_quiz_set_id', '=', quizSetId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    updateQuizSetQuizItem: function(data, {quizId, quizSetId}){
        console.log(data);
        return new Promise ((resolve, reject) => {
            qb('quiz_set_quiz')
            .update(data)
            .where('fk_quiz_id', '=', quizId)
            .andWhere('fk_quiz_set_id', '=', quizSetId)
            .andWhere('quiz_set_quiz_is_deleted', '=', false)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    getLeadSet: function({id=null}) {
        return new Promise (
            resolve => {
                qb('lead_set')
                .select('*')
                .modify((queryBuilder)=>{
                    if(id != null){
                        queryBuilder.where('lead_set_id', '=', id)
                    }
                    queryBuilder.andWhere('lead_set_is_deleted', '=', false)
                })
                .then((rows) => {
                    resolve(modelOutput(rows, null, { multirow: id ? false : true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getLeadSetTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'lead_set_created_at', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {}
    ) {

        return new Promise ( 
            resolve => {
                qb('lead_set')
                .select('*')
                .modify((queryBuilder)=>{
                    queryBuilder.andWhere('lead_set_is_deleted', '=', false)
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'lead_set_created_at', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('lead_set')
                    .select(qb.raw('count(`lead_set_id`) as count'))
                    .modify((queryBuilder)=>{
                        queryBuilder.andWhere('lead_set_is_deleted', '=', false)
                    })
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw new Error(error)
                })
            }
        )
    },

    getLead: function({leadSetId}) {
        return new Promise (
            resolve => {
                qb('lead')
                .select('*')
                .leftJoin('lead_set', 'lead_set.lead_set_id', '=', 'lead.fk_lead_set_id')
                .andWhere('lead.fk_lead_set_id', '=', leadSetId)
                .andWhere('lead.lead_is_deleted', '=', 0)
                .then((rows) => {
                    resolve(modelOutput(rows, null));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )
    },

    getLeadTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'lead_created_at', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {leadSetId: null}
    ) {

        return new Promise ( 
            resolve => {
                qb('lead')
                .select('*')
                //.distinct('lead.lead_id')
                // .groupBy('lead.lead_id')
                // .max('lead_email_response_created_at')
                .modify((queryBuilder)=>{
                    // add id filter
                    queryBuilder.leftJoin('lead_set', 'lead.fk_lead_set_id', '=', 'lead_set.lead_set_id')
                    // queryBuilder.leftJoin('lead_email_response', 'lead_email_response.fk_lead_id', '=', 'lead.lead_id')
                    queryBuilder.andWhere('lead_is_deleted', '=', 0)
                    //queryBuilder.andWhere('quiz_is_deleted', '=', 0)
                    if(options.leadSetId){
                        queryBuilder.where('fk_lead_set_id', '=', options.leadSetId)
                    }
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'lead_created_at', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('lead')
                    .select(qb.raw('count(`lead_id`) as count'))
                    .andWhere('lead_is_deleted', '=', 0)
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw (error)
                })
            }
        )
    },

    addLead: function(data){
        return new Promise (
            resolve => {
                qb('lead')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    addLeadSet: function(data){
        return new Promise (
            resolve => {
                qb('lead_set')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    addLeadSetItem: function(data){
        return new Promise (
            resolve => {
                qb('lead_set_lead')
                .insert(data)
                .then((id) => {
                    resolve(modelOutput(id, null, { multirow: true }));
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw error
                })
            }
        )                
    },

    addLeadSetItem: function(data, {leadId, leadSetId}){
        return new Promise ((resolve, reject) => {
            qb('lead_set_lead')
            .update(data)
            .where('fk_lead_id', '=', leadId)
            .andWhere('fk_lead_set_id', '=', leadSetId)
            .andWhere('lead_set_lead_is_deleted', '=', false)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    updateLeadSet: function(data, {leadSetId}){
        return new Promise ((resolve, reject) => {
            qb('lead_set')
            .update(data)
            .where('lead_set_id', '=', leadSetId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    updateLead: function(data, {leadId}){
        return new Promise ((resolve, reject) => {
            qb('lead')
            .update(data)
            .where('lead_id', '=', leadId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    updateLeads: function(data, {leadSetId}){
        return new Promise ((resolve, reject) => {
            qb('lead')
            .update(data)
            .where('fk_lead_set_id', '=', leadSetId)
            .then((rows) => {
                resolve(modelOutput(rows));
            })
            .catch((error) => {
                resolve(modelOutput(null, error));
                throw error
            })
        });
    },

    
    /**
     * 
     * @param {tableParams} Table Params
     * @param {clientSetId} Id of client
     * @param {quizSetId} Id of quiz set 
     * @returns requests row
     */

     getClientQuizSetRequestTable: function(
        dataTableOptions = {
            orderBy: {
                column: 'client_quiz_set_request_created_at', direction: 'DESC'
            },
            limit: null,
            offset: null,
            filter: {
                keyword: null, columns: null
            }
        }, options = {}
    ) {

        return new Promise ( 
            resolve => {
                qb('client_quiz_set_request')
                .select('*')
                .modify((queryBuilder)=>{
                    queryBuilder.leftJoin('client', 'client.client_id', '=', 'client_quiz_set_request.fk_client_id')
                    queryBuilder.leftJoin('quiz_set', 'quiz_set.quiz_set_id', '=', 'client_quiz_set_request.fk_quiz_set_id')
                    queryBuilder.andWhere('client_quiz_set_request_is_deleted', '=', false)
                    queryBuilder.andWhere('quiz_set_is_deleted', '=', false)
                    dataTable(queryBuilder, dataTableOptions);
                })
                .orderBy(dataTableOptions.orderBy.column || 'client_quiz_set_request_created_at', dataTableOptions.orderBy.direction || 'ASC')
                .then((rows) => {
                    qb('client_quiz_set_request')
                    .select(qb.raw('count(`client_quiz_set_request_id`) as count'))
                    .leftJoin('client', 'client.client_id', '=', 'client_quiz_set_request.fk_client_id')
                    .leftJoin('quiz_set', 'quiz_set.quiz_set_id', '=', 'client_quiz_set_request.fk_quiz_set_id')
                    .andWhere('client_quiz_set_request_is_deleted', '=', false)
                    .andWhere('quiz_set_is_deleted', '=', false)
                    .then((countRows)=>{
                        resolve(modelOutput({rows, countRows}));
                    })
                    .catch((error) => {
                        resolve(modelOutput(null, error));
                        throw error
                    })
                })
                .catch((error) => {
                    resolve(modelOutput(null, error));
                    throw new Error(error)
                })
            }
        )
    },


    
};


module.exports = Model;