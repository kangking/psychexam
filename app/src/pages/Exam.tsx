// material
import { Container, Typography } from '@material-ui/core';
// components
import _ from 'lodash';
import React from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { GetExam } from '../actions/ExamActions';
import Page from '../components/Page';
import Question from '../components/question/Question';
// ----------------------------------------------------------------------

interface Props {
  props: any;
}

const ExamPage: React.FC<Props> = (props) => {
  const dispatch = useDispatch();
  const examState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Exam')) {
      return state.Exam;
    }
    return null;
  });

  const examId: number = Number(useParams().id);
  const maxQuestionCount = 5;

  React.useEffect(() => {
    dispatch(GetExam(examId));
  }, []);

  const { questions } = examState.data;

  const ShowData = () => {
    if (!_.isEmpty(questions)) {
      let counter = 0;
      return (
        <TransitionGroup exit={true} className="todo-list">
          {questions.map((question: any) => {
            if (question.answer === null || question.answer === '') {
              if (counter < maxQuestionCount) {
                counter++;
                return (
                  <CSSTransition key={question.question_id} timeout={500} classNames="item">
                    <Question questionId={question.question_id} exam={examState} />
                  </CSSTransition>
                );
              }
            }
            return null;
          })}
        </TransitionGroup>
      );
    }

    if (examState.loading) {
      return <p>Loading...</p>;
    }

    if (examState.errorMsg !== '') {
      return <p>{examState.errorMsg}</p>;
    }

    return <p>unable to get quiz data</p>;
  };

  return (
    <Page title="Exam">
      <Container maxWidth="xl">
        <Typography variant="h3" component="h1" paragraph>
          {examState.quiz_name}
        </Typography>
        {ShowData()}
      </Container>
    </Page>
  );
};

export default ExamPage;
