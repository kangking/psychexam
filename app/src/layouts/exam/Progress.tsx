// material
import { Paper, Box, Typography, LinearProgress } from '@material-ui/core';
// components
import _ from 'lodash';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { SetAnswer } from '../../actions/QuestionActions';
// ----------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%'
  },
  paper: {
    margin: theme.spacing(0),
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.primary
  }
}));

interface ChoiceOptions {
  choice_legend: string;
  choice_value: any;
}

interface Props {
  choice: ChoiceOptions;
  letter: string;
}

const Choice: React.FC<Props> = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useNavigate();

  const examState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Exam')) {
      return state.Exam;
    }
    return null;
  });
  const { questions } = examState.data;

  React.useEffect(() => {
    calcProgress();
  }, []);

  const calcProgress = () => {
    let progressCounter = 0;
    const completeCount = 100;

    if (!_.isEmpty(questions)) {
      questions.forEach((question: any) => {
        if (question.answer !== null && question.answer !== '') {
          progressCounter++;
        }
      });
      const percent = (progressCounter / examState.data.questions.length) * 100;
      if (Math.floor(percent) == completeCount) {
        setTimeout(() => {
          history.push('/client/quiz-result');
        }, 1500);
      }
      return percent;
    }
    return 0;
  };

  return (
    <Paper className={classes.paper} elevation={2}>
      <Box display="flex" alignItems="center">
        <Box width="100%" mr={1}>
          <LinearProgress variant="determinate" value={calcProgress()} />
        </Box>
        <Box minWidth={35}>
          <Typography variant="body2" color="textSecondary">
            {`${Math.round(calcProgress())}%`}
          </Typography>
        </Box>
      </Box>
    </Paper>
  );
};

export default Choice;
