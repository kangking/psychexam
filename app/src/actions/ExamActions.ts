import axios from 'axios';

export const GetExam =
  (examId: number = 1) =>
  async (dispatch: any) => {
    try {
      dispatch({
        type: 'EXAM_LOADING'
      });

      const response: any = await FetchQuiz(examId);

      dispatch({
        type: 'EXAM_SUCCESS',
        payload: response.data,
        examId
      });
    } catch (e) {
      dispatch({
        type: 'EXAM_FAIL'
      });
    }
  };

async function FetchQuiz(id: number) {
  const url = `http://psychapi.kosmokode.com/api/quiz?id=${id}`;

  const response = await axios.get(url);
  response.data.questions.forEach((question: any) => {
    question.answer = null;
  });

  return response;
}
