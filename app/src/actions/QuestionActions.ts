export const IncrementQuestionIdx = () => async (dispatch: any) => {
  dispatch({
    type: 'QUESTION_CURRENT_IDX_INCREMENT'
  });
};

export const DecrementQuestionIdx = () => async (dispatch: any) => {
  dispatch({
    type: 'QUESTION_CURRENT_IDX_DECREMENT'
  });
};

export const SetAnswer = (answer: any, questionId: number) => async (dispatch: any) => {
  dispatch({
    type: 'QUESTION_SET_ANSWER',
    answer,
    questionId
  });
  dispatch({
    type: 'ANSWER_ADD',
    answer: { value: answer, number: questionId }
  });
};
