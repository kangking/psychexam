// material
import { Container, Typography, RadioGroup, FormControl, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import _ from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SetAnswer } from '../../actions/QuestionActions';
import Choice from './Choice';
// ----------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  formControl: {
    width: '100%'
  }
}));

interface Props {
  questionId: number;
}

const TypeMultipleChoice: React.FC<Props> = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const examState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Exam')) {
      return state.Exam;
    }
    return null;
  });

  const { questions } = examState.data;
  const question = questions.find((question: any) => question.question_id === props.questionId);

  const handleChange = (e: any) => {
    dispatch(SetAnswer(e.target.value, props.questionId));
  };

  let abcCounter = 9;
  const choices = question.choices.map((choice: any) => {
    abcCounter++;
    return <Choice choice={choice} key={choice.choice_id} letter={abcCounter.toString(36)} />;
  });

  const ShowData = () => {
    if (!_.isEmpty(question.choices)) {
      return choices;
    }
    return <p>unable to load multiple choice typed question</p>;
  };

  return (
    <Container maxWidth="xl" style={{ padding: 0 }}>
      <FormControl component="fieldset" className={classes.formControl} style={{ padding: 0 }}>
        <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
          value=""
          onChange={handleChange}
          style={{ padding: 0 }}
        >
          <Grid container spacing={3}>
            {ShowData()}
          </Grid>
        </RadioGroup>
      </FormControl>
    </Container>
  );
};

export default TypeMultipleChoice;
