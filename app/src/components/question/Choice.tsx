// material
import {
  Radio,
  FormControlLabel,
  Paper,
  Box,
  Grid,
  Container,
  Typography
} from '@material-ui/core';
// components
import _ from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { SetAnswer } from '../../actions/QuestionActions';
// ----------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  },
  radioWrap: {
    // flexGrow: 1,
  },
  radio: {
    '& span': {
      fontSize: '.9rem'
    },
    '& MuiFormControlLabel-root': {
      padding: '2px'
    }
  },
  letter: {
    verticalAlign: 'middle',
    margin: '16px',
    textTransform: 'uppercase',
    textAlign: 'left'
  }
}));

interface ChoiceOptions {
  choice_legend: string;
  choice_value: any;
}

interface Props {
  choice: ChoiceOptions;
  letter: string;
}

const Choice: React.FC<Props> = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  React.useEffect(() => {}, []);

  const ShowData = () => {
    if (props.choice) {
      return (
        <FormControlLabel
          className={classes.radio}
          style={{
            width: '100%',
            padding: '6px',
            marginRight: '0',
            marginLeft: '0',
            minHeight: '60px',
            textTransform: 'capitalize'
          }}
          control={<Radio className={classes.radio} />}
          value={props.choice.choice_legend}
          label={props.choice.choice_value}
          labelPlacement="end"
        />
      );
    }
    return <p>unable to load choice</p>;
  };

  return (
    <Grid item xs={12} sm={6} md={4} className={classes.root}>
      <Box display="flex" alignItems="center">
        {/* <Box minWidth={50}>
          <Typography variant="h5" className={classes.letter}>
            {props.letter}
          </Typography>
        </Box> */}
        <Box width="100%">
          <Paper variant="outlined" className={classes.root} elevation={2}>
            {ShowData()}
          </Paper>
        </Box>
      </Box>
    </Grid>
  );
};

export default Choice;
