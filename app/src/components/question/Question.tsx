// material
import { Container, Typography, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import _ from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TypeMultipleChoice from './TypeMultipleChoice';
// ----------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  question: {
    color: theme.palette.text.primary,
    textTransform: 'capitalize',
    fontWeight: 'bolder',
    fontSize: '1.4rem',
    textAlign: 'left',
    paddingTop: '8px',
    paddingBottom: '20px',
    marginBottom: '16px',
    borderBottom: '1px dashed #ddd'
  },
  paper: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.primary,
    border: 3
  }
}));

interface Props {
  questionId: any;
  exam: any;
}

const Question: React.FC<Props> = (props) => {
  const classes = useStyles();
  const question = props.exam.data.questions.find(
    (question: any) => question.question_id === props.questionId
  );
  const questionLabel = question.question_value.toLowerCase();

  const ShowData = () => {
    if (!_.isEmpty(question)) {
      const renderQuestionByType = () => <TypeMultipleChoice questionId={question.question_id} />;
      return (
        <div>
          <Typography variant="h5" className={classes.question}>
            {question.question_value.toLowerCase()}
          </Typography>
          {renderQuestionByType()}
        </div>
      );
    }

    return <p>unable to load question</p>;
  };

  return (
    <Container maxWidth="xl">
      <Paper
        className={classes.paper}
        sx={{
          p: 1,
          boxShadow: (theme) => theme.customShadows.z8,
          '&:hover img': { transform: 'scale(1.1)' }
        }}
      >
        {ShowData()}
      </Paper>
    </Container>
  );
};

export default Question;
