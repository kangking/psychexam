const initialState = {
  currentQuestionIdx: 0
};

interface ActionOptions {
  type: string;
  questionIdx: number;
}

const QuestionReducer = (state = initialState, action: ActionOptions) => {
  switch (action.type) {
    case 'QUESTION_CURRENT_SET': {
      return {
        ...state,
        currentQuestionIdx: action.questionIdx
      };
    }

    case 'QUESTION_CURRENT_IDX_INCREMENT': {
      return {
        ...state,
        currentQuestionIdx: state.currentQuestionIdx + 1
      };
    }

    case 'QUESTION_CURRENT_IDX_DECREMENT': {
      return {
        ...state,
        currentQuestionIdx: state.currentQuestionIdx > 0 ? state.currentQuestionIdx - 1 : 0
      };
    }

    default:
      return state;
  }
};

export default QuestionReducer;
