import { combineReducers } from 'redux';
import Exam from './ExamReducer';
import Question from './QuestionReducer';

const RootReducer = combineReducers({
  Exam,
  Question
});

export default RootReducer;
