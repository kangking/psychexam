interface StateInterface {
  loading: boolean;
  data: any;
  errorMsg: string;
}

interface ActionOptions {
  type: string;
  payload: any;
  answer: string;
  questionId: number;
}

const initialState: StateInterface = {
  loading: false,
  data: {},
  errorMsg: ''
};

const ExamReducer = (state = initialState, action: ActionOptions) => {
  switch (action.type) {
    case 'EXAM_LOADING': {
      return {
        ...state,
        loading: true,
        errorMsg: ''
      };
    }
    case 'EXAM_FAIL': {
      return {
        ...state,
        loading: false,
        errorMsg: 'unable to get quiz'
      };
    }
    case 'EXAM_SUCCESS': {
      return {
        ...state,
        loading: false,
        data: action.payload,
        errorMsg: ''
      };
    }

    case 'QUESTION_SET_ANSWER': {
      const questions = Object.assign([], state.data.questions);
      const found = questions.find((question: any) => question.question_id === action.questionId);
      if (found) {
        found.answer = action.answer;
      }

      return {
        ...state,
        loading: false,
        data: {
          ...state.data,
          questions
        },
        errorMsg: ''
      };
    }

    default:
      return state;
  }
};

export default ExamReducer;
