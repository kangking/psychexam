// material
import { Paper, Typography, Grid, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
// components
import Page from '../components/Page';
import LoginForm from '../components/client/clientLoginForm';

const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    paddingTop: theme.spacing(4),
    textAlign: 'left',
    color: theme.palette.text.primary,
    border: 3,
    position: 'relative'
  },
  root: {
    minHeight: '100vh',
    backgroundColor: theme.palette.grey[300]
  }
}));

const AdminLogin = () => {
  const classes = useStyles();

  return (
    <Page title="Registration Form" className={classes.root}>
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        maxWidth="md"
        style={{ height: '100vh' }}
      >
        <Grid item xs={12} sm={8} md={6} lg={4}>
          <Paper
            className={`${classes.paper}`}
            sx={{
              p: 1,
              boxShadow: (theme) => theme.customShadows.z8,
              '&:hover img': { transform: 'scale(1.1)' }
            }}
          >
            <Typography variant="h5" style={{ textAlign: 'center' }} mb={6}>
              PeoplePsyche Login
            </Typography>
            <LoginForm />
            <Typography sx={{ mt: 2 }} variant="body1" paragraph>
              Don't have an account? &nbsp;
              <Link to="/register">Go to Registration Page</Link>
            </Typography>
          </Paper>
        </Grid>
      </Grid>
    </Page>
  );
};

export default AdminLogin;
