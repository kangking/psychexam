import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
// material
import { Container, Typography, Box, Paper, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../components/Page';
import axios from '../utils/axios';

import useAuth from '../hooks/useAuth';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  }
}));

export default function ExamPage() {
  const classes = useStyles();
  const { clientLogout } = useAuth();

  useEffect(() => {
    const init = async () => {
      await clientLogout();
    };
    init();
  }, []);

  return (
    <Page title="Exam">
      <Container maxWidth="xl">
        <Typography variant="h5" paragraph>
          Logging out
        </Typography>
      </Container>
    </Page>
  );
}
