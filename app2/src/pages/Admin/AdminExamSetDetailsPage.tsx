import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
import { useParams, Outlet, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// material
import { Container, Link, AppBar, Paper } from '@material-ui/core';
// components
import Page from '../../components/Page';
import ExamSetNavbar from '../../components/admin/examSet/adminExamSetNavbar';

import axios from '../../utils/axios';
import {
  ExamSet,
  Exam,
  ExamSetAsResponse,
  ExamAsResponse,
  ExamSetItemAsResponse
} from '../../@types/exam';

import { DoneReload } from '../../actions/ReloadActions';

function ButtonLink(props: { to: string; children: string | React.ReactNode }) {
  const navigate = useNavigate();

  return (
    <Link
      style={{ marginRight: 10, fontWeight: 'bold', fontSize: '1rem' }}
      component="button"
      variant="body2"
      onClick={() => {
        navigate(props.to);
      }}
    >
      {props.children}
    </Link>
  );
}

export default function ExamSetDetailsPage() {
  const dispatch = useDispatch();

  const [examSet, setExamSet] = useState<ExamSetAsResponse | null>(null);
  const [reload, setReload] = useState(false);
  const examSetId: number = Number(useParams().examSetId);

  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  const fetchExamSet = async () => {
    const response = await axios.get(`/api/admin/quiz-set?quizSetId=${examSetId}`);
    setExamSet(response.data.quizSet.data);
    setReload(false);
    dispatch(DoneReload({ type: 'RELOAD_DONE_EXAM_SET_ITEMS_TABLE' }));
  };

  useEffect(() => {
    // fetchExamSet();
    // checker();
  }, []);

  return (
    <Page title="Dashboard">
      <Container maxWidth="xl">
        <Paper elevation={3} sx={{ px: 2, mb: 2 }}>
          <ExamSetNavbar indicatorColor="secondary" textColor="inherit" />
        </Paper>
        {/* <ButtonLink to="details">Details</ButtonLink>
        <ButtonLink to="examinees">Exam Takers</ButtonLink> */}
        <Outlet />
      </Container>
    </Page>
  );
}
