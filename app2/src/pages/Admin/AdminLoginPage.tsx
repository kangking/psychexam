// react
import React from 'react';
// material
import { Paper, Typography, Grid, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../../components/Page';
import LoginForm from '../../components/admin/adminLoginForm';

import axios from '../../utils/axios';

const useStyles = makeStyles((theme: any) => ({
  paper: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.primary,
    border: 3,
    position: 'relative'
  },
  root: {
    minHeight: '100vh',
    backgroundColor: theme.palette.grey[300]
  }
}));

const AdminLogin = () => {
  const classes = useStyles();

  React.useEffect(() => {
    const initTry = async () => {
      const response = await axios.get('/api/admin/get-logo-file');
      console.log(response);
    };
    initTry();
  }, []);

  return (
    <Page title="Registration Form" className={classes.root}>
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        maxWidth="md"
        style={{ height: '100vh' }}
      >
        <Grid item xs={12} sm={8} md={6} lg={4}>
          <Paper
            className={`${classes.paper}`}
            sx={{
              p: 1,
              boxShadow: (theme) => theme.customShadows.z8,
              '&:hover img': { transform: 'scale(1.1)' }
            }}
          >
            <Typography variant="h5" style={{ textAlign: 'center' }} mb={6}>
              Admin Login
            </Typography>
            <LoginForm />
          </Paper>
        </Grid>
      </Grid>
    </Page>
  );
};

export default AdminLogin;
