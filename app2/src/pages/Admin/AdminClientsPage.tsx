import _ from 'lodash';
// react
import { Outlet } from 'react-router-dom';
// material
import { Container } from '@material-ui/core';
// components
import Page from '../../components/Page';

export default function ClientsPage() {
  return (
    <Page title="Dashboard">
      <Container maxWidth="xl">
        <Outlet />
      </Container>
    </Page>
  );
}
