import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
// material
import { Container, Typography, Box, Paper, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../../components/Page';
// import ClientList from '../../components/admin/adminClientList';
import axios from '../../utils/axios';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  }
}));

export default function ExamPage() {
  const classes = useStyles();

  return (
    <Page title="Dashboard">
      <Container maxWidth="xl">
        <Typography variant="h5" paragraph>
          Admin Dashboard
        </Typography>
        {/* <ClientList /> */}
      </Container>
    </Page>
  );
}
