import _ from 'lodash';
// react
import { useState } from 'react';
// material
import { Container } from '@material-ui/core';
// components
import RequestList from '../../components/admin/requestPermission/adminRequestList';

export default function AdminRequestList() {
  return (
    <Container>
      <RequestList />
    </Container>
  );
}
