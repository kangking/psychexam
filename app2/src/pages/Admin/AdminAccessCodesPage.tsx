import _ from 'lodash';
// react
import { useState } from 'react';
// material
import { Container } from '@material-ui/core';
// components
import AccessCodesList from '../../components/admin/accessCodes/adminAccessCodesList';
import AccessCodesForm from '../../components/admin/accessCodes/adminAccessCodesForm';

export default function AdminAccessCodesPage() {
  console.log('haha');

  return (
    <Container>
      <AccessCodesForm isNew={true} />
      <AccessCodesList />
    </Container>
  );
}
