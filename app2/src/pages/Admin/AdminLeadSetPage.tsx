import _ from 'lodash';
// react
import { Outlet } from 'react-router-dom';
// material
import { Container } from '@material-ui/core';
// components
import Page from '../../components/Page';
import { LeadProvider } from '../../contexts/LeadContext';

export default function LeadSetPage() {
  return (
    <Page title="Dashboard">
      <Container maxWidth="xl">
        <LeadProvider>
          <Outlet />
        </LeadProvider>
      </Container>
    </Page>
  );
}
