import _ from 'lodash';
// react
import React, { useState, useEffect, useRef } from 'react';
// material
import { Container, Typography, Box, Paper, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../components/Page';
import Timer from '../components/Timer';
import axios from '../utils/axios';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  }
}));

export default function ExamPage() {
  const classes = useStyles();
  const [action, setAction] = useState<'start' | 'reset' | null>();

  return (
    <Page title="Testing Page">
      <Container maxWidth="xl">
        <Typography variant="h5" paragraph>
          Experiment
        </Typography>
        <Button
          variant="contained"
          name="start"
          onClick={() => {
            setAction('start');
          }}
        >
          Start
        </Button>
        <Button
          variant="contained"
          name="reset"
          onClick={() => {
            setAction('reset');
          }}
        >
          Reset
        </Button>
      </Container>
    </Page>
  );
}
