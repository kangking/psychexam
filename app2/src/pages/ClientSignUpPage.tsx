// material
import { Container, Typography, Paper, Button } from '@material-ui/core';
import { useNavigate, Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../components/Page';
import RegisterForm from '../components/client/clientRegistrationForm';

const useStyles = makeStyles((theme) => ({
  paper: {
    margin: theme.spacing(0),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.primary,
    border: 3,
    position: 'relative'
  },
  root: {
    minHeight: '100vh',
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[300]
  }
}));

const ClientSignup = () => {
  const classes = useStyles();
  const navigate = useNavigate();

  return (
    <Page title="Registration Form" className={classes.root}>
      <Container maxWidth="md">
        <Typography variant="body1" paragraph>
          Already registered? &nbsp;
          <Link to="/login">Go to Login Page</Link>
        </Typography>
        <Paper
          className={`${classes.paper}`}
          sx={{
            p: 1,
            boxShadow: (theme) => theme.customShadows.z8,
            '&:hover img': { transform: 'scale(1.1)' }
          }}
        >
          <RegisterForm style={{ paddingTop: 4 }}>
            <Typography variant="h5" style={{ marginBottom: '32px' }}>
              Registration Form
            </Typography>
          </RegisterForm>
        </Paper>
      </Container>
    </Page>
  );
};

export default ClientSignup;
