// material
import {
  Container,
  Grid,
  Box,
  Typography,
  AppBar,
  Tabs,
  Tab,
  Stack,
  Card
} from '@material-ui/core';
// components
import _ from 'lodash';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { experimentalStyled as styled, makeStyles } from '@material-ui/core/styles';

import { useDispatch, useSelector } from 'react-redux';
import { GetResult, GetExam } from '../actions/ExamActions';

import Page from '../components/Page';
import ResultInterpretation from '../components/examResult/InterpretationResult';
import ResultGeneric from '../components/examResult/GenericResult';

// Hooks
// import useAuth from '../hooks/useAuth';

import answersStateA from '../testAnswers/testA.json';
import answersStateB from '../testAnswers/testB.json';
import answersStateC from '../testAnswers/testC.json';
// ----------------------------------------------------------------------

const style = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexWrap: 'wrap',
  '& > *': { mx: '8px !important' }
} as const;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  message: {
    lineHeight: '1.8rem',
    marginBottom: theme.spacing(2)
  }
}));

interface Props {
  props: any;
}

const ExamResult: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [type, setTypeValue] = React.useState<string>('generic');
  const examState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Exam')) {
      return state.Exam;
    }
    return null;
  });
  const answersState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'AnswerSheet')) {
      return state.AnswerSheet;
    }
    return null;
  });

  React.useEffect(() => {
    if (answersState.isDone) dispatch(GetResult(answersState));
    else navigate(`/exam`);
  }, []);

  const ShowData = () => {
    if (answersState.isDone) {
      if (type === 'generic') return <ResultGeneric exam={examState} />;
      if (type === 'interpretation') return <ResultInterpretation exam={examState} />;
      return <p>Failed to load</p>;
    }
    return <p>Failed to load</p>;
  };

  return (
    <Page title="Exam">
      <Box sx={{ mb: 5 }}>{ShowData()}</Box>
    </Page>
  );
};

export default ExamResult;
