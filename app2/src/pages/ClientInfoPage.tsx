// material
import { Container, Typography } from '@material-ui/core';
// components
import Page from '../components/Page';

interface Props {
  props: any;
}

const ClientInfo: React.FC<Props> = (props) => (
  <Page title="Exam">
    <Container maxWidth="xl">
      <Typography variant="h5">Info Sheet</Typography>
    </Container>
  </Page>
);

export default ClientInfo;
