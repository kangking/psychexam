import { sentenceCase } from 'change-case';
// material
import { Container, Typography, Box, Button, Snackbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import _ from 'lodash';
import moment from 'moment';
import React, { useState, useRef, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { GetExam } from '../actions/ExamActions';
import useTimer from '../hooks/useTimer';
import Page from '../components/Page';
import Question from '../components/question/Question';
import Progress from '../components/question/Progress';
import Timer from '../components/Timer';

import { SetDone } from '../actions/AnswerSheetActions';

import StartModal from '../components/exam/examStartModal';
import StopModal from '../components/exam/examStopModal';

import axios from '../utils/axios';
import { setSentenceCase } from '../utils/string';

import './exam.css';
// ----------------------------------------------------------------------

// const initBeforeUnLoad = (showExitPrompt: any) => {
//   window.onbeforeunload = (event) => {
//     // Show prompt based on state
//     if (showExitPrompt) {
//       const e = event || window.event;
//       e.preventDefault();
//       if (e) {
//         e.returnValue = '';
//       }
//       return '';
//     }
//     return '';
//   };
// };

const ExamPage = () => {
  // const classes = useStyles();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { duration, startTimer, resetTimer, setDuration, stopTimer } = useTimer();
  const examSetId: number = Number(useParams().quizSetId);

  const startCountdown = useRef(false);
  const timesUp = useRef(false);
  const [canBegin, setCanBegin] = useState(false);
  const [openStartModal, setOpenStartModal] = useState(true);
  const [openStopModal, setOpenStopModal] = useState(false);
  const [isAlreadyDone, setIsAlreadyDone] = useState(false);
  const [enableProgress, setEnableProgress] = useState(false);

  const [showExitPrompt, setShowExitPrompt] = useState(false);

  const examState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Exam')) {
      return state.Exam;
    }
    return null;
  });

  const examSlug: string = String(useParams().slug);
  const examSetItemId: number = Number(useParams().quizSetItemId);
  const maxQuestionCount = 5;

  React.useEffect(() => {
    dispatch(GetExam(examSlug, examSetItemId));
    setEnableProgress(false);
    startCountdown.current = false;
  }, [examSlug, examSetItemId]);

  const { questions, quiz, success, canTake } = examState.data;

  React.useEffect(() => {
    if (canTake === false) {
      navigate(`/exam-sets/${examSetId}`);
    }
  }, []);

  React.useEffect(() => {
    if (quiz) {
      resetTriggers();
      setOpenStartModal(true);
      if (!quiz.data.quiz_is_done) {
        setIsAlreadyDone(false);
        setCanBegin(true);
        setDuration(Number(quiz.data.quiz_duration));
      } else {
        setIsAlreadyDone(true);
        setCanBegin(false);
      }
    } else setCanBegin(false);
  }, [quiz]);

  React.useEffect(() => {
    if (quiz) {
      if (startCountdown.current === true) startTimer(quiz.data.quiz_duration);
      setEnableProgress(true);
    }
  }, [startCountdown.current]);

  React.useEffect(() => {
    if (quiz)
      if (quiz.data.quiz_duration > 0)
        if (duration <= 0) {
          timesUp.current = true;
        }
  }, [duration]);

  React.useEffect(() => {
    if (timesUp.current === true) {
      stopTimer();
      setOpenStopModal(true);
      dispatch(SetDone(true));
      setTimeout(() => {
        navigate(`result`);
      }, 4000);
    }
  }, [timesUp.current]);

  const resetTriggers = () => {
    stopTimer();
    console.log('reset exam page');
    setEnableProgress(false);
    startCountdown.current = false;
    timesUp.current = false;
  };

  const showItems = (arr: [], length: number) => {
    let counter = 0;
    const elements = arr.map((item: any) => {
      if (item.answer === null || item.answer === '') {
        if (counter < length) {
          counter++;
          return (
            <CSSTransition key={item.question_id} timeout={300} classNames="item">
              <Question questionId={item.question_id} exam={examState} />
            </CSSTransition>
          );
        }
      }
      return null;
    });
    return (
      <TransitionGroup exit={true} className="todo-list">
        {elements}
      </TransitionGroup>
    );
  };

  const ShowData = () => {
    if (!_.isEmpty(questions)) {
      return showItems(questions, maxQuestionCount);
    }

    if (examState.loading) {
      return <p>Loading...</p>;
    }

    if (examState.errorMsg !== '') {
      return <p>{examState.errorMsg}</p>;
    }

    return <p>unable to get quiz data</p>;
  };

  const ShowTitle = () => {
    if (Object.prototype.hasOwnProperty.call(examState.data, 'quiz')) {
      if (Object.prototype.hasOwnProperty.call(examState.data.quiz.data, 'quiz_name')) {
        if (examState.data.quiz.data.quiz_name) {
          return (
            <div>
              <Typography
                variant="h5"
                paragraph
                style={{ fontWeight: 'normal', textTransform: 'capitalize' }}
              >
                {setSentenceCase(examState.data.quiz.data.quiz_name)}
              </Typography>
            </div>
          );
        }
        return null;
      }
      return null;
    }
    return null;
  };

  const ShowSlug = () => {
    if (Object.prototype.hasOwnProperty.call(examState.data, 'quiz')) {
      if (Object.prototype.hasOwnProperty.call(examState.data.quiz.data, 'quiz_slug')) {
        if (examState.data.quiz.data.quiz_slug) {
          return examState.data.quiz.data.quiz_slug;
        }
        return null;
      }
      return null;
    }
    return null;
  };

  const ShowInstruction = () => {
    if (Object.prototype.hasOwnProperty.call(examState.data, 'quiz')) {
      if (Object.prototype.hasOwnProperty.call(examState.data.quiz.data, 'quiz_instruction')) {
        if (examState.data.quiz.data.quiz_instruction) {
          return examState.data.quiz.data.quiz_instruction;
        }
        return null;
      }
      return null;
    }
    return null;
  };

  const ShowTimer = () => {
    if (!_.isUndefined(quiz)) {
      if (quiz.data.quiz_duration > 0) {
        return (
          <Box mt={2}>
            <Timer duration={duration} />
          </Box>
        );
      }
      return null;
    }
    return null;
  };

  return (
    <Page title="Exam">
      <Container maxWidth="xl">
        {ShowTitle()}
        {enableProgress ? <Progress /> : null}
        {ShowTimer()}
        {ShowData()}
        <StartModal
          quizSlug={ShowSlug()}
          open={openStartModal}
          duration={duration}
          isDone={isAlreadyDone}
          instruction={ShowInstruction()}
        >
          <Button
            color="primary"
            variant="contained"
            style={{ textAlign: 'center' }}
            disabled={!canBegin}
            onClick={() => {
              startCountdown.current = true;
              setOpenStartModal(false);
            }}
          >
            Begin Test
          </Button>
        </StartModal>
        <StopModal open={openStopModal} />
      </Container>
    </Page>
  );
};

export default ExamPage;
