// React
import { Link, useNavigate } from 'react-router-dom';
// material
import { Container, Typography, Button, Paper, Box, Grid } from '@material-ui/core';
// components
import Page from '../components/Page';

const ClientHomePage = () => {
  const navigate = useNavigate();

  return (
    <Page title="Exam">
      <Grid
        container
        maxWidth="sm"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '100vh' }}
      >
        <Paper
          sx={{ padding: (theme) => theme.spacing(2) }}
          elevation={0}
          style={{ minHeight: 'unset' }}
        >
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12}>
              <Typography variant="h4" align="center" style={{ marginBottom: 12 }}>
                Welcome to the Psych App
              </Typography>
            </Grid>

            <Grid item xs={12} spacing={2} justifyContent="center" style={{ textAlign: 'center' }}>
              <Button
                variant="outlined"
                onClick={() => {
                  navigate('/login');
                }}
                style={{ marginRight: 8 }}
              >
                Login
              </Button>
              <Button
                variant="contained"
                onClick={() => {
                  navigate('/register');
                }}
              >
                Register
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Page>
  );
};

export default ClientHomePage;
