import { createContext, ReactNode, useEffect, useReducer } from 'react';
import axios from '../utils/axios';
import { LeadSetAsResponse, LeadAsResponse, LeadSetForm, LeadContextType } from '../@types/lead';
import { TableParams } from '../@types/table';

enum Types {
  Initial = 'INITIALIZE',
  LeadSetTableLoading = 'LEAD_SET_TABLE_LOADING',
  LeadSetTableSuccess = 'LEAD_SET_TABLE_SUCCESS',
  LeadSetTableFail = 'LEAD_SET_TABLE_FAIL'
}

interface InitialState {
  leadSetTable: LeadSetAsResponse[];
  leadTable: LeadAsResponse[];
  leadSet: LeadSetAsResponse | null;
  lead: LeadAsResponse | null;
  emailBlastResults: any[];
  isLoadingLeadSetTable: boolean;
  isLoadingLeadTable: boolean;
  isLoadingLeadSet: boolean;
  leadSetAddForm: any;
}

const initialState: InitialState = {
  leadSetTable: [],
  leadTable: [],
  leadSet: null,
  lead: null,
  emailBlastResults: [],
  isLoadingLeadSetTable: false,
  isLoadingLeadTable: false,
  isLoadingLeadSet: false,
  leadSetAddForm: null
};

const LeadReducer = (state: any, action: any) => {
  switch (action.type) {
    // LEAD SET
    case 'LEAD_SET_TABLE_LOADING':
      return {
        ...state,
        isLoadingLeadSetTable: true
      };
    case 'LEAD_SET_TABLE_SUCCESS':
      return {
        ...state,
        leadSetTable: action.leadSetTable,
        isLoadingLeadSetTable: false
      };
    case 'LEAD_SET_TABLE_FAIL':
      return {
        ...state,
        isLoadingLeadSetTable: false
      };
    case 'LEAD_SET_LOADING':
      return {
        ...state,
        isLoadingLeadSet: true
      };
    case 'LEAD_SET_SUCCESS':
      return {
        ...state,
        // leadSet: action.leadSet,
        leadSet: { ...action.leadSet },
        isLoadingLeadSet: false
      };
    case 'LEAD_SET_FAIL':
      return {
        ...state,
        isLoadingLeadSet: false
      };

    // LEADS
    case 'LEAD_BLAST_EMAIL_ACCESSCODE_LOADING':
      return {
        ...state,
        isSendingEmailBlast: true
      };
    case 'LEAD_BLAST_EMAIL_ACCESSCODE_SUCCESS':
      return {
        ...state,
        isSendingEmailBlast: false,
        emailBlastResults: [...action.emailStatus]
      };
    case 'LEAD_BLAST_EMAIL_ACCESSCODE_FAIL':
      return {
        ...state,
        isSendingEmailBlast: false
      };
    case 'LEAD_TABLE_LOADING':
      return {
        ...state,
        isLoadingLeadTable: true
      };
    case 'LEAD_TABLE_SUCCESS':
      return {
        ...state,
        leadTable: action.leadTable,
        isLoadingLeadTable: false
      };
    case 'LEAD_TABLE_FAIL':
      return {
        ...state,
        isLoadingLeadTable: false
      };
    case 'RELOAD_LEAD_TABLE':
      return {
        ...state,
        isReloadingLeadTable: true
      };
    case 'RELOAD_DONE_LEAD_TABLE':
      return {
        ...state,
        isReloadingLeadTable: false
      };
    default:
      return state;
  }
};

const LeadContext = createContext<LeadContextType | null>(null);

function LeadProvider({ children }: { children: ReactNode }) {
  const [state, dispatch] = useReducer(LeadReducer, initialState);

  const getLeadSetTable = async (params: TableParams) => {
    try {
      dispatch({
        type: 'LEAD_SET_TABLE_LOADING'
      });

      const response = await axios.get('/api/admin/lead-set-table', { params });

      if (response.data.success) {
        dispatch({
          type: 'LEAD_SET_TABLE_SUCCESS',
          leadSetTable: response.data.leadSets.data
        });
      }
    } catch (error) {
      dispatch({
        type: 'LEAD_SET_TABLE_FAIL'
      });
      throw error;
    }
  };

  interface LeadTableParams extends TableParams {
    leadSetId: number;
  }
  const getLeadTable = async (params: LeadTableParams, leadSetId: number) => {
    try {
      dispatch({
        type: 'LEAD_TABLE_LOADING'
      });

      params.leadSetId = leadSetId;

      const response = await axios.get('/api/admin/lead-table', { params });

      if (response.data.success) {
        dispatch({
          type: 'LEAD_TABLE_SUCCESS',
          leadTable: response.data.leads.data
        });
      }
    } catch (error) {
      dispatch({
        type: 'LEAD_TABLE_FAIL'
      });
      throw error;
    }
  };

  const getLeadSet = async (params: { leadSetId: number }) => {
    try {
      dispatch({
        type: 'LEAD_SET_LOADING'
      });
      const response = await axios.get('/api/admin/lead-set', { params });

      if (response.data.success) {
        dispatch({
          type: 'LEAD_SET_SUCCESS',
          leadSet: response.data.leadSet.data
        });
        console.log('CTX response:', response.data.leadSet.data);
        console.log('CTX state:', state.leadSet);
      }
    } catch (error) {
      dispatch({
        type: 'LEAD_SET_FAIL'
      });
      throw error;
    }
    dispatch({
      type: 'RELOAD_DONE_LEAD_TABLE'
    });
  };

  const reloadLeadTable = (doReload: boolean = true) => {
    if (doReload) {
      dispatch({
        type: 'RELOAD_LEAD_TABLE'
      });
    } else {
      dispatch({
        type: 'RELOAD_DONE_LEAD_TABLE'
      });
    }
  };

  const sendEmailBlast = async (email: string | null, leadSetId: number | null) => {
    const payload = { email, leadSetId };
    const response = await axios.post('/api/admin/email-registration-accesscode', payload);

    if (response.data.success) {
      dispatch({
        type: 'LEAD_BLAST_EMAIL_ACCESSCODE_SUCCESS',
        emailStatus: response.data.emailStatus.data
      });
    } else {
      dispatch({
        type: 'LEAD_BLAST_EMAIL_ACCESSCODE_FAIL'
      });
      throw new Error(response.data.error);
    }

    dispatch({
      type: 'LEAD_BLAST_EMAIL_ACCESSCODE_DONE'
    });
  };

  const providerValue = {
    ...state,
    getLeadSetTable,
    getLeadTable,
    getLeadSet,
    reloadLeadTable,
    sendEmailBlast
  };

  return <LeadContext.Provider value={providerValue}>{children}</LeadContext.Provider>;
}

export { LeadContext, LeadProvider };
