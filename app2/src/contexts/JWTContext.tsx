import { createContext, ReactNode, useEffect, useReducer } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { SetClient } from '../actions/ClientActions';
// utils
import axios from '../utils/axios';
import { isValidToken, setSession, determineAccountType } from '../utils/jwt';
// @types
import {
  ActionMap,
  AuthState,
  AuthUser,
  JWTContextType,
  RegisterClientType
} from '../@types/authentication';

// ----------------------------------------------------------------------

enum Types {
  Initial = 'INITIALIZE',
  Login = 'LOGIN',
  LoginAdmin = 'LOGIN_ADMIN',
  Logout = 'LOGOUT',
  Register = 'REGISTER'
}

type JWTAuthPayload = {
  [Types.Initial]: {
    isAuthenticated: boolean;
    isAdminAuthenticated: boolean;
    user: AuthUser;
    admin?: any;
  };
  [Types.Login]: {
    user: AuthUser;
    admin?: any;
  };
  [Types.LoginAdmin]: {
    user: any;
    admin?: any;
  };
  [Types.Logout]: undefined;
  [Types.Register]: {
    user: AuthUser;
    admin?: any;
  };
};

export type JWTActions = ActionMap<JWTAuthPayload>[keyof ActionMap<JWTAuthPayload>];

const initialState: AuthState = {
  isAuthenticated: false,
  isAdminAuthenticated: false,
  isInitialized: false,
  user: null,
  admin: null
};

const JWTReducer = (state: AuthState, action: JWTActions) => {
  switch (action.type) {
    case 'INITIALIZE':
      return {
        isAuthenticated: action.payload.isAuthenticated,
        isAdminAuthenticated: action.payload.isAdminAuthenticated,
        isInitialized: true,
        user: action.payload.user
      };
    case 'LOGIN':
      return {
        ...state,
        isAuthenticated: true,
        isAdminAuthenticated: false,
        user: action.payload.user,
        admin: null
      };
    case 'LOGIN_ADMIN':
      return {
        ...state,
        isAuthenticated: false,
        isAdminAuthenticated: true,
        user: null,
        admin: action.payload.admin
      };
    case 'LOGOUT':
      return {
        ...state,
        isAuthenticated: false,
        isAdminAuthenticated: false,
        user: null,
        admin: null
      };

    case 'REGISTER':
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user
      };

    default:
      return state;
  }
};

const AuthContext = createContext<JWTContextType | null>(null);

function AuthProvider({ children }: { children: ReactNode }) {
  const navigate = useNavigate();
  const [state, dispatch] = useReducer(JWTReducer, initialState);
  const dispatchRedux = useDispatch();

  useEffect(() => {
    const initialize = async () => {
      try {
        const accessToken = window.localStorage.getItem('accessToken');

        if (accessToken && isValidToken(accessToken)) {
          setSession(accessToken);
          const accountType = determineAccountType(accessToken);

          if (accountType == 'client') {
            const response = await axios.get('/api/client');
            const { client } = response.data;

            dispatch({
              type: Types.Initial,
              payload: {
                isAdminAuthenticated: false,
                isAuthenticated: true,
                user: client.data
              }
            });
          }

          if (accountType == 'admin') {
            const response = await axios.get('/api/admin');
            const { admin } = response.data;

            dispatch({
              type: Types.Initial,
              payload: {
                isAdminAuthenticated: true,
                isAuthenticated: false,
                user: null,
                admin: admin.data
              }
            });
          }
        } else {
          dispatch({
            type: Types.Initial,
            payload: {
              isAdminAuthenticated: false,
              isAuthenticated: false,
              user: null
            }
          });
        }
      } catch (err) {
        console.error(err);
        dispatch({
          type: Types.Initial,
          payload: {
            isAdminAuthenticated: false,
            isAuthenticated: false,
            user: null
          }
        });
      }
    };

    initialize();
  }, []);

  const clientLogin = async (email: string, accessCode: string) => {
    const response = await axios.post('/api/client/login', {
      email,
      accessCode
    });
    const { accessToken, client, success, message } = response.data;
    if (success === true) {
      setSession(accessToken);
      dispatch({
        type: Types.Login,
        payload: {
          user: client.data
        }
      });
      navigate('/exam');
    } else {
      throw new Error(`Login Failed. ${message}`);
    }
  };

  const clientRegister = async ({
    email,
    firstName,
    lastName,
    middleName,
    gender,
    dateOfBirth,
    civilStatus,
    religion,
    homeAddress,
    mobileNumber,
    accessCode
  }: RegisterClientType) => {
    const response = await axios.post('/api/client', {
      email,
      firstName,
      lastName,
      middleName,
      gender,
      dateOfBirth,
      civilStatus,
      religion,
      homeAddress,
      mobileNumber,
      accessCode
    });

    const { accessToken, client, success, message } = response.data;

    if (success === true) {
      // window.localStorage.setItem('accessToken', accessToken);
      setSession(accessToken);
      dispatch({
        type: Types.Register,
        payload: {
          user: client.data
        }
      });
      navigate('/exam');
    } else {
      throw new Error(`Registration Failed. ${message}`);
    }
  };

  const clientLogout = async () => {
    setSession(null);
    dispatch({ type: Types.Logout });
    navigate('/');
  };

  const adminLogin = async (username: string, password: string) => {
    const response = await axios.post('/api/admin/login', {
      username,
      password
    });
    const { accessToken, admin, success, message } = response.data;

    if (success === true) {
      setSession(accessToken);
      dispatch({
        type: Types.LoginAdmin,
        payload: {
          user: null,
          admin: admin.data
        }
      });
      navigate('/admin/dashboard');
    } else {
      throw new Error(`Login Failed. ${message}`);
    }
  };

  const resetPassword = (email: string) => console.log(email);

  const updateProfile = () => {};

  return (
    <AuthContext.Provider
      value={{
        ...state,
        method: 'jwt',
        clientLogin,
        clientRegister,
        clientLogout,
        adminLogin,
        resetPassword,
        updateProfile
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export { AuthContext, AuthProvider };
