import { Icon } from '@iconify/react';
import homeFill from '@iconify/icons-eva/home-fill';
import fileFill from '@iconify/icons-eva/file-fill';
import roundGrain from '@iconify/icons-ic/round-grain';
import bookOpenFill from '@iconify/icons-eva/book-open-fill';
// routes
import { PATH_ADMIN } from '../../routes/paths';

// ----------------------------------------------------------------------

const ICON_SIZE = {
  width: 22,
  height: 22
};

const menuConfig = [
  {
    title: 'Dashboard',
    icon: <Icon icon={homeFill} {...ICON_SIZE} />,
    path: PATH_ADMIN.dashboard
  },
  {
    title: 'Clients',
    icon: <Icon icon={roundGrain} {...ICON_SIZE} />,
    path: PATH_ADMIN.clients
  },
  {
    title: 'Access Codes',
    icon: <Icon icon={roundGrain} {...ICON_SIZE} />,
    path: PATH_ADMIN.accessCodes
  },
  {
    title: 'Requests',
    icon: <Icon icon={roundGrain} {...ICON_SIZE} />,
    path: PATH_ADMIN.requestExamSet
  },
  {
    title: 'Exam Sets',
    icon: <Icon icon={roundGrain} {...ICON_SIZE} />,
    path: PATH_ADMIN.examSets
  },
  {
    title: 'Lead Sets',
    icon: <Icon icon={roundGrain} {...ICON_SIZE} />,
    path: PATH_ADMIN.leadSets
  }
];

export default menuConfig;
