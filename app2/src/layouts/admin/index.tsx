import { Link as ScrollLink } from 'react-scroll';
import { useLocation, Outlet } from 'react-router-dom';
// material
import { experimentalStyled as styled } from '@material-ui/core/styles';
import { Box, Link, Container, Typography } from '@material-ui/core';
// components
//
import MainNavbar from './AdminNavbar';

const APP_BAR_MOBILE = 64;
const APP_BAR_DESKTOP = 88;

const MainStyle = styled('div')(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE + 24,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: APP_BAR_DESKTOP + 24,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  backgroundColor: theme.palette.grey[300]
}));

// ----------------------------------------------------------------------

export default function MainLayout() {
  const { pathname } = useLocation();

  return (
    <>
      <MainNavbar />
      <MainStyle>
        <Outlet />
      </MainStyle>
    </>
  );
}
