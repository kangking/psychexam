// routes
import { PATH_EXAM } from '../../routes/paths';
// components
import SvgIconStyle from '../../components/SvgIconStyle';

// ----------------------------------------------------------------------

const getIcon = (name: string) => (
  <SvgIconStyle src={`/static/icons/navbar/${name}.svg`} sx={{ width: '100%', height: '100%' }} />
);

const ICONS = {
  user: getIcon('ic_user'),
  ecommerce: getIcon('ic_ecommerce'),
  analytics: getIcon('ic_analytics'),
  dashboard: getIcon('ic_dashboard')
};

const sidebarConfig = [
  // GENERAL
  // ----------------------------------------------------------------------
  {
    subheader: 'Tests',
    items: [
      { title: 'Personality Test A', path: PATH_EXAM.exam.personalityTestA, icon: ICONS.analytics },
      { title: 'Personality Test B', path: PATH_EXAM.exam.personalityTestB, icon: ICONS.analytics },
      { title: 'Personality Test C', path: PATH_EXAM.exam.personalityTestC, icon: ICONS.analytics }
    ]
  }
];

export default sidebarConfig;
