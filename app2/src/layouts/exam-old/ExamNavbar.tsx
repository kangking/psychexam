import { Icon } from '@iconify/react';
import menu2Fill from '@iconify/icons-eva/menu-2-fill';
// material
import { alpha, experimentalStyled as styled } from '@material-ui/core/styles';
import { Box, Stack, AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
//
import { useSelector } from 'react-redux';
import { MHidden } from '../../components/@material-extend';

// ----------------------------------------------------------------------

const DRAWER_WIDTH = 280;
const APPBAR_MOBILE = 64;
const APPBAR_DESKTOP = 92;

const RootStyle = styled(AppBar)(({ theme }) => ({
  color: theme.palette.grey[800],
  boxShadow: 'none',
  backdropFilter: 'blur(6px)',
  WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
  backgroundColor: alpha(theme.palette.background.default, 0.72),
  [theme.breakpoints.up('lg')]: {
    width: `calc(100% - ${DRAWER_WIDTH + 1}px)`
  }
}));

const ToolbarStyle = styled(Toolbar)(({ theme }) => ({
  minHeight: APPBAR_MOBILE,
  [theme.breakpoints.up('lg')]: {
    minHeight: APPBAR_DESKTOP,
    padding: theme.spacing(0, 5)
  }
}));
// ----------------------------------------------------------------------

type ExamNavbarProps = {
  onOpenSidebar: VoidFunction;
};

export default function ExamNavbar({ onOpenSidebar }: ExamNavbarProps) {
  const examState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Exam')) {
      return state.Exam;
    }
    return null;
  });

  const ShowExamName = () => {
    if (Object.prototype.hasOwnProperty.call(examState.data, 'quiz')) {
      if (Object.prototype.hasOwnProperty.call(examState.data.quiz.data, 'quiz_name')) {
        return <Typography variant="h5">{examState.data.quiz.data.quiz_name}</Typography>;
      }
      return null;
    }
    return null;
  };

  return (
    <RootStyle>
      <ToolbarStyle>
        <MHidden width="lgUp">
          <IconButton onClick={onOpenSidebar} sx={{ mr: 1, color: 'text.primary' }}>
            <Icon icon={menu2Fill} />
          </IconButton>
        </MHidden>

        {ShowExamName()}

        <Box sx={{ flexGrow: 1 }} />
      </ToolbarStyle>
    </RootStyle>
  );
}
