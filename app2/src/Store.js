// import { fromPairs } from 'lodash';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import RootReducer from './reducers/RootReducer';

const Store = createStore(RootReducer, composeWithDevTools(applyMiddleware(thunk)));
// const Store = createStore(RootReducer);

export default Store;
