import { Suspense, lazy } from 'react';
import { Navigate, useRoutes, useLocation } from 'react-router-dom';
// layouts
import MainLayout from '../layouts/main';
import DashboardLayout from '../layouts/dashboard';
import ExamLayout from '../layouts/exam';
import ClientLayout from '../layouts/client';
import AdminLayout from '../layouts/admin';
import LogoOnlyLayout from '../layouts/LogoOnlyLayout';
// components
import LoadingScreen from '../components/LoadingScreen';

import AuthGuard from '../guards/AuthGuard';
import AuthAdminGuard from '../guards/AuthAdminGuard';
import GuestGuard from '../guards/GuestGuard';
// ----------------------------------------------------------------------

const Loadable = (Component: any) => (props: any) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { pathname } = useLocation();
  const isDashboard = pathname.includes('/dashboard');

  return (
    <Suspense
      fallback={
        <LoadingScreen
          sx={{
            ...(!isDashboard && {
              top: 0,
              left: 0,
              width: 1,
              zIndex: 9999,
              position: 'fixed'
            })
          }}
        />
      }
    >
      <Component {...props} />
    </Suspense>
  );
};

export default function Router() {
  return useRoutes([
    // Client Routes
    {
      path: 'dashboard',
      element: <Navigate to="exam-sets" replace />
    },
    {
      path: 'exam-sets',
      element: (
        <AuthGuard>
          <ExamLayout />
        </AuthGuard>
      ),
      children: [
        { path: '/', element: <ExamSetListPage /> },
        { path: '/:quizSetId', element: <ExamSetPage /> },
        { path: '/:quizSetId/:slug/:quizSetItemId', element: <ExamPage /> },
        { path: '/:quizSetId/:slug/:quizSetItemId/result', element: <ExamResultPage /> }
      ]
    },
    // {
    //   path: 'exam',
    //   element: (
    //     <AuthGuard>
    //       <ExamLayout />
    //     </AuthGuard>
    //   ),
    //   children: [
    //     { path: '/', element: <ClientDashboardPage /> },
    //     { path: '/:slug/:quizSetItemId', element: <ExamPage /> },
    //     { path: '/:slug/:quizSetItemId/result', element: <ExamResultPage /> }
    //   ]
    // },
    {
      path: 'register',
      element: (
        <GuestGuard>
          <ClientSignUpPage />
        </GuestGuard>
      )
    },
    {
      path: 'login',
      element: (
        <GuestGuard>
          <ClientLoginPage />
        </GuestGuard>
      )
    },
    {
      path: '',
      element: (
        <GuestGuard>
          <ClientHomePage />
        </GuestGuard>
      )
    },

    // Admin Routes
    {
      path: 'admin-login',
      element: <AdminLogin />
    },
    {
      path: 'admin',
      element: (
        <AuthAdminGuard>
          <AdminLayout />
        </AuthAdminGuard>
      ),
      children: [
        { path: '/', element: <Navigate to="dashboard" replace /> },
        { path: '/dashboard', element: <AdminDashboard /> },
        {
          path: '/clients',
          element: <AdminClientsPage />,
          children: [
            { path: '/', element: <AdminClientList /> },
            { path: '/:id', element: <AdminClientProfile /> }
          ]
        },
        {
          path: '/access-codes',
          element: <AdminAccessCodesPage />
        },
        {
          path: '/request-exam-set',
          element: <AdminRequestExamSetListPage />
        },
        {
          path: '/exam-sets',
          element: <AdminExamSetsPage />,
          children: [
            { path: '/', element: <AdminExamSetList /> },
            {
              path: '/:examSetId',
              element: <AdminExamSetsDetailsPage />,
              children: [
                { path: '/', element: <Navigate to="details" replace /> },
                { path: '/examinees', element: <AdminExamSetClientList /> },
                {
                  path: '/details',
                  element: <AdminExamSetProfile />
                }
              ]
            }
          ]
        },
        {
          path: '/lead-sets',
          element: <AdminLeadSetsPage />,
          children: [
            { path: '/', element: <AdminLeadSetList /> },
            { path: '/:leadSetId', element: <AdminLeadSetProfile /> }
          ]
        }
      ]
    },

    {
      path: '/logout',
      element: <LogoutPage />
    },
    // {
    //   path: '/',
    //   element: <MainLayout />,
    //   children: [{ path: '/', element: <Navigate to="/register" replace /> }]
    // },
    { path: '*', element: <Navigate to="/404" replace /> }
  ]);
}

// IMPORT COMPONENTS
const LogoutPage = Loadable(lazy(() => import('../pages/LogoutPage')));

//
const ClientHomePage = Loadable(lazy(() => import('../pages/ClientHomePage')));
const ClientLoginPage = Loadable(lazy(() => import('../pages/ClientLoginPage')));
const ClientDashboardPage = Loadable(lazy(() => import('../pages/ClientDashboardPage')));
const ClientSignUpPage = Loadable(lazy(() => import('../pages/ClientSignUpPage')));
const ExamPage = Loadable(lazy(() => import('../pages/Exam')));
const ExamResultPage = Loadable(lazy(() => import('../pages/ExamResult')));
const ExamSetPage = Loadable(lazy(() => import('../pages/ExamSetPage')));
const ExamSetListPage = Loadable(lazy(() => import('../pages/ExamSetListPage')));
// const ExamSetList = Loadable(lazy(() => import('../components/exam/examSetList')));
// const ExamSetInfo = Loadable(lazy(() => import('../components/exam/examSetInfo')));

//
const AdminLogin = Loadable(lazy(() => import('../pages/Admin/AdminLoginPage')));
const AdminDashboard = Loadable(lazy(() => import('../pages/Admin/AdminDashboardPage')));
const AdminAccessCodesPage = Loadable(lazy(() => import('../pages/Admin/AdminAccessCodesPage')));
const AdminClientsPage = Loadable(lazy(() => import('../pages/Admin/AdminClientsPage')));
const AdminClientProfile = Loadable(lazy(() => import('../components/admin/adminClientProfile')));
const AdminClientList = Loadable(lazy(() => import('../components/admin/adminClientList')));
const AdminRequestExamSetListPage = Loadable(
  lazy(() => import('../pages/Admin/AdminRequestExamSetListPage'))
);

const AdminExamSetsPage = Loadable(lazy(() => import('../pages/Admin/AdminExamSetPage')));
const AdminExamSetsDetailsPage = Loadable(
  lazy(() => import('../pages/Admin/AdminExamSetDetailsPage'))
);
const AdminExamSetProfile = Loadable(
  lazy(() => import('../components/admin/examSet/adminExamSetProfile'))
);
const AdminExamSetClientList = Loadable(
  lazy(() => import('../components/admin/examSet/adminExamSetClientList'))
);
const AdminExamSetList = Loadable(
  lazy(() => import('../components/admin/examSet/adminExamSetList'))
);
const AdminLeadSetsPage = Loadable(lazy(() => import('../pages/Admin/AdminLeadSetPage')));
const AdminLeadSetProfile = Loadable(
  lazy(() => import('../components/admin/lead/adminLeadSetProfile'))
);
const AdminLeadSetList = Loadable(lazy(() => import('../components/admin/lead/adminLeadSetList')));

const ExperimentPage = Loadable(lazy(() => import('../pages/Experiment')));
