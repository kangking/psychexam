// ----------------------------------------------------------------------

function path(root: string, sublink: string) {
  return `${root}${sublink}`;
}

const ROOTS_DASHBOARD = '/dashboard';
const ROOTS_EXAM = '/exam-sets';
const ROOTS_ADMIN = '/admin';

// ----------------------------------------------------------------------

export const PATH_DASHBOARD = {
  root: ROOTS_DASHBOARD,
  general: {
    pageOne: path(ROOTS_DASHBOARD, '/one'),
    pageTwo: path(ROOTS_DASHBOARD, '/two'),
    pageThree: path(ROOTS_DASHBOARD, '/three')
  },
  app: {
    root: path(ROOTS_DASHBOARD, '/app'),
    pageFour: path(ROOTS_DASHBOARD, '/app/four'),
    pageFive: path(ROOTS_DASHBOARD, '/app/five'),
    pageSix: path(ROOTS_DASHBOARD, '/app/six')
  }
};

export const PATH_EXAM = {
  root: ROOTS_EXAM,
  exam: {
    personalityTestA: path(ROOTS_EXAM, '/personality-test-a'),
    personalityTestB: path(ROOTS_EXAM, '/personality-test-b'),
    personalityTestC: path(ROOTS_EXAM, '/personality-test-c')
  }
};

export const PATH_ADMIN = {
  dashboard: path(ROOTS_ADMIN, '/dashboard'),
  clients: path(ROOTS_ADMIN, '/clients'),
  accessCodes: path(ROOTS_ADMIN, '/access-codes'),
  examSets: path(ROOTS_ADMIN, '/exam-sets'),
  leadSets: path(ROOTS_ADMIN, '/lead-sets'),
  requestExamSet: path(ROOTS_ADMIN, '/request-exam-set')
};
