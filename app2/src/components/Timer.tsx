import { Box, Typography, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%'
  },
  paper: {
    margin: theme.spacing(0),
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.primary
  }
}));

interface Props {
  duration: number | string;
}

const Timer = (props: Props) => {
  const classes = useStyles();

  return (
    <>
      <Paper className={classes.paper} elevation={2}>
        <Box display="flex" alignItems="center">
          <Typography variant="h6">Time Left: {props.duration}</Typography>
        </Box>
      </Paper>
    </>
  );
};

export default Timer;
