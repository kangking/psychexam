import { sentenceCase } from 'change-case';
// material
import { Radio, FormControlLabel, Paper, Box, Grid, GridSize } from '@material-ui/core';
// components
import _ from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { setSentenceCase } from '../../utils/string';
// ----------------------------------------------------------------------

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary,
    '&:hover': {
      borderSize: 6
    }
  },
  radioWrap: {
    // flexGrow: 1,
  },
  radio: {
    '& span': {
      fontSize: '.9rem'
    },
    '& MuiFormControlLabel-root': {
      padding: '2px'
    }
  },
  letter: {
    verticalAlign: 'middle',
    margin: '16px',
    textTransform: 'uppercase',
    textAlign: 'left'
  }
}));

interface ChoiceOptions {
  choice_legend: string;
  choice_value: any;
}

interface Props {
  choice: ChoiceOptions;
  letter: string;
  lgColSize?: number;
  isGrid?: boolean;
}

const Choice: React.FC<Props> = (props) => {
  const classes = useStyles();
  const lgCol: number = props.lgColSize ? props.lgColSize : 4;

  const ShowData = () => {
    if (props.choice) {
      return (
        <FormControlLabel
          className={classes.radio}
          style={{
            width: '100%',
            padding: '6px',
            marginRight: '0',
            minHeight: '60px',
            marginLeft: '0'
          }}
          control={<Radio className={classes.radio} />}
          value={props.choice.choice_legend}
          label={
            props.choice.choice_value != '?' ? setSentenceCase(props.choice.choice_value) : '?'
          }
          labelPlacement="end"
        />
      );
    }
    return <p>unable to load choice</p>;
  };

  return (
    <>
      {props.isGrid == true ? (
        <Grid item xs={12} sm={6} md={Number(lgCol) as GridSize} className={classes.root}>
          <Box display="flex" alignItems="center">
            <Box width="100%">
              <Paper variant="outlined" className={classes.root}>
                {ShowData()}
              </Paper>
            </Box>
          </Box>
        </Grid>
      ) : (
        <Grid item xs={12} sm="auto" className={classes.root}>
          <Box display="flex" alignItems="center">
            <Box width="100%">
              <Paper variant="outlined" className={classes.root}>
                {ShowData()}
              </Paper>
            </Box>
          </Box>
        </Grid>
      )}
    </>
  );
};

export default Choice;
