// material
import { sentenceCase } from 'change-case';
import { Container, Typography, Paper, Avatar, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import _ from 'lodash';
import React from 'react';
import TypeMultipleChoice from './TypeMultipleChoice';
import { setSentenceCase } from '../../utils/string';

// ----------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  question: {
    color: theme.palette.text.primary,
    fontWeight: 'bolder',
    fontSize: '1.3rem',
    textAlign: 'left',
    paddingTop: '8px',
    paddingBottom: '20px',
    marginBottom: '16px',
    borderBottom: '1px dashed #ddd'
  },
  paper: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.primary,
    border: 3,
    position: 'relative'
  },
  square: {
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.background.default,
    position: 'absolute',
    top: '-8px',
    left: 0,
    fontSize: '.8rem',
    width: theme.spacing(3),
    height: theme.spacing(3),
    borderRadius: 10,
    boxShadow: '0 8px 16px 0 rgb(145 158 171 / 24%)'
  },
  questionImg: {}
}));

interface Props {
  questionId: any;
  exam: any;
}

const Question = (props: Props) => {
  const classes = useStyles();
  const question = props.exam.data.questions.find(
    (question: any) => question.question_id === props.questionId
  );

  const ShowData = () => {
    if (!_.isEmpty(question)) {
      const renderQuestionByType = () => (
        <TypeMultipleChoice question={question} questionId={question.question_id} />
      );

      // TECH DEBT
      if (props.exam.data.quiz.data.quiz_slug == 'intelligence-qoutient-test') {
        const STATIC_URL = '/static/exam-photos';
        const SUFFIX = '-min';
        const DATA_TYPE = '.PNG';

        const slug = props.exam.data.quiz.data.quiz_slug;

        const imgUrl = `${STATIC_URL}/${slug}/questions/${question.question_number}${SUFFIX}${DATA_TYPE}`;

        return (
          <div>
            <Avatar variant="square" className={classes.square}>
              {question.question_number2}
            </Avatar>
            <Box
              component="img"
              src={imgUrl}
              alt={`iq-${question.question_number}`}
              sx={{
                mx: 'auto',
                maxHeight: 480,
                transition: (theme) => theme.transitions.create('all'),
                mb: (theme) => theme.spacing(4)
              }}
            />
            {renderQuestionByType()}
          </div>
        );
      }
      if (question.question_value) {
        return (
          <div>
            <Avatar variant="square" className={classes.square}>
              {question.question_number2}
            </Avatar>
            <Typography variant="h5" className={classes.question}>
              {setSentenceCase(question.question_value)}
            </Typography>
            {renderQuestionByType()}
          </div>
        );
      }
      return (
        <>
          <Avatar variant="square" className={classes.square}>
            {question.question_number2}
          </Avatar>
          {renderQuestionByType()}
        </>
      );
    }

    return <p>unable to load question</p>;
  };

  return (
    <Paper
      className={`${classes.paper}`}
      sx={{
        p: 1,
        boxShadow: (theme) => theme.customShadows.z8
      }}
    >
      {ShowData()}
    </Paper>
  );
};

export default Question;
