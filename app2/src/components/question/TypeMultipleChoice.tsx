// material
import { Container, Typography, RadioGroup, FormControl, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import _ from 'lodash';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SetAnswer } from '../../actions/AnswerSheetActions';
import Choice from './Choice';
// ----------------------------------------------------------------------
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  formControl: {
    width: '100%'
  }
}));

interface Props {
  questionId: number;
  question: any;
}

const columnSize = (size: number) => {
  if (size === 5) return 2;
  if (size === 3) return 4;
  if (size === 2) return 6;
  return 4;
};

const isGrid = (size: number) => {
  if (size === 5) return false;
  return true;
};

const TypeMultipleChoice: React.FC<Props> = ({ question, questionId }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [value, setValue] = React.useState<any | null>(() => null);
  const [hasValue, setHasValue] = React.useState(false);

  const { choices: rawChoices } = question;

  const handleChange = (e: any) => {
    setHasValue(true);
    setValue(e.target.value);
    dispatch(SetAnswer(e.target.value, questionId));
  };

  interface ChoicesOptions {
    choice_id: number;
    choice_legend: string;
    choice_points: number;
    choice_value: string;
    fk_question_id: number;
  }

  let abcCounter = 9;
  let choices: ChoicesOptions[];

  if (!_.isEmpty(rawChoices)) {
    choices = rawChoices.map((choice: any) => {
      abcCounter++;
      return (
        <Choice
          choice={choice}
          key={choice.choice_id}
          letter={abcCounter.toString(36)}
          lgColSize={columnSize(rawChoices.length)}
          isGrid={isGrid(rawChoices.length)}
        />
      );
    });
  }

  const ShowData = () => {
    if (!_.isEmpty(rawChoices)) {
      return choices;
    }
    return <p>unable to load</p>;
  };

  return (
    <Container maxWidth="xl" style={{ padding: 0 }}>
      <FormControl
        disabled={hasValue}
        component="fieldset"
        className={classes.formControl}
        style={{ padding: 0 }}
      >
        <RadioGroup
          row
          aria-label="position"
          name="position"
          defaultValue="top"
          value={value}
          onChange={handleChange}
          style={{ padding: 0 }}
        >
          <Grid container spacing={3}>
            {ShowData()}
          </Grid>
        </RadioGroup>
      </FormControl>
    </Container>
  );
};

export default TypeMultipleChoice;
