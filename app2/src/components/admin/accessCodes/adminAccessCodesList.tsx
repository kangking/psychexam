import _ from 'lodash';
// react
// material
import { Container, Typography, Card, CardHeader, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import { useDispatch, useSelector } from 'react-redux';
import { MChip } from '../../@material-extend';
import TablePrime from '../../TablePrime';

type AccessCode = {
  access_code_id: number;
  access_code_value: string;
  access_code_created_at: Date | string;
  access_code_is_valid: boolean | number;
};

export default function AdminAccessCodesPage() {
  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  interface Column {
    data: any;
    align: 'left' | 'center' | 'right' | 'justify' | 'inherit' | undefined;
  }

  const headColumns: Column[] = [
    { data: 'Access Code', align: 'left' },
    { data: 'Valid', align: 'left' }
    // { data: 'Exam Set', align: 'left' }
  ];

  const columns: Column[] = [
    { data: 'access_code_value', align: 'left' },
    {
      data: (row: AccessCode) =>
        row.access_code_is_valid === 1 || row.access_code_is_valid === true ? (
          <MChip label="Valid" color="primary" size="small" />
        ) : (
          <MChip label="Invalid" color="error" size="small" />
        ),
      align: 'left'
    }
    // { data: 'quiz_set_name', align: 'left' }
  ];

  return (
    <Card sx={{ mt: 2 }}>
      <CardContent>
        <TablePrime
          orderBy={{ column: 'access_code_created_at', direction: 'ASC' }}
          filter={{ keyword: '', columns: ['access_code_is_valid'] }}
          headColumns={headColumns}
          columns={columns}
          responseKey="accessCodes"
          fetchUrl="/api/admin/access-codes-table"
          forceReload={reloadState.accessCodesTable}
          forceReloadActionType={{
            reload: 'RELOAD_ACCESS_CODES_TABLE',
            doneReload: 'RELOAD_DONE_ACCESS_CODES_TABLE'
          }}
        />
      </CardContent>
    </Card>
  );
}
