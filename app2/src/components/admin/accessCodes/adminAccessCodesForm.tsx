import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useSnackbar } from 'notistack';
import {
  Stack,
  Alert,
  TextField,
  MenuItem,
  Card,
  CardHeader,
  CardContent
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { Icon } from '@iconify/react';
import closeFill from '@iconify/icons-eva/close-fill';
import { useDispatch, useSelector } from 'react-redux';
import { MIconButton } from '../../@material-extend';
import { ExamSetAsResponse } from '../../../@types/exam';
import { DoReload } from '../../../actions/ReloadActions';

// hooks
import useIsMountedRef from '../../../hooks/useIsMountedRef';

import axios from '../../../utils/axios';

interface Props {
  isNew: boolean;
  children?: React.ReactNode | null;
}

type InitialValues = {
  quantity: number;
  // examSetId: number | null;
  afterSubmit?: any;
};

const validationSchema = yup.object({
  quantity: yup.string().required('Quantity is required')
  // examSetId: yup.number().required('Exam Set is Required')
});

const AccessCodeForm = (props: Props) => {
  const { isNew } = props;
  const dispatch = useDispatch();
  // const [examSets, setExamSets] = useState<ExamSetAsResponse[]>([]);
  // const [examSetId, setExamSetId] = useState<number | null>(null);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const isMountedRef = useIsMountedRef();

  // useEffect(() => {
  //   fetchExamSets();
  // }, []);

  // const fetchExamSets = async () => {
  //   const response = await axios.get('/api/admin/quiz-set');
  //   const { quizSet, success, message } = response.data;

  //   if (success === true) {
  //     if (quizSet.data) {
  //       setExamSets(quizSet.data);
  //     }
  //   } else {
  //     throw new Error(`Failed to fetch exam sets. ${message}`);
  //   }
  // };

  const addAccessCode = async (data: any) => {
    console.log(data, 'ACFORM');
    const response = await axios.post('/api/admin/access-code', {
      quantity: data.quantity
      // quizSetId: examSetId
    });
    const { success, message } = response.data;

    console.log(success);

    if (success === true) {
      console.log('added');
      dispatch(DoReload({ type: 'RELOAD_ACCESS_CODES_TABLE' }));
      // add force reload
    } else {
      throw new Error(`Failed to add Access Code/s. ${message}`);
    }
  };

  const onSubmitHandler = async (values: any, { setErrors, setSubmitting, resetForm }: any) => {
    if (isNew) {
      // This is add new
      try {
        await addAccessCode(values);
        enqueueSnackbar('Access code added', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        console.log(error);
        resetForm();
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error });
          setSubmitting(false);
        }
      }
    } else {
      // This is update existing
      console.log('update no available');
    }
  };

  // const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   const { value, name } = event.target;

  //   if (name === 'examSetId') {
  //     setExamSetId(Number(value));
  //   }
  // };

  const formik = useFormik<InitialValues>({
    initialValues: {
      quantity: 5
      // examSetId: 1
    },
    validationSchema,
    onSubmit: (values, { setErrors, setSubmitting, resetForm }) => {
      onSubmitHandler(values, { setErrors, setSubmitting, resetForm });
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, isValidating, getFieldProps } = formik;

  return (
    <div>
      <Card>
        <CardHeader
          title="Add Access Codes"
          action={
            <LoadingButton
              fullWidth={false}
              size="large"
              type="submit"
              variant="contained"
              form="form"
              loading={isSubmitting}
            >
              {isNew ? 'Create Access Codes' : 'Save Changes'}
            </LoadingButton>
          }
        />
        <CardContent>
          <form onSubmit={handleSubmit} id="form">
            {errors.afterSubmit && Object.keys(errors.afterSubmit).length != 0 && (
              <Alert severity="error">{errors.afterSubmit}</Alert>
            )}
            <Stack spacing={3}>
              <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
                <TextField
                  fullWidth
                  autoComplete="quantity"
                  type="text"
                  label="Quantity"
                  {...getFieldProps('quantity')}
                  error={Boolean(touched.quantity && errors.quantity)}
                  helperText={touched.quantity && errors.quantity}
                />
                {/* <TextField
                  fullWidth
                  select
                  label="Assign exam set"
                  value={examSetId}
                  name="examSetId"
                  onChange={handleChange}
                  error={Boolean(formik.touched.examSetId && formik.errors.examSetId)}
                  helperText={formik.touched.examSetId && formik.errors.examSetId}
                >
                  {examSets.map((option: ExamSetAsResponse) => (
                    <MenuItem key={option.quiz_set_id} value={option.quiz_set_id}>
                      {option.quiz_set_name}
                    </MenuItem>
                  ))}
                </TextField> */}
              </Stack>
            </Stack>
          </form>
        </CardContent>
      </Card>
    </div>
  );
};

export default AccessCodeForm;
