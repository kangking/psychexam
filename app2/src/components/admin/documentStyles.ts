import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  UnderlineType,
  LevelFormat,
  AlignmentType,
  convertInchesToTwip,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

export const styles = {
  default: {
    title: {
      run: {
        size: 34,
        bold: true,
        italics: false,
        font: 'Calibri',
        color: '000000'
      },
      paragraph: {
        spacing: {
          after: 120
        }
      }
    },
    heading1: {
      run: {
        size: 36,
        bold: true,
        italics: false,
        font: 'Calibri',
        color: '000000'
      },
      paragraph: {
        spacing: {
          after: 400
        }
      }
    },
    heading2: {
      run: {
        size: 32,
        bold: true,
        font: 'Calibri',
        color: '000000'
      },
      paragraph: {
        spacing: {
          before: 400,
          after: 120
        }
      }
    },
    heading3: {
      run: {
        size: 30,
        bold: true,
        font: 'Calibri',
        color: '000000'
      },
      paragraph: {
        spacing: {
          before: 240,
          after: 120
        }
      }
    },
    heading4: {
      run: {
        size: 28,
        bold: false,
        font: 'Calibri',
        color: '000000'
      },
      paragraph: {
        spacing: {
          before: 400,
          after: 400
        }
      }
    },
    listParagraph: {
      run: {
        color: '#FF0000'
      }
    }
  },
  paragraphStyles: [
    {
      id: 'titleHeader',
      name: 'TitleHeader',
      basedOn: 'Normal',
      next: 'Normal',
      paragraph: {
        spacing: {
          before: 20,
          after: 240
        }
      },
      run: {
        size: 22,
        font: 'Calibri',
        color: '000000',
        bold: false,
        italics: true,
        underline: {}
      }
    },
    {
      id: 'subtitle',
      name: 'Subtitle',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 32,
        font: 'Calibri',
        color: '000000',
        bold: false,
        italics: true,
        underline: {}
      }
    },
    {
      id: 'clientTable',
      name: 'Client Table',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 28,
        font: 'Calibri',
        color: '000000',
        bold: false
      }
    },
    {
      id: 'normalContent',
      name: 'Normal Content',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 24,
        font: 'Calibri',
        color: '000000',
        bold: false
      },
      paragraph: {
        indent: {
          left: 400
        }
      }
    },
    {
      id: 'traitHeader',
      name: 'Trait Header',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 28,
        font: 'Calibri',
        color: '000000',
        bold: true
      },
      paragraph: {
        indent: {
          left: 400
        }
      }
    },
    {
      id: 'trait',
      name: 'Trait',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 26,
        font: 'Calibri',
        color: '000000',
        bold: false
      },
      paragraph: {
        indent: {
          left: 740
        }
      }
    },
    {
      id: 'personalityHeader',
      name: 'Personality Header',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 24,
        font: 'Calibri',
        color: '000000',
        bold: true
      },
      paragraph: {
        indent: {
          left: 740
        }
      }
    },
    {
      id: 'personality',
      name: 'Personality',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 24,
        font: 'Calibri',
        color: '000000',
        bold: false
      },
      paragraph: {
        indent: {
          left: 740
        }
      }
    }
  ]
};

export const numbering = {
  config: [
    {
      reference: 'my-crazy-numbering',
      levels: [
        {
          level: 0,
          format: LevelFormat.LOWER_LETTER,
          text: '%1)',
          alignment: AlignmentType.LEFT
        }
      ]
    }
  ]
};

export const stylesAsAttributes = {
  headerTitle: {
    heading: HeadingLevel.HEADING_1,
    alignment: AlignmentType.CENTER,
    spacing: {
      after: 0
    }
  },
  introTitle: {
    heading: HeadingLevel.HEADING_1,
    alignment: AlignmentType.CENTER,
    spacing: {
      after: 0
    }
  },
  introSubtitle: {
    alignment: AlignmentType.CENTER,
    spacing: {
      before: 0
    }
  },
  numeralHeaders: {
    heading: HeadingLevel.HEADING_2,
    alignment: AlignmentType.LEFT,
    spacing: {
      before: 400,
      after: 200
    }
  },
  traitHeaders: {
    heading: HeadingLevel.HEADING_3,
    spacing: {
      before: 400,
      after: 200
    },
    tabStops: [
      {
        type: TabStopType.LEFT,
        position: 200
      },
      {
        type: TabStopType.RIGHT,
        position: 2268
      }
    ]
  },
  traits: {
    font: 'Calibri',
    spacing: {
      before: 0
    }
  },
  personalityHeaders: {
    heading: HeadingLevel.HEADING_4,
    alignment: AlignmentType.LEFT,
    spacing: {
      before: 400,
      after: 200
    }
  },
  personalities: {
    spacing: {
      before: 0
    }
  },
  tableWidth: {
    labels: {
      width: {
        size: 50,
        type: WidthType.PERCENTAGE
      }
    },
    values: {
      width: {
        size: 50,
        type: WidthType.PERCENTAGE
      }
    }
  },
  noTableBorders: {
    borders: {
      top: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      },
      bottom: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      },
      left: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      },
      right: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      }
    }
  }
};
