import _ from 'lodash';

// material
import { Grid, Box, Paper, Pagination, Typography, TextField, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
//
import { useEffect, useState, useRef } from 'react';
import Scrollbar from '../Scrollbar';
import axios from '../../utils/axios';

const ClientList = () => {
  const [clients, setClients] = useState([]);
  const [filterKeyword, setFilterKeyword] = useState('');
  const [pagination, setPagination] = useState(1);
  // const [totalPages, setTotalPages] = useState(1);
  const [limit, setLimit] = useState(6);
  const totalPages = useRef(0);
  const offset = useRef(0);

  useEffect(() => {
    fetchClientTable();
  }, [pagination]);

  const fetchClientTable = async () => {
    offset.current = pagination * limit - limit;

    const params = {
      orderBy: { column: 'client_id', direction: 'ASC' },
      limit: limit * 1,
      offset: offset.current,
      filter: { keyword: filterKeyword, columns: ['client_last_name'] },
      pagination
    };

    axios
      .get('/api/admin/client-table', { params })
      .then((response) => {
        offset.current = pagination * limit - limit;
        // setTotalPages(Math.ceil(response.data.client.totalRows / limit));
        totalPages.current = Math.ceil(response.data.client.totalRows / limit);
        setClients(response.data.client.data);
      })
      .catch((error) => {
        throw new Error(error);
      });
  };

  const handlePagination = (event: any, value: number) => {
    setPagination(value);
  };

  const handleSearch = () => {
    fetchClientTable();
  };

  const handleInputChange = (event: any) => {
    const { name } = event.target;
    const { value } = event.target;

    if (name === 'filterKeyword') {
      setFilterKeyword(value);
    }
  };

  const CLIENT_TABLE = clients;

  const summaryDetailKeys = [
    {
      key: 'client_id',
      label: 'Database ID'
    },
    {
      key: 'client_last_name',
      label: 'Last Name'
    },
    {
      key: 'client_first_name',
      label: 'First Name'
    },
    {
      key: 'client_email',
      label: 'Email'
    },
    {
      key: 'client_mobile_number',
      label: 'Mobile No.'
    }
  ];

  const ShowData = () => {
    if (!_.isEmpty(CLIENT_TABLE)) {
      const list = CLIENT_TABLE.map((item: any, index: number) => {
        const keys: string[] = Object.keys(item);
        return (
          <Grid item md={6} lg={4} xl={3} key={`client-${index}`}>
            <Paper sx={{ p: 2 }} elevation={4}>
              <Box>
                {summaryDetailKeys.map((item2: any, index2: number) => (
                  <Grid container key={`a-${index2}`}>
                    <Typography align="left" variant="subtitle2">
                      {item2.label}:
                    </Typography>
                    <Typography ml={1} align="right" variant="body1">
                      {item[item2.key]}
                    </Typography>
                  </Grid>
                ))}
              </Box>
              <Link style={{ marginTop: 4 }} to={`/admin/clients/${item.client_id}`}>
                View Details
              </Link>
            </Paper>
          </Grid>
        );
      });
      return list;
    }
    return null;
  };

  return (
    <>
      {/* <Typography variant="overline" paragraph>
        totalPages: {totalPages.current} | offset: {offset.current} | pagination:
        {pagination} | limit: {limit}
      </Typography> */}
      <Paper sx={{ p: 2, mb: 2 }} elevation={4}>
        <Grid container justifyContent="left" spacing={2}>
          <Grid item>
            <TextField
              size="small"
              id="standard-basic"
              label="Search Last Name"
              name="filterKeyword"
              variant="outlined"
              value={filterKeyword}
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item>
            <Button variant="contained" onClick={handleSearch}>
              Search
            </Button>
          </Grid>
        </Grid>
      </Paper>
      <Grid container justifyContent="left" spacing={2}>
        {ShowData()}
      </Grid>
      <Grid container justifyContent="left" spacing={2} sx={{ mt: 1 }}>
        <Paper sx={{ p: 1, m: 2 }} elevation={4}>
          <Pagination
            count={totalPages.current}
            page={pagination}
            onChange={handlePagination}
            color="primary"
          />
        </Paper>
      </Grid>
    </>
  );
};

export default ClientList;
