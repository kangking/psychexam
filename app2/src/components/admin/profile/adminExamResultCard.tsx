import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect } from 'react';
// material
import { Typography, IconButton, Card, CardHeader, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Close';
import { setSentenceCase, setPronounGender } from '../../../utils/string';

interface Props {
  result: any;
  gender?: string;
  deleteAction?: any;
  children: React.ReactNode;
  actionElements: React.ReactNode[];
}

export default function adminExamResultCard(props: Props) {
  const { result, gender, deleteAction, actionElements } = props;

  const keys = Object.keys(result.details);

  const ShowData = () => (
    <Card>
      <CardHeader
        action={actionElements.map((el) => el)}
        title={result.exam.quiz_name}
        subheader={
          <>
            <Typography variant="overline" paragraph>
              {result.exam.quiz_set_name}
            </Typography>
            <Typography variant="subtitle2" paragraph>
              {moment(result.exam.client_answer_sheet_created_at).format(
                'MMMM DD, YYYY @ h:mm:ss a'
              )}
            </Typography>
          </>
        }
      />

      <CardContent>
        {keys.map((key) => (
          <Typography key={key} variant="body2" paragraph>
            {key}: {JSON.stringify(result.details[key], undefined, 4)}
          </Typography>
        ))}
        <Typography variant="body2" paragraph>
          {result.interpretations.message
            ? setSentenceCase(
                setPronounGender(JSON.stringify(result.interpretations.message), gender)
              )
            : ''}
        </Typography>
      </CardContent>
    </Card>
  );

  return <>{ShowData()}</>;
}
