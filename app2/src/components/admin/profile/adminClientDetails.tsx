import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect, useRef } from 'react';
// material
import {
  Typography,
  Grid,
  Stack,
  Card,
  CardHeader,
  CardContent,
  Avatar,
  IconButton
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { clientLabels1, clientLabels2, clientLabels3 } from './detailsConfig';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  },
  paper: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.primary,
    border: 3,
    position: 'relative'
  }
}));

interface Props {
  client: any;
}

export default function ClientDetails(props: Props) {
  const classes = useStyles();

  const ShowNameOnly = (): string => {
    let name = '';

    if (props.client != null) {
      const fname = props.client.client_last_name ? props.client.client_last_name : '';
      const lname = props.client.client_last_name ? props.client.client_first_name : '';
      const mname = props.client.client_last_name ? props.client.client_middle_name : '';

      name = `${lname}, ${fname} ${mname}`;
    }

    return name;
  };

  const ShowName = (): JSX.Element | null => {
    let component = null;

    if (props.client != null) {
      component = (
        <Stack direction="row" alignItems="center" justifyContent="space-between">
          <Typography variant="h4" component="h4" sx={{ color: 'text.primary' }}>
            {ShowNameOnly()}
          </Typography>
        </Stack>
      );
    }

    return component;
  };

  const showDataColumn = (clientLabels: {}[]): JSX.Element[] => {
    let component: JSX.Element[] = [];

    if (props.client != null) {
      component = clientLabels.map((clientLabel: any, index: number) => (
        <Stack
          key={`client-label-${index}`}
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Typography variant="overline" component="p" sx={{ color: 'text.secondary' }}>
            {clientLabel.label}:
          </Typography>
          <Typography variant="subtitle2" component="p" sx={{ color: 'text.primary' }}>
            {props.client[clientLabel.key] ? props.client[clientLabel.key] : 'N/A'}
          </Typography>
        </Stack>
      ));
    }

    return component;
  };

  return (
    <Card>
      <CardHeader
        avatar={<Avatar aria-label="recipe">{props.client.client_last_name.slice(0, 2)}</Avatar>}
        title={ShowNameOnly()}
        subheader={props.client.client_created_at}
      />
      <CardContent>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={4}>
            {showDataColumn(clientLabels1)}
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            {showDataColumn(clientLabels2)}
          </Grid>
          <Grid item xs={12} sm={12} md={4}>
            {showDataColumn(clientLabels3)}
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}
