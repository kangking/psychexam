import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect } from 'react';
// material
import {
  Typography,
  Paper,
  Grid,
  Button,
  IconButton,
  Fade,
  Stack,
  Container,
  Card,
  CardHeader,
  CardContent
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Close';
import Popper, { PopperPlacementType } from '@material-ui/core/Popper';
// import GenerateFullEvaluation from '../adminGenerateFullEvaluation';
import GenerateFullEvaluation from '../evaluation/generateEvaluationButton';
import { setSentenceCase, setPronounGender } from '../../../utils/string';
import { Client } from '../../../@types/client';

import ExamResultCard from './adminExamResultCard';
import PopperDelete from '../../PopperDelete';

import axios from '../../../utils/axios';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  },
  paper: {
    margin: theme.spacing(0),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.primary,
    border: 3,
    position: 'relative'
  }
}));

interface Props {
  client: Client;
}

export default function ExamPage(props: Props) {
  const [exams, setExams] = useState([]);
  const [results, setResults] = useState<any[]>([]);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = useState(false);
  const [answerSheetIdToDelete, setAnswerSheetIdToDelete] = useState<number | null>(null);

  const [gender, setGender] = useState<string>('m');

  const fetchClientExams = async () => {
    setExams([]);
    const response = await axios.get(`/api/admin/client-quiz?clientId=${props.client.client_id}`);
    if (response) setExams(response.data.answerSheet.data);
  };

  useEffect(() => {
    setGender(props.client.client_gender);
    const initialize = async () => {
      if (props.client) {
        fetchClientExams();
      }
    };
    initialize();
  }, [props.client]);

  useEffect(() => {
    GetResultAnalysis();
  }, [exams]);

  const handleDeleteConfirmation = (id: any) => (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnswerSheetIdToDelete(id);
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const handleDeleteExecution = (id: any) => async (event: React.MouseEvent<HTMLButtonElement>) => {
    const response = await axios.delete(`/api/admin/answer-sheet?answerSheetId=${id}`);
    if (response.data) {
      if (response.data.success) {
        console.log('deleted successfully');
        fetchClientExams();
      } else {
        console.log('deletion unsuccessful');
        console.log('too bad so sad');
      }
    }
    setOpen(false);
  };

  const GetResultAnalysis = async () => {
    setResults([]);
    if (!_.isEmpty(exams)) {
      const result: any = [];

      const analyzeResult = async (answerSheet: any, exam: any) => {
        const response = await axios.post('/api/analyze-result', answerSheet);
        const procResult: any = {
          details: response.data.details,
          interpretations: response.data.interpretations,
          answerSheet,
          exam
        };

        return new Promise((resolve, reject) => {
          resolve(procResult);
          reject(new Error('unable to fetch analysis'));
        });
      };

      for (let i: number = 0; i < exams.length; i++) {
        const exam: any = exams[i];
        const answerSheet = exam.client_answer_sheet_value;
        try {
          const promiseElement = analyzeResult(answerSheet, exam);
          result.push(promiseElement);
        } catch (err) {
          console.log('something went wrong', err);
        }
      }
      setResults(await Promise.all(result));
    }
  };

  const ExamResultsActionElements = (id: number | string) => {
    const el = [];
    el.push(
      <IconButton
        key="delAct"
        aria-label="delete"
        color="primary"
        onClick={handleDeleteConfirmation(id)}
      >
        <DeleteIcon />
      </IconButton>
    );
    return el;
  };

  const ShowResults = () => {
    if (!_.isEmpty(results)) {
      const elements = results.map((result: any, index: number) => {
        const keys = Object.keys(result.details);
        return (
          <Grid item xs={12} key={`exam-result-${index}`}>
            <ExamResultCard
              result={result}
              gender={gender}
              actionElements={ExamResultsActionElements(result.exam.client_answer_sheet_id)}
            >
              hey i just met you and you are crazy so hers my numberrr
            </ExamResultCard>
          </Grid>
        );
      });
      return elements;
    }
    return <p>No Results Found</p>;
  };

  const PopperDeleteActionElements = () => (
    <>
      <Button
        sx={{ mr: 2 }}
        variant="contained"
        color="error"
        onClick={handleDeleteExecution(answerSheetIdToDelete)}
      >
        Delete
      </Button>
      <Button
        variant="outlined"
        color="inherit"
        onClick={() => {
          setOpen(false);
        }}
      >
        Cancel
      </Button>
    </>
  );

  return (
    <Grid container>
      <Grid item mt={6}>
        <Grid container maxWidth="xl">
          <Grid item xs>
            <Typography variant="h6" paragraph>
              Test Results:
            </Typography>
          </Grid>
          <Grid item xs style={{ justifyContent: 'end', textAlign: 'right' }}>
            <GenerateFullEvaluation client={props.client} results={results} />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          {ShowResults()}
        </Grid>
        <PopperDelete anchorEl={anchorEl} open={open} actions={PopperDeleteActionElements()} />
      </Grid>
    </Grid>
  );
}
