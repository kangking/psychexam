export const clientLabels1: {}[] = [
  {
    key: 'client_civil_status',
    label: 'Civil Status'
  },
  {
    key: 'client_gender',
    label: 'Gender'
  },
  {
    key: 'client_age',
    label: 'Chronological Age'
  }
];

export const clientLabels2: {}[] = [
  {
    key: 'client_birthdate',
    label: 'Birthdate'
  },
  {
    key: 'client_email',
    label: 'Email'
  },
  {
    key: 'client_mobile_number',
    label: 'Mobile No.'
  }
];

export const clientLabels3: {}[] = [
  {
    key: 'client_address_line',
    label: 'Address'
  }
];

export const clientLabelsAll: {}[] = [
  {
    key: ['client_first_name', 'client_last_name', 'client_middle_name'],
    label: 'Name'
  },
  {
    key: 'client_middle_name',
    label: 'Middle Name'
  },
  {
    key: 'client_gender',
    label: 'Gender'
  },
  {
    key: 'client_created_at',
    label: 'Date of Testing'
  },
  {
    key: 'client_age',
    label: 'Chronological Age'
  },
  {
    key: 'client_civil_status',
    label: 'Civil Status'
  },
  {
    key: 'client_birthdate',
    label: 'Date of Birth'
  },
  {
    key: 'client_email',
    label: 'Email'
  },
  {
    key: 'client_mobile_number',
    label: 'Mobile No.'
  },
  {
    key: 'client_address_line',
    label: 'Address'
  }
];
