import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
// material
import { Typography, Card, CardContent, Grid } from '@material-ui/core';
import useLead from '../../../hooks/useLead';

// components
// import ExamSetForm from './adminExamSetForm';
import LeadList from './adminLeadList';
import LeadForm from './adminLeadForm';

export default function AdminLeadSetProfile() {
  const { leadSet, getLeadSet } = useLead();
  const leadSetId: number = Number(useParams().leadSetId);

  useEffect(() => {
    fetchLeadSet();
  }, []);

  useEffect(() => {
    console.log('UseEffect:', leadSet);
  }, [leadSet]);

  const fetchLeadSet = () => {
    getLeadSet({ leadSetId });
    console.log('State:', leadSet);
    // try {
    //   await getLeadSet({ leadSetId });
    //   console.log('lllll', leadSet);
    // } catch (error) {
    //   alert(error);
    // }
  };

  const ShowData = () => {
    if (leadSet) {
      return (
        <>
          <Grid container style={{ justifyContent: 'left' }}>
            <Grid item xs={12} sm={12}>
              <Typography variant="h6" paragraph>
                {ShowFuckingName()}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Card sx={{ mb: 2 }}>
                <CardContent>
                  <LeadForm isNew={true} leadSetId={leadSetId} />
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={12} sm={12}>
              <LeadList />
            </Grid>
          </Grid>
        </>
      );
    }
    return null;
  };

  const ShowFuckingName = () => {
    if (leadSet) {
      if (Object.prototype.hasOwnProperty.call(leadSet, 'lead_set_name')) {
        return leadSet.lead_set_name;
      }
    }
    return 'Name not provided';
  };

  return <>{ShowData()}</>;
}
