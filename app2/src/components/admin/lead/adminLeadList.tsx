import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router';
// material
import {
  Container,
  Typography,
  Paper,
  Grid,
  Pagination,
  Button,
  Card,
  CardHeader,
  TablePagination,
  Popper,
  Fade
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { LoadingButton } from '@material-ui/lab';
// components
import { useParams } from 'react-router-dom';
import { MChip, MButton, MIconButton } from '../../@material-extend';
import Page from '../../Page';
import LeadSetAddDialog from './adminLeadSetAddDialog';
import Scrollbar from '../../Scrollbar';

import { LeadAsResponse } from '../../../@types/lead';

import useLead from '../../../hooks/useLead';

import axios from '../../../utils/axios';

export default function LeadList() {
  const navigate = useNavigate();
  const { leadTable, getLeadTable, reloadLeadTable, isReloadingLeadTable } = useLead();
  const leadSetId: number = Number(useParams().leadSetId);

  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState<number | null>(null);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [pagination, setPagination] = useState(1);
  const [limit, setLimit] = useState(10);
  const totalPages = useRef(0);
  const offset = useRef(0);

  useEffect(() => {
    fetchLeads();
  }, [pagination, limit]);

  useEffect(() => {
    if (isReloadingLeadTable) {
      fetchLeads();
    }
  }, [isReloadingLeadTable]);

  const fetchLeads = () => {
    offset.current = pagination * limit - limit;
    setIsLoading(true);

    const params = {
      orderBy: { column: 'lead_created_at', direction: 'ASC' },
      limit: limit * 1,
      offset: offset.current,
      filter: {
        keyword: leadSetId,
        columns: ['fk_lead_set_id']
      },
      pagination
    };

    getLeadTable(params, leadSetId);
  };

  const handlePagination = (event: any, value: number) => {
    setPagination(value);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(+event.target.value);
    setPagination(1);
  };

  const handleOpenRow = (id: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
    navigate(`/admin/leads/${id}`);
  };

  const handleDeleteConfirmation = (id: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
    setIdToDelete(id);
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const handleDeleteExecution = (id: any) => async (event: React.MouseEvent<HTMLButtonElement>) => {
    const response = await axios.delete(`/api/admin/lead?leadId=${id}`);
    if (response.data) {
      if (response.data.success) {
        fetchLeads();
        enqueueSnackbar('Delete success', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
      } else {
        console.log('deletion unsuccessful');
      }
    }
    setOpen(false);
  };

  const handleSendEmail =
    (email: string | null) => (event: React.MouseEvent<HTMLButtonElement>) => {
      try {
        if (email) sendEmail(email);
        else sendEmail(null);
      } catch (error) {
        console.log(error);
      }
    };

  const sendEmail = async (email: string | null = null) => {
    setIsSubmitting(true);
    const payload = { email, leadSetId };
    const response = await axios.post('/api/admin/email-registration-accesscode', payload);

    if (response.data.success) {
      enqueueSnackbar('Email sent!', {
        variant: 'success',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    } else {
      enqueueSnackbar('Email not sent!', {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    }
    setIsSubmitting(false);
  };

  const ShowLeadsRows = () => {
    if (!_.isEmpty(leadTable)) {
      return leadTable.map((row: LeadAsResponse) => (
        <TableRow
          hover
          key={row.lead_id}
          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
          <TableCell component="th" scope="row">
            {row.lead_email}
          </TableCell>
          <TableCell align="left">
            {moment(row.lead_created_at).format('MMM DD, YYYY - h:mm:ss a')}
          </TableCell>
          <TableCell align="right">
            <LoadingButton
              sx={{ mr: 1 }}
              fullWidth={false}
              variant="outlined"
              color="primary"
              onClick={handleSendEmail(row.lead_email)}
              loading={isSubmitting}
            >
              Send
            </LoadingButton>

            <LoadingButton
              fullWidth={false}
              variant="outlined"
              color="inherit"
              onClick={handleDeleteConfirmation(row.lead_id)}
              loading={isSubmitting}
            >
              Delete
            </LoadingButton>
          </TableCell>
        </TableRow>
      ));
    }
    return null;
  };

  const ShowLeadsTable = () => (
    <Scrollbar>
      <TableContainer sx={{ minWidth: 800, mt: 3 }}>
        <Table sx={{ minWidth: 650 }} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Email</TableCell>
              <TableCell align="left">Created At</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{ShowLeadsRows()}</TableBody>
        </Table>
      </TableContainer>
    </Scrollbar>
  );

  return (
    <Page title="Dashboard">
      <>
        <Card>
          <CardHeader
            title="Lead"
            action={
              // <Button
              //   sx={{ mr: 2 }}
              //   variant="contained"
              //   color="error"
              //   onClick={handleSendEmail(null)}
              // >
              //   Send Email Blast
              // </Button>
              <LoadingButton
                fullWidth={false}
                variant="contained"
                color="error"
                onClick={handleSendEmail(null)}
                loading={isSubmitting}
              >
                Send Email Blast
              </LoadingButton>
            }
          />
          {ShowLeadsTable()}
          <Paper sx={{ p: 1, m: 2 }}>
            <Pagination
              count={totalPages.current}
              page={pagination}
              onChange={handlePagination}
              color="primary"
            />
          </Paper>
        </Card>
        <Popper open={open} anchorEl={anchorEl} placement="bottom-start" transition>
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper elevation={4} sx={{ p: 2 }}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography variant="h5" paragraph>
                      Are you sure you want to delete?
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      sx={{ mr: 2 }}
                      variant="contained"
                      color="error"
                      size="small"
                      onClick={handleDeleteExecution(idToDelete)}
                    >
                      Delete
                    </Button>
                    <Button
                      variant="outlined"
                      color="info"
                      size="small"
                      onClick={() => {
                        setOpen(false);
                      }}
                    >
                      Cancel
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Fade>
          )}
        </Popper>
      </>
    </Page>
  );
}
