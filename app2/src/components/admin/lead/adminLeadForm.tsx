import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useSnackbar } from 'notistack';
import {
  Button,
  Link,
  Stack,
  Alert,
  Checkbox,
  TextField,
  IconButton,
  InputAdornment,
  FormControlLabel,
  Typography
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { MIconButton } from '../../@material-extend';
import useLead from '../../../hooks/useLead';
import { Lead, LeadAsResponse } from '../../../@types/lead';

// hooks
import useIsMountedRef from '../../../hooks/useIsMountedRef';

import axios from '../../../utils/axios';

interface Props {
  lead?: LeadAsResponse;
  leadSetId?: number;
  isEditable?: boolean;
  isNew: boolean;
  children?: React.ReactNode | null;
}

type InitialValues = {
  email: string;
  afterSubmit?: any;
};

const validationSchema = yup.object({
  email: yup.string().email('Enter a valid email').required('Email is required')
  // firstName: yup.string().required('name is required'),
  // middleName: yup.string().required('name is required'),
  // lastName: yup.string().required('name is required')
});

const LeadForm = (props: Props) => {
  const { reloadLeadTable } = useLead();

  const { isEditable = true, isNew, children } = props;

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const isMountedRef = useIsMountedRef();

  const leadAdd = async (values: any) => {
    console.log(values);
    const response = await axios.post('/api/admin/lead', {
      leadSetId: props.leadSetId,
      email: values.email
    });
    const { lead, success, message } = response.data;

    if (success === true) {
      reloadLeadTable(true);
    } else {
      throw new Error(`Failed to add Lead. ${message}`);
    }
  };

  const onSubmitHandler = async (values: any, { setErrors, setSubmitting, resetForm }: any) => {
    if (isNew) {
      // This is add new
      try {
        await leadAdd(values);
        enqueueSnackbar('Lead added', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        resetForm();
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error });
          setSubmitting(false);
        }
      }
    }
  };

  const formik = useFormik<InitialValues>({
    initialValues: {
      email: ''
    },
    validationSchema,
    onSubmit: (values, { setErrors, setSubmitting, resetForm }) => {
      onSubmitHandler(values, { setErrors, setSubmitting, resetForm });
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, isValidating, getFieldProps } = formik;

  return (
    <div>
      <Typography variant="h6" sx={{ mb: 2 }}>
        Add an email address
      </Typography>
      <form onSubmit={handleSubmit}>
        {errors.afterSubmit && <Alert severity="error">{errors.afterSubmit}</Alert>}
        <Stack spacing={3}>
          <TextField
            fullWidth
            autoComplete="email"
            type="email"
            label="Lead Email"
            {...getFieldProps('email')}
            disabled={!isEditable}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />
          <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
            {isNew ? 'Add Lead' : 'Save Changes'}
          </LoadingButton>
        </Stack>
      </form>
    </div>
  );
};

export default LeadForm;
