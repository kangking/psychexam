import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router';
// material
import {
  Container,
  Typography,
  Paper,
  Grid,
  Pagination,
  Button,
  Card,
  CardHeader,
  TablePagination,
  Popper,
  Fade
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
// components
import { MChip, MButton, MIconButton } from '../../@material-extend';
import Page from '../../Page';
import LeadSetAddDialog from './adminLeadSetAddDialog';
import Scrollbar from '../../Scrollbar';

import { LeadSetAsResponse } from '../../../@types/lead';

import useLead from '../../../hooks/useLead';

import axios from '../../../utils/axios';

export default function AdminLeadSetList(props: any) {
  const navigate = useNavigate();
  const { leadSetTable, getLeadSetTable } = useLead();

  const [isLoading, setIsLoading] = useState(false);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState<number | null>(null);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [pagination, setPagination] = useState(1);
  const [limit, setLimit] = useState(10);
  const totalPages = useRef(0);
  const offset = useRef(0);

  useEffect(() => {
    fetchLeadSets();
  }, [pagination, limit]);

  const fetchLeadSets = () => {
    offset.current = pagination * limit - limit;
    setIsLoading(true);

    const params = {
      orderBy: { column: 'lead_set_created_at', direction: 'ASC' },
      limit: limit * 1,
      offset: offset.current,
      filter: { keyword: '', columns: ['lead_set_name'] },
      pagination
    };

    getLeadSetTable(params);
  };

  const handlePagination = (event: any, value: number) => {
    setPagination(value);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(+event.target.value);
    setPagination(1);
  };

  const handleOpenRow = (id: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
    navigate(`/admin/lead-sets/${id}`);
  };

  const handleDeleteConfirmation = (id: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
    setIdToDelete(id);
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const handleDeleteExecution = (id: any) => async (event: React.MouseEvent<HTMLButtonElement>) => {
    const response = await axios.delete(`/api/admin/lead-set?leadSetId=${id}`);
    if (response.data) {
      if (response.data.success) {
        fetchLeadSets();
        enqueueSnackbar('Delete success', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
      } else {
        console.log('deletion unsuccessful');
      }
    }
    setOpen(false);
  };

  const ShowLeadSetsRows = () => {
    if (!_.isEmpty(leadSetTable)) {
      return leadSetTable.map((row: LeadSetAsResponse) => (
        <TableRow
          hover
          key={row.lead_set_id}
          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
          <TableCell component="th" scope="row">
            {row.lead_set_name}
          </TableCell>
          <TableCell align="left">
            {moment(row.lead_set_created_at).format('MMM DD, YYYY - h:mm:ss a')}
          </TableCell>
          <TableCell align="right">
            <MButton
              sx={{ mr: 1 }}
              variant="outlined"
              size="small"
              color="primary"
              onClick={handleOpenRow(row.lead_set_id)}
            >
              Open
            </MButton>
            <MButton
              variant="outlined"
              size="small"
              color="inherit"
              onClick={handleDeleteConfirmation(row.lead_set_id)}
            >
              Delete
            </MButton>
          </TableCell>
        </TableRow>
      ));
    }
    return null;
  };

  const ShowLeadSetsTable = () => (
    <Scrollbar>
      <TableContainer sx={{ minWidth: 800, mt: 3 }}>
        <Table sx={{ minWidth: 650 }} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Name</TableCell>
              <TableCell align="left">Created At</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{ShowLeadSetsRows()}</TableBody>
        </Table>
      </TableContainer>
    </Scrollbar>
  );

  return (
    <Page title="Dashboard">
      <>
        <Card>
          <CardHeader title="Lead Sets" action={<LeadSetAddDialog />} />
          {ShowLeadSetsTable()}
          <Paper sx={{ p: 1, m: 2 }}>
            <Pagination
              count={totalPages.current}
              page={pagination}
              onChange={handlePagination}
              color="primary"
            />
          </Paper>
        </Card>
        <Popper open={open} anchorEl={anchorEl} placement="bottom-start" transition>
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper elevation={4} sx={{ p: 2 }}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography variant="h5" paragraph>
                      Are you sure you want to delete?
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      sx={{ mr: 2 }}
                      variant="contained"
                      color="error"
                      size="small"
                      onClick={handleDeleteExecution(idToDelete)}
                    >
                      Delete
                    </Button>
                    <Button
                      variant="outlined"
                      color="info"
                      size="small"
                      onClick={() => {
                        setOpen(false);
                      }}
                    >
                      Cancel
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Fade>
          )}
        </Popper>
      </>
    </Page>
  );
}
