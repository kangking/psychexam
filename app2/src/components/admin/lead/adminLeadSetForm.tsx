import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useSnackbar } from 'notistack';
import {
  Button,
  Link,
  Stack,
  Alert,
  Checkbox,
  TextField,
  IconButton,
  InputAdornment,
  FormControlLabel
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { MIconButton } from '../../@material-extend';
import { LeadSet, LeadSetAsResponse } from '../../../@types/lead';

// hooks
import useIsMountedRef from '../../../hooks/useIsMountedRef';

import axios from '../../../utils/axios';

interface Props {
  leadSet?: LeadSetAsResponse;
  isEditable?: boolean;
  isNew: boolean;
  children?: React.ReactNode | null;
}

type InitialValues = {
  name: string;
  afterSubmit?: any;
};

const validationSchema = yup.object({
  name: yup.string().required('name is required')
});

const LeadSetForm = (props: Props) => {
  const navigate = useNavigate();

  const { isEditable = true, isNew, children } = props;

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const isMountedRef = useIsMountedRef();

  const leadSetUpdate = async (data: InitialValues) => {
    const payload: any = {
      name: data.name,
      leadSetId: props.leadSet ? props.leadSet.lead_set_id : null
    };
    const response = await axios.put('/api/admin/lead-set', payload);
    const { leadSet, success, message } = response.data;

    if (success === true) {
      console.log('success');
    } else {
      throw new Error(`Failed to update Exam Set. ${message}`);
    }
  };

  const leadSetAdd = async (name: string) => {
    const response = await axios.post('/api/admin/lead-set', { name });
    const { leadSet, success, message } = response.data;

    if (success === true) {
      navigate(`/admin/lead-sets/${leadSet.data}`);
    } else {
      throw new Error(`Failed to add Lead Set. ${message}`);
    }
  };

  const onSubmitHandler = async (values: any, { setErrors, setSubmitting, resetForm }: any) => {
    if (isNew) {
      // This is add new
      try {
        await leadSetAdd(values.name);
        enqueueSnackbar('Lead set added', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        resetForm();
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error });
          setSubmitting(false);
        }
      }
    } else {
      // This is update existing
      try {
        await leadSetUpdate(values);
        enqueueSnackbar('Lead set updated', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        resetForm();
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error });
          setSubmitting(false);
        }
      }
    }
  };

  const formik = useFormik<InitialValues>({
    initialValues: {
      name: props.leadSet ? props.leadSet.lead_set_name : ''
    },
    validationSchema,
    onSubmit: (values, { setErrors, setSubmitting, resetForm }) => {
      onSubmitHandler(values, { setErrors, setSubmitting, resetForm });
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, isValidating, getFieldProps } = formik;

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Stack spacing={3}>
          {errors.afterSubmit && <Alert severity="error">{errors.afterSubmit}</Alert>}
          <TextField
            fullWidth
            autoComplete="name"
            type="text"
            label="Lead Set Name"
            {...getFieldProps('name')}
            disabled={!isEditable}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />
          <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
            {isNew ? 'Create Lead Set' : 'Save Changes'}
          </LoadingButton>
        </Stack>
      </form>
    </div>
  );
};

export default LeadSetForm;
