import React from 'react';
import moment from 'moment';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useSnackbar } from 'notistack';
import { Stack, Alert, TextField, Typography, Grid } from '@material-ui/core';
import { LoadingButton, DateTimePicker, LocalizationProvider } from '@material-ui/lab';
import { useDispatch } from 'react-redux';
import AdapterDateFns from '@date-io/date-fns';
import { Icon } from '@iconify/react';
import closeFill from '@iconify/icons-eva/close-fill';
import { MIconButton } from '../../@material-extend';
import { ExamSetItemAsResponse, ExamSetItemMeta } from '../../../@types/exam';
import { DoReload } from '../../../actions/ReloadActions';

// hooks
import useIsMountedRef from '../../../hooks/useIsMountedRef';

import axios from '../../../utils/axios';

interface Props {
  examSetItemMeta: ExamSetItemMeta;
  examSetItem: ExamSetItemAsResponse;
  isEditable?: boolean;
  children?: React.ReactNode | null;
  childSubmitButton?: any;
  doSubmit?: boolean;
  doneSubmit?: any;
}

type InitialValues = {
  startTime?: Date | string | null;
  endTime?: Date | string | null;
  afterSubmit?: any;
};

const validationSchema = yup.object({
  startTime: yup.date().required('date is required'),
  endTime: yup.date().required('date is required')
});

const ExamSetItemForm = (props: Props) => {
  const dispatch = useDispatch();
  const { examSetItemMeta, examSetItem, childSubmitButton, doSubmit, doneSubmit } = props;

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const isMountedRef = useIsMountedRef();
  const date = new Date();
  const formik = useFormik<InitialValues>({
    initialValues: {
      startTime: examSetItem
        ? moment(examSetItem.date_time_start).format('YYYY-MM-DDTkk:mm')
        : null,
      endTime: examSetItem ? moment(examSetItem.date_time_end).format('YYYY-MM-DDTkk:mm') : null
    },
    validationSchema,
    onSubmit: (values, { setErrors, setSubmitting, resetForm }) => {
      // console.log({ values });
      onSubmitHandler(values, { setErrors, setSubmitting, resetForm });
    }
  });

  const { errors, handleSubmit, isSubmitting } = formik;

  React.useEffect(() => {
    if (doSubmit) {
      handleSubmit();
    }
  }, [doSubmit]);

  const examUpdate = async (data: InitialValues) => {
    console.log('Time Debug', { start: formik.values.startTime, end: formik.values.endTime });
    const payload = {
      quizId: examSetItem ? examSetItem.fk_quiz_id : null,
      quizSetId: examSetItem ? examSetItem.fk_quiz_set_id : null,
      // startTime: moment(formik.values.startTime).format('YYYY-MM-DDTkk:mm'),
      // endTime: moment(formik.values.endTime).format('YYYY-MM-DDTkk:mm')
      startTime: formik.values.startTime,
      endTime: formik.values.endTime
      // endTime: '2021-10-13T01:35:0'
    };
    const response = await axios.put('/api/admin/quiz-set-item', payload);
    const { success, message } = response.data;

    if (success === true) {
      console.log('success');
      dispatch(DoReload({ type: 'RELOAD_EXAM_SET_ITEMS_TABLE' }));
    } else {
      throw new Error(`Failed to update Exam Set. ${message}`);
    }
  };

  const onSubmitHandler = async (values: any, { setErrors, setSubmitting, resetForm }: any) => {
    try {
      await examUpdate(values);
      enqueueSnackbar('Exam in Set is updated', {
        variant: 'success',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
      doneSubmit(false);
      if (isMountedRef.current) {
        setSubmitting(false);
        doneSubmit(false);
      }
    } catch (error) {
      resetForm();
      if (isMountedRef.current) {
        setErrors({ afterSubmit: error });
        setSubmitting(false);
        doneSubmit(false);
      }
    }
  };

  React.useEffect(() => {
    childSubmitButton(
      <LoadingButton form="editForm3" type="submit" variant="contained" loading={isSubmitting}>
        Save Changes
      </LoadingButton>
    );
  }, []);

  return (
    <>
      {/* {JSON.stringify(formik)}
      {JSON.stringify(errors)} */}
      <Grid container spacing={2}>
        <Grid item sm={6}>
          <Stack sx={{ mb: 3 }}>
            <Typography variant="overline" color="primary">
              Exam Set:
            </Typography>
            <Typography variant="subtitle1">{examSetItemMeta.examSetName}</Typography>
          </Stack>
        </Grid>
        <Grid item sm={6}>
          <Stack sx={{ mb: 3 }}>
            <Typography variant="overline" color="primary">
              Exam Name:
            </Typography>
            <Typography variant="subtitle1">{examSetItemMeta.examName}</Typography>
          </Stack>
        </Grid>
      </Grid>
      {errors.afterSubmit && <Alert severity="error">{errors.afterSubmit}</Alert>}

      <form id="editForm3" onSubmit={handleSubmit}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <Grid container spacing={2}>
            <Grid item sm={6}>
              <Stack>
                <TextField
                  fullWidth
                  label="Start Time"
                  type="datetime-local"
                  {...formik.getFieldProps('startTime')}
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={Boolean(formik.touched.startTime && formik.errors.startTime)}
                  helperText={formik.touched.startTime && formik.errors.startTime}
                />
              </Stack>
            </Grid>
            <Grid item sm={6}>
              <Stack>
                <TextField
                  fullWidth
                  label="End Time"
                  type="datetime-local"
                  {...formik.getFieldProps('endTime')}
                  InputLabelProps={{
                    shrink: true
                  }}
                  error={Boolean(formik.touched.endTime && formik.errors.endTime)}
                  helperText={formik.touched.endTime && formik.errors.endTime}
                />
              </Stack>
            </Grid>
          </Grid>
        </LocalizationProvider>
      </form>
    </>
  );
};

export default ExamSetItemForm;
