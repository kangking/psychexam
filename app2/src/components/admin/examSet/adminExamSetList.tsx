import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect, useRef } from 'react';
import { useNavigate } from 'react-router';
// material
import {
  Container,
  Typography,
  Paper,
  Grid,
  Pagination,
  Button,
  Card,
  CardHeader,
  TablePagination,
  Popper,
  Fade
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
// components
import { MChip, MButton, MIconButton } from '../../@material-extend';
import Page from '../../Page';
import ExamSetAddDialog from './adminExamSetAddDialog';
import Scrollbar from '../../Scrollbar';

import { ExamSetAsResponse } from '../../../@types/exam';

import axios from '../../../utils/axios';

export default function AdminAccessCodesPage() {
  const navigate = useNavigate();

  const [examSets, setExamSets] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [pagination, setPagination] = useState(1);
  const [limit, setLimit] = useState(10);
  const totalPages = useRef(0);
  const offset = useRef(0);

  const [showAddDrawer, setShowAddDrawer] = useState(false);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState<number | null>(null);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    fetchExamSets();
  }, [pagination, limit]);

  const fetchExamSets = async () => {
    offset.current = pagination * limit - limit;
    setIsLoading(true);

    const params = {
      orderBy: { column: 'quiz_set_created_at', direction: 'ASC' },
      limit: limit * 1,
      offset: offset.current,
      filter: { keyword: '', columns: ['quiz_set_name'] },
      pagination
    };

    axios
      .get('/api/admin/quiz-set-table', { params })
      .then((response) => {
        offset.current = pagination * limit - limit;
        totalPages.current = Math.ceil(response.data.quizSet.totalRows / limit);
        setExamSets(response.data.quizSet.data);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        throw error;
      });
  };

  const handleAdd = () => {
    setShowAddDrawer(true);
  };

  const handleOpenRow = (id: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
    navigate(`/admin/exam-sets/${id}`);
  };

  const handleDeleteConfirmation = (id: number) => (event: React.MouseEvent<HTMLButtonElement>) => {
    setIdToDelete(id);
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const handleDeleteExecution = (id: any) => async (event: React.MouseEvent<HTMLButtonElement>) => {
    const response = await axios.delete(`/api/admin/quiz-set?quizSetId=${id}`);
    if (response.data) {
      if (response.data.success) {
        fetchExamSets();
        enqueueSnackbar('Delete success', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
      } else {
        console.log('deletion unsuccessful');
      }
    }
    setOpen(false);
  };

  const handlePagination = (event: any, value: number) => {
    setPagination(value);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(+event.target.value);
    setPagination(1);
  };

  const ShowExamSetsRows = () => {
    if (!_.isEmpty(examSets)) {
      return examSets.map((row: ExamSetAsResponse) => (
        <TableRow
          hover
          key={row.quiz_set_id}
          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
          <TableCell component="th" scope="row">
            {row.quiz_set_name}
          </TableCell>
          <TableCell align="left">
            {moment(row.quiz_set_created_at).format('MMM DD, YYYY - h:mm:ss a')}
          </TableCell>
          <TableCell align="right">
            <MButton
              sx={{ mr: 1 }}
              variant="outlined"
              size="small"
              color="primary"
              onClick={handleOpenRow(row.quiz_set_id)}
            >
              Open
            </MButton>
            <MButton
              variant="outlined"
              size="small"
              color="inherit"
              onClick={handleDeleteConfirmation(row.quiz_set_id)}
            >
              Delete
            </MButton>
          </TableCell>
        </TableRow>
      ));
    }
    return null;
  };

  const ShowExamSetsTable = () => (
    <Scrollbar>
      <TableContainer sx={{ minWidth: 800, mt: 3 }}>
        <Table sx={{ minWidth: 650 }} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Code</TableCell>
              <TableCell align="left">Created At</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{ShowExamSetsRows()}</TableBody>
        </Table>
      </TableContainer>
    </Scrollbar>
  );

  return (
    <Page title="Dashboard">
      <>
        <Card>
          <CardHeader title="Exam Sets" action={<ExamSetAddDialog />} />
          {ShowExamSetsTable()}
          <Paper sx={{ p: 1, m: 2 }}>
            <Pagination
              count={totalPages.current}
              page={pagination}
              onChange={handlePagination}
              color="primary"
            />
          </Paper>
        </Card>
        <Popper open={open} anchorEl={anchorEl} placement="bottom-start" transition>
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper elevation={4} sx={{ p: 2 }}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography variant="h5" paragraph>
                      Are you sure you want to delete?
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      sx={{ mr: 2 }}
                      variant="contained"
                      color="error"
                      size="small"
                      onClick={handleDeleteExecution(idToDelete)}
                    >
                      Delete
                    </Button>
                    <Button
                      variant="outlined"
                      color="info"
                      size="small"
                      onClick={() => {
                        setOpen(false);
                      }}
                    >
                      Cancel
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Fade>
          )}
        </Popper>
      </>
    </Page>
  );
}
