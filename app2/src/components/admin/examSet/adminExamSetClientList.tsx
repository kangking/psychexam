import _ from 'lodash';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { Document, Packer } from 'docx';
// react
// material
import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Typography, Card, CardHeader, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import { useDispatch, useSelector } from 'react-redux';
import { LoadingButton } from '@material-ui/lab';

import axios from '../../../utils/axios';
import { MChip } from '../../@material-extend';
import TablePrime from '../../TablePrime';
import { Client } from '../../../@types/client';

import document from '../evaluation/documentBuilder';

interface ClientAnswerSheet extends Client {
  client_answer_sheet_id: number;
  client_answer_sheet_value: string;
  client_answer_sheet_is_done: boolean;
  client_answer_sheet_created_at: Date;
  fk_client_id: number;
  fk_quiz_set_quiz_id: number;
  fk_quiz_id: number;
  fk_quiz_Set_id: number;
  quiz_set_name: string;
}

interface ExamTakenAsResponse {
  client_answer_sheet_created_at: Date;
  client_answer_sheet_created_by: any;
  client_answer_sheet_deleted_at: Date;
  client_answer_sheet_deleted_by: any;
  client_answer_sheet_id: number;
  client_answer_sheet_is_deleted: boolean | number;
  client_answer_sheet_is_done: boolean | number;
  client_answer_sheet_result: any;
  client_answer_sheet_updated_at: Date;
  client_answer_sheet_updated_by: any;
  client_answer_sheet_value: any;
  date_time_end: Date;
  date_time_start: Date;
  quiz_duration: number;
  quiz_id: number;
  quiz_instruction: string;
  quiz_is_enabled: 1;
  quiz_name: string;
  quiz_slug: string;
}

interface Column {
  data: any;
  align: 'left' | 'center' | 'right' | 'justify' | 'inherit' | undefined;
}

export default function ExamSetClient() {
  const navigate = useNavigate();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const examSetId: number = Number(useParams().examSetId);

  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  const fetchAnalysis = async (clientId: number, examSetId: number) => {
    setIsSubmitting(true);
    const params = {
      clientId,
      quizSetId: examSetId
    };
    const response = await axios.get('/api/admin/client-answer-sheet-and-analysis', { params });

    setIsSubmitting(false);

    if (response.data.success) {
      return {
        client: response.data.client.data,
        results: response.data.results.data
      };
    }
    throw new Error(response.data.message);
  };

  const handleGenerateReport =
    (clientId: number) => async (event: React.MouseEvent<HTMLButtonElement>) => {
      try {
        const { client, results } = await fetchAnalysis(clientId, examSetId);
        // console.log({ results });
        const generateReport = async () => {
          let fileName = 'report.docx';

          if (client) {
            const { client_last_name, client_id, client_created_at } = client;
            const date = moment(client_created_at).format('DD-MMMM-YYYY');
            fileName = `${client_last_name}-${client_id}-${date}-report.docx`;
          }

          const file = await document({ client, results });

          Packer.toBlob(file).then((blob) => {
            saveAs(blob, fileName);
            console.log('Document created successfully');
          });
        };
        await generateReport();
      } catch (error) {
        alert(`Error occured! ${error}`);
      }
    };

  const headColumns: Column[] = [
    { data: 'Name', align: 'left' },
    { data: 'Set', align: 'left' },
    { data: 'Quizzes Taken', align: 'left' },
    { data: 'Actions', align: 'left' }
  ];

  const columns: Column[] = [
    {
      data: (row: Client) =>
        `${row.client_last_name}, ${row.client_first_name} ${row.client_middle_name}`,
      align: 'left'
    },
    { data: 'quiz_set_name', align: 'left' },
    {
      data: (row: any) => {
        console.log(row.client_quizzes_taken);
        // eslint-disable-next-line arrow-body-style
        return row.client_quizzes_taken.map((item: ExamTakenAsResponse) => {
          return item.client_answer_sheet_is_done === 1 ||
            item.client_answer_sheet_is_done === true ? (
            <MChip label={item.quiz_name} color="primary" size="small" />
          ) : (
            <MChip label={item.quiz_name} color="warning" size="small" />
          );
        });
      },
      align: 'left'
    },
    {
      data: (row: Client) => RowActions(row.client_id),
      align: 'left'
    }
    // { data: 'quiz_set_name', align: 'left' }
  ];

  function RowActions(id: number) {
    return (
      <>
        <LoadingButton
          sx={{ mr: 1 }}
          fullWidth={false}
          variant="outlined"
          color="primary"
          onClick={handleGenerateReport(id)}
          loading={isSubmitting}
        >
          Generate
        </LoadingButton>
        <LoadingButton
          sx={{ mr: 1 }}
          fullWidth={false}
          variant="outlined"
          color="primary"
          onClick={() => {
            navigate(`/admin/clients/${id}`);
          }}
          loading={isSubmitting}
        >
          View
        </LoadingButton>
      </>
    );
  }

  return (
    <Card sx={{ mt: 2 }}>
      <CardContent>
        <TablePrime
          orderBy={{ column: 'client_id', direction: 'ASC' }}
          filter={{ keyword: '', columns: ['client_last_name'] }}
          filterOptions={{ quizSetId: examSetId }}
          headColumns={headColumns}
          columns={columns}
          responseKey="client"
          fetchUrl="/api/admin/client-quiz-set-table"
          forceReload={reloadState.clientTable}
          forceReloadActionType={{
            reload: 'RELOAD_CLIENT_TABLE',
            doneReload: 'RELOAD_DONE_CLIENT_TABLE'
          }}
        />
      </CardContent>
    </Card>
  );
}
