import React from 'react';
import { useParams, useNavigate, useLocation } from 'react-router-dom';
import { Box, Tabs, Tab } from '@material-ui/core';

interface LinkTabProps {
  label?: string;
  href?: string;
}

interface Props {
  indicatorColor?: string;
  textColor?: string;
}

export default function NavTabs(props: Props) {
  const navigate = useNavigate();
  const location = useLocation();
  const examSetId = Number(useParams().examSetId);
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const passProps = () => {
    const attribs = [];
    if (props.indicatorColor) attribs.push({ indicatorColor: props.indicatorColor });
    if (props.textColor) attribs.push({ textColor: props.textColor });

    return attribs;
  };

  React.useEffect(() => {
    if (location.pathname === `/admin/exam-sets/${examSetId}/details`) {
      setValue(0);
    } else if (location.pathname === `/admin/exam-sets/${examSetId}/examinees`) {
      setValue(1);
    }
  }, []);

  const LinkTab = (props2: LinkTabProps) => (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        event.preventDefault();
        navigate(String(props2.href));
      }}
      {...props2}
    />
  );

  return (
    <Box sx={{ width: '100%' }}>
      <Tabs {...passProps()} value={value} onChange={handleChange} aria-label="nav tabs example">
        <LinkTab label="Exam Set" href="details" />
        <LinkTab label="Examinees" href="examinees" />
      </Tabs>
    </Box>
  );
}
