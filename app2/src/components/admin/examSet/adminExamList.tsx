import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect, useRef } from 'react';
// material
import {
  Container,
  Typography,
  Box,
  Paper,
  Grid,
  Pagination,
  Button,
  Card,
  CardHeader,
  TablePagination,
  Popper,
  Fade,
  Checkbox
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
// components
import { MChip, MButton, MIconButton } from '../../@material-extend';
import { DoneReload } from '../../../actions/ReloadActions';
import Scrollbar from '../../Scrollbar';

import { ExamAsResponse, ExamSetAsResponse, ExamSetItemAsResponse } from '../../../@types/exam';

import axios from '../../../utils/axios';

interface Props {
  examSet?: ExamSetAsResponse;
  children?: React.ReactNode | null;
  actionElements?: React.ReactNode | null;
  fetchURL?: string;
  fetchParams?: {};
  isRowSelectable?: boolean;
  isForSelection?: boolean;
  isPartOfExamSet?: boolean;
  rowActionsElements?: any;
  reload?: boolean;
  examIdsToParent?: any | null;
}

export default function AdminExamList(props: Props) {
  const {
    actionElements,
    rowActionsElements,
    examSet,
    fetchURL = '/api/admin/quiz-table',
    fetchParams,
    isRowSelectable = false,
    isPartOfExamSet = false,
    reload = false,
    examIdsToParent = null
  } = props;

  const dispatch = useDispatch();

  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  const [selected, setSelected] = useState<readonly number[]>([]);
  const [errors, setErrors] = useState<any[]>([]);

  const [exams, setExams] = useState<ExamAsResponse[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  const [pagination, setPagination] = useState(1);
  const [limit, setLimit] = useState(10);
  const totalPages = useRef(0);
  const offset = useRef(0);

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = useState(false);
  const [idToDelete, setIdToDelete] = useState<number | null>(null);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    fetchExams();
  }, [pagination, limit, fetchURL]);

  useEffect(() => {
    if (reload === true) {
      fetchExams();
    }
  }, [reload]);

  useEffect(() => {
    if (reloadState.examSetItemsTable === true) {
      fetchExams();
    }
  }, [reloadState.examSetItemsTable]);

  const fetchExams = async () => {
    offset.current = pagination * limit - limit;
    setIsLoading(true);

    const url = fetchURL;

    const params = {
      orderBy: { column: 'quiz_id', direction: 'ASC' },
      limit: limit * 1,
      offset: offset.current,
      filter: {
        keyword: examSet && isPartOfExamSet ? examSet.quiz_set_id : '',
        columns: [examSet && isPartOfExamSet ? 'fk_quiz_set_id' : 'quiz_id']
      },
      pagination,
      ...fetchParams
    };

    await axios
      .get(url, { params })
      .then((response) => {
        offset.current = pagination * limit - limit;
        totalPages.current = Math.ceil(response.data.quiz.totalRows / limit);
        setExams(response.data.quiz.data);
        setIsLoading(false);

        if (examIdsToParent) {
          if (!_.isEmpty(response.data.quiz.data)) {
            const tempExamIds = response.data.quiz.data.map((exam: ExamAsResponse) => exam.quiz_id);
            examIdsToParent(tempExamIds);
            // console.log(response, exams, tempExamIds, 'examlist return ids');
          }
        }

        dispatch(DoneReload({ type: 'RELOAD_DONE_EXAM_SET_ITEMS_TABLE' }));
      })
      .catch((error) => {
        setIsLoading(false);
        dispatch(DoneReload({ type: 'RELOAD_DONE_EXAM_SET_ITEMS_TABLE' }));
        throw error;
      });
  };

  const handleDeleteConfirmation = (id: any) => (event: React.MouseEvent<HTMLButtonElement>) => {
    setIdToDelete(id);
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const handleDeleteExecution =
    (id: any) => async (event: React.MouseEvent<HTMLButtonElement>) => {};

  const handlePagination = (event: any, value: number) => {
    setPagination(value);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLimit(+event.target.value);
    setPagination(1);
  };

  const addOrRemoveQuizInSet = async (id: number, selected: boolean) => {
    if (selected) {
      const response = await axios.post('');
    } else {
      const response = await axios.delete('');
    }
  };

  const handleClick = async (event: React.MouseEvent<unknown>, id: number, selected: boolean) => {
    try {
      await addOrRemoveQuizInSet(id, selected);
    } catch (err) {
      const newErrors = [...errors, err];
      setErrors(newErrors);
    }

    // const selectedIndex = selected.indexOf(id);
    // let newSelected: readonly number[] = [];

    // if (selectedIndex === -1) {
    //   newSelected = newSelected.concat(selected, id);
    // } else if (selectedIndex === 0) {
    //   newSelected = newSelected.concat(selected.slice(1));
    // } else if (selectedIndex === selected.length - 1) {
    //   newSelected = newSelected.concat(selected.slice(0, -1));
    // } else if (selectedIndex > 0) {
    //   newSelected = newSelected.concat(
    //     selected.slice(0, selectedIndex),
    //     selected.slice(selectedIndex + 1)
    //   );
    // }

    // setSelected(newSelected);
  };

  const renderRowActions = (row: ExamSetItemAsResponse) => {
    if (rowActionsElements) {
      return rowActionsElements(row);
    }
    return (
      <MButton
        variant="outlined"
        size="small"
        color="inherit"
        onClick={handleDeleteConfirmation(row.quiz_id)}
      >
        Delete
      </MButton>
    );
  };

  // const isSelected = (id: number) => selected.indexOf(id) !== -1;

  const ShowExamsRows = () => {
    if (!_.isEmpty(exams)) {
      return exams.map((row: ExamSetItemAsResponse, index: number) => {
        // const isItemSelected = isSelected(row.quiz_id);
        // console.log(selected);
        const labelId = `exam-list-table-checkbox-${index}`;

        return (
          <TableRow
            // onClick={(event) => handleClick(event, row.quiz_id, isItemSelected)}
            hover
            key={row.quiz_id}
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
          >
            <TableCell component="th" scope="row">
              {row.quiz_name}
            </TableCell>
            {/* <TableCell component="th" scope="row">
              {row.quiz_slug}
            </TableCell> */}
            <TableCell component="th" scope="row">
              {row.quiz_duration}
            </TableCell>
            {isPartOfExamSet ? (
              <>
                <TableCell component="th" scope="row">
                  {row.date_time_start
                    ? moment(row.date_time_start).format('MMM DD, YYYY - h:mm:ss a')
                    : 'No Schedule'}
                </TableCell>
                <TableCell component="th" scope="row">
                  {row.date_time_end
                    ? moment(row.date_time_end).format('MMM DD, YYYY - h:mm:ss a')
                    : 'No Schedule'}
                </TableCell>
              </>
            ) : null}
            {/* <TableCell align="left">
              {moment(row.quiz_created_at).format('MMM DD, YYYY - h:mm:ss a')}
            </TableCell> */}
            {/* {isRowSelectable ? (
              <TableCell>
                <Checkbox
                  color="primary"
                  checked={isItemSelected}
                  inputProps={{
                    'aria-labelledby': labelId
                  }}
                />{' '}
              </TableCell>
            ) : null}
            <TableCell align="right">{renderRowActions(row)}</TableCell> */}
            <TableCell align="right">{renderRowActions(row)}</TableCell>
          </TableRow>
        );
      });
    }
    return null;
  };

  const ShowExamsTable = () => (
    <Scrollbar>
      <TableContainer sx={{ minWidth: 800, mt: 3 }}>
        <Table sx={{ minWidth: 650 }} size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Exam</TableCell>
              {/* <TableCell align="left">Slug</TableCell> */}
              <TableCell align="left">Duration</TableCell>
              {isPartOfExamSet ? (
                <>
                  <TableCell align="left">Schedule Start</TableCell>
                  <TableCell align="left">Schedule End</TableCell>
                </>
              ) : null}
              {/* <TableCell align="left">Created At</TableCell> */}
              {isRowSelectable ? <TableCell sx={{ px: 2 }} /> : null}
              {!isRowSelectable ? <TableCell align="right">Actions</TableCell> : null}
            </TableRow>
          </TableHead>
          <TableBody>{ShowExamsRows()}</TableBody>
        </Table>
      </TableContainer>
    </Scrollbar>
  );

  return (
    <>
      <Card>
        <CardHeader title="Exam List" action={actionElements} />
        {ShowExamsTable()}
        <Paper sx={{ p: 1, m: 2 }}>
          <Pagination
            count={totalPages.current}
            page={pagination}
            onChange={handlePagination}
            color="primary"
          />
        </Paper>
      </Card>
      <Popper open={open} anchorEl={anchorEl} placement="bottom-start" transition>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <Paper elevation={4} sx={{ p: 2 }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography variant="h5" paragraph>
                    Are you sure you want to delete?
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Button
                    sx={{ mr: 2 }}
                    variant="contained"
                    color="error"
                    size="small"
                    onClick={handleDeleteExecution(idToDelete)}
                  >
                    Delete
                  </Button>
                  <Button
                    variant="outlined"
                    color="info"
                    size="small"
                    onClick={() => {
                      setOpen(false);
                    }}
                  >
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Fade>
        )}
      </Popper>
    </>
  );
}
