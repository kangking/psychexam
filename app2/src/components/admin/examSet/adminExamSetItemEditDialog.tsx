import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { LoadingButton } from '@material-ui/lab';
import { TransitionProps } from '@material-ui/core/transitions';
import { MChip, MButton, MIconButton } from '../../@material-extend';
import Form from './adminExamSetItemForm';
import { ExamSetItemAsResponse, ExamSetItemMeta } from '../../../@types/exam';

const Transition = React.forwardRef(
  (
    props: TransitionProps & {
      children?: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>
  ) => <Slide direction="up" ref={ref} {...props} />
);

interface Props {
  examSetItemMeta: ExamSetItemMeta;
  examSetItem: ExamSetItemAsResponse;
}

export default function AlertDialogSlide(props: Props) {
  const { examSetItemMeta, examSetItem } = props;
  const [open, setOpen] = React.useState(false);
  const [submitButton, setSubmitButton] = React.useState<React.ReactNode | null>(null);
  const [doSubmit, setDoSubmit] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <MButton variant="outlined" color="primary" onClick={handleClickOpen}>
        Edit Schedule
      </MButton>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>Set Schedule</DialogTitle>
        <DialogContent>
          <Form
            examSetItemMeta={examSetItemMeta}
            examSetItem={examSetItem}
            childSubmitButton={(button: any) => setSubmitButton(button)}
            doSubmit={doSubmit}
            doneSubmit={(done: boolean) => setDoSubmit(done)}
          />
        </DialogContent>
        <DialogActions>
          <Button variant="outlined" color="inherit" onClick={handleClose}>
            Cancel
          </Button>
          <LoadingButton
            onClick={() => {
              setDoSubmit(true);
              setOpen(false);
            }}
            variant="contained"
            loading={doSubmit}
          >
            Save Changes
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </div>
  );
}
