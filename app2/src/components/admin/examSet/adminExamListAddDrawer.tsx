import * as React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import { MChip, MButton, MIconButton } from '../../@material-extend';
import ExamList from './adminExamList';

import axios from '../../../utils/axios';
import { ExamSet, Exam, ExamSetAsResponse, ExamAsResponse } from '../../../@types/exam';

interface PassedRowExamAsResponse extends ExamAsResponse {
  isPartOfExamSet?: boolean;
}

interface Props {
  examSet: ExamSetAsResponse;
  examIds?: number[];
  reloadParent?: any;
  // disableButtons?: boolean;
}

const Transition = React.forwardRef(
  (
    props: TransitionProps & {
      children?: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>
  ) => <Slide direction="up" ref={ref} {...props} />
);

export default function AddDrawer(props: Props) {
  const { examIds, examSet, reloadParent } = props;
  const [open, setOpen] = React.useState(false);
  const [errors, setErrors] = React.useState<any[]>([]);
  const [disableButtons, setDisableButtons] = React.useState(false);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const addExamItem = async (id: number) => {
    const response = await axios.post('/api/admin/quiz-set-items', {
      quizId: id,
      quizSetId: examSet.quiz_set_id
    });
    if (response.data.success) {
      console.log('added!');
      enqueueSnackbar('Item added', {
        variant: 'success',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
      reloadParent();
    } else {
      enqueueSnackbar(`Add unsuccessful: ${response.data.message}`, {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
      throw new Error('ooops');
    }
  };

  const deleteExamItem = async (id: number) => {
    const response = await axios.delete(
      `/api/admin/quiz-set-items?quizId=${id}&quizSetId=${examSet.quiz_set_id}`
    );
    if (response.data.success) {
      console.log('deleted!');
      enqueueSnackbar('Item removed', {
        variant: 'success',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
      reloadParent();
    } else {
      enqueueSnackbar(`Delete unsuccessful: ${response.data.message}`, {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
      throw new Error('ooops');
    }
  };

  const handleAddExamItem = (id: number) => async (event: React.MouseEvent) => {
    setDisableButtons(true);
    try {
      await addExamItem(id);
    } catch (error) {
      setErrors((errors) => [...errors, error]);
    }
    setDisableButtons(false);
  };

  const handleRemoveExamItem = (id: number) => async (event: React.MouseEvent) => {
    setDisableButtons(true);
    try {
      await deleteExamItem(id);
    } catch (error) {
      setErrors((errors) => [...errors, error]);
    }
    setDisableButtons(false);
  };

  const isInExamSet = (id: number) => {
    if (examIds) {
      return examIds.indexOf(id) !== -1;
    }
    return false;
  };

  return (
    <div>
      <MButton variant="contained" color="primary" onClick={handleClickOpen}>
        Add Exams
      </MButton>
      <Drawer anchor="top" open={open} onClose={handleClose}>
        {JSON.stringify(examIds)}
        <ExamList
          isRowSelectable={true}
          rowActionsElements={(row: PassedRowExamAsResponse) => (
            <>
              <MButton
                variant="outlined"
                size="small"
                color="primary"
                onClick={handleAddExamItem(row.quiz_id)}
                disabled={isInExamSet(row.quiz_id) || disableButtons}
                sx={{ mr: 1 }}
              >
                Add
              </MButton>

              <MButton
                variant="outlined"
                size="small"
                color="inherit"
                disabled={!isInExamSet(row.quiz_id) || disableButtons}
                onClick={handleRemoveExamItem(row.quiz_id)}
              >
                Delete
              </MButton>
            </>
          )}
        />
      </Drawer>
    </div>
  );
}
