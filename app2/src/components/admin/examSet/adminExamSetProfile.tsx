import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// material
import { Container, Typography, Card, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { DoneReload } from '../../../actions/ReloadActions';
// components
import ExamSetForm from './adminExamSetForm';
import ExamList from './adminExamList';
import ExamListAddDrawer from './adminExamListAddDrawer';
import { MChip, MButton, MIconButton } from '../../@material-extend';
import ExamSetItemEditDialog from './adminExamSetItemEditDialog';

import axios from '../../../utils/axios';
import {
  ExamSet,
  Exam,
  ExamSetAsResponse,
  ExamAsResponse,
  ExamSetItemAsResponse
} from '../../../@types/exam';

export default function ExamSetProfile() {
  const dispatch = useDispatch();
  const [examSet, setExamSet] = useState<ExamSetAsResponse | null>(null);
  const [examItems, setExamItems] = useState<ExamAsResponse[]>([]);
  const [examItemsIds, setExamItemsIds] = useState<number[]>([]);
  const examSetId: number = Number(useParams().examSetId);
  const [reload, setReload] = useState(false);
  const [disableButtons, setDisableButtons] = useState(false);

  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  useEffect(() => {
    fetchExamSet();
    checker();
  }, []);

  useEffect(() => {
    if (reload === true) {
      fetchExamSet();
      checker();
    }
  }, [reload]);

  const checker = () => {
    const tempArr: number[] = [];
    examItems.forEach((item: ExamAsResponse) => {
      tempArr.push(item.quiz_id);
    });
    setExamItemsIds(tempArr);
  };

  const fetchExamSet = async () => {
    const response = await axios.get(`/api/admin/quiz-set?quizSetId=${examSetId}`);
    setExamSet(response.data.quizSet.data);
    setReload(false);
    dispatch(DoneReload({ type: 'RELOAD_DONE_EXAM_SET_ITEMS_TABLE' }));
  };

  const ShowData = () => {
    if (examSet) {
      return (
        <>
          <Card sx={{ mb: 2 }}>
            <CardContent>
              <ExamSetForm examSet={examSet} isEditable={true} isNew={false} />
            </CardContent>
          </Card>
          <ExamList
            examIdsToParent={(examIds: number[]) => {
              setExamItemsIds(examIds);
            }}
            reload={reload}
            examSet={examSet}
            isPartOfExamSet={true}
            fetchURL={`/api/admin/quiz-set-items-table?quizSetId=${examSetId}`}
            rowActionsElements={(row: ExamSetItemAsResponse) => (
              <>
                <ExamSetItemEditDialog
                  examSetItemMeta={{ examSetName: examSet.quiz_set_name, examName: row.quiz_name }}
                  examSetItem={row}
                />
                {/* <MButton
                  variant="outlined"
                  size="small"
                  color="primary"
                  onClick={handleEditSchedule(row)}
                  sx={{ mr: 1 }}
                >
                  Edit
                </MButton> */}
              </>
            )}
            actionElements={
              <ExamListAddDrawer
                examSet={examSet}
                examIds={examItemsIds}
                // disableButtons={disableButtons}
                reloadParent={() => {
                  setReload(true);
                }}
              />
            }
          />
        </>
      );
    }
    return null;
  };

  return (
    <>
      {/* <Typography variant="h6" paragraph>
        Exam Set
      </Typography> */}
      {ShowData()}
    </>
  );
}
