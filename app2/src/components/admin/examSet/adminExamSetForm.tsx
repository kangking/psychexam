import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useSnackbar } from 'notistack';
import {
  Button,
  Link,
  Stack,
  Alert,
  Checkbox,
  TextField,
  IconButton,
  InputAdornment,
  FormControlLabel
} from '@material-ui/core';
import { LoadingButton } from '@material-ui/lab';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { MIconButton } from '../../@material-extend';
import { ExamSet, ExamSetAsResponse } from '../../../@types/exam';

// hooks
import useIsMountedRef from '../../../hooks/useIsMountedRef';

import axios from '../../../utils/axios';

interface Props {
  examSet?: ExamSetAsResponse;
  isEditable?: boolean;
  isNew: boolean;
  children?: React.ReactNode | null;
}

type InitialValues = {
  name: string;
  afterSubmit?: any;
};

const validationSchema = yup.object({
  name: yup.string().required('name is required')
});

const ExamSetForm = (props: Props) => {
  const navigate = useNavigate();
  const { examSet } = props;

  const { isEditable = true, isNew, children } = props;

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const isMountedRef = useIsMountedRef();

  const examSetUpdate = async (data: InitialValues) => {
    const payload = { name: data.name, quizSetId: examSet ? examSet.quiz_set_id : null };
    const response = await axios.put('/api/admin/quiz-set', payload);
    const { quizSet, success, message } = response.data;

    if (success === true) {
      console.log('success');
    } else {
      throw new Error(`Failed to update Exam Set. ${message}`);
    }
  };

  const examSetAdd = async (name: string) => {
    const response = await axios.post('/api/admin/quiz-set', { name });
    const { quizSet, success, message } = response.data;

    if (success === true) {
      navigate(`/admin/exam-sets/${quizSet.data}`);
    } else {
      throw new Error(`Failed to add Exam Set. ${message}`);
    }
  };

  const onSubmitHandler = async (values: any, { setErrors, setSubmitting, resetForm }: any) => {
    if (isNew) {
      // This is add new
      try {
        await examSetAdd(values.name);
        enqueueSnackbar('Exam set added', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        resetForm();
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error });
          setSubmitting(false);
        }
      }
    } else {
      // This is update existing
      try {
        await examSetUpdate(values);
        enqueueSnackbar('Exam set updated', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        if (isMountedRef.current) {
          setSubmitting(false);
        }
      } catch (error) {
        resetForm();
        if (isMountedRef.current) {
          setErrors({ afterSubmit: error });
          setSubmitting(false);
        }
      }
    }
  };

  const formik = useFormik<InitialValues>({
    initialValues: {
      name: props.examSet ? props.examSet.quiz_set_name : ''
    },
    validationSchema,
    onSubmit: (values, { setErrors, setSubmitting, resetForm }) => {
      onSubmitHandler(values, { setErrors, setSubmitting, resetForm });
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, isValidating, getFieldProps } = formik;

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Stack spacing={3}>
          {errors.afterSubmit && <Alert severity="error">{errors.afterSubmit}</Alert>}
          <TextField
            fullWidth
            autoComplete="name"
            type="text"
            label="Exam Set Name"
            {...getFieldProps('name')}
            disabled={!isEditable}
            error={Boolean(touched.name && errors.name)}
            helperText={touched.name && errors.name}
          />
          <LoadingButton type="submit" variant="contained" loading={isSubmitting}>
            {isNew ? 'Create Exam Set' : 'Save Changes'}
          </LoadingButton>
        </Stack>
      </form>
    </div>
  );
};

export default ExamSetForm;
