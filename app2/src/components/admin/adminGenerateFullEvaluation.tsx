// material
import {
  Radio,
  FormControlLabel,
  Paper,
  Box,
  Grid,
  GridSize,
  Button,
  Container
} from '@material-ui/core';
// components
import _ from 'lodash';
import moment from 'moment';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { saveAs } from 'file-saver';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  UnderlineType,
  LevelFormat,
  AlignmentType,
  convertInchesToTwip
} from 'docx';
import { styles, numbering } from './documentStyles';
import { Client } from '../../@types/client';
import axios from '../../utils/axios';
import { setSentenceCase } from '../../utils/string';
import sectionFormat from './reportFormat';
import Logo from '../../assets/logo.png';
// ----------------------------------------------------------------------

interface Props {
  client: Client;
  results: {}[];
  style?: {};
}

const GenerateDoc = (props: Props) => {
  const image = useRef<any | null>(null);

  useEffect(() => {
    const loadImage = async () => {
      image.current = await fetch(Logo);
      image.current = await image.current.blob();
    };
    loadImage();
  }, []);

  const doc = new Document({
    styles,
    numbering,
    sections: [
      {
        properties: {},
        children: [...sectionFormat.sections(props.client, props.results, { logo: image.current })]
      }
    ]
  });

  const handleClick = () => {
    let fileName = 'report.docx';

    if (props.client) {
      const { client_last_name, client_id, client_created_at } = props.client;
      const date = moment(client_created_at).format('DD-MMMM-YYYY');
      fileName = `${client_last_name}-${client_id}-${date}-report.docx`;
    }

    Packer.toBlob(doc).then((blob) => {
      saveAs(blob, fileName);
      console.log('Document created successfully');
    });
  };

  return (
    <>
      <Button style={props.style} variant="contained" onClick={handleClick}>
        Generate Report
      </Button>
    </>
  );
};

export default GenerateDoc;
