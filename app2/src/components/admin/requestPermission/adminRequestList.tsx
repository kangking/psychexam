import _ from 'lodash';
// react
// material
import { Container, Typography, Card, CardHeader, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
// components
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import axios from '../../../utils/axios';
import { MChip, MButton, MIconButton } from '../../@material-extend';
import TablePrime from '../../TablePrime';
import { DoReload } from '../../../actions/ReloadActions';

type ClientQuizSetRequest = {
  client_quiz_set_request_id: number;
  client_quiz_set_request_is_allowed: boolean | number;
  client_quiz_set_request_id_created_at: Date | string;
  fk_client_id: number;
  fk_quiz_set_id: number;
  client_last_name: string;
  client_first_name: string;
  client_email: string;
};

export default function ClientQuizSetRequest() {
  const dispatch = useDispatch();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  const approveRequest =
    (clientId: number, examSetId: number) => async (event: React.MouseEvent) => {
      const payload = {
        clientId,
        quizSetId: examSetId
      };
      const response = await axios.post('/api/admin/client-quiz-set', payload);

      if (response.data.success) {
        enqueueSnackbar('Approved', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
        dispatch(DoReload({ type: 'RELOAD_REQUEST_EXAM_SET' }));
      } else {
        enqueueSnackbar(`Action failed: ${response.data.error}`, {
          variant: 'error',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
      }
    };

  const RenderActions = (clientId: number, examSetId: number) => (
    <MButton
      sx={{ mr: 1 }}
      variant="outlined"
      size="small"
      color="primary"
      onClick={approveRequest(clientId, examSetId)}
    >
      Approve
    </MButton>
  );

  interface Column {
    data: any;
    align: 'left' | 'center' | 'right' | 'justify' | 'inherit' | undefined;
  }

  const headColumns: Column[] = [
    { data: 'Name', align: 'left' },
    { data: 'Email', align: 'left' },
    { data: 'Permission', align: 'left' },
    { data: 'Quiz Set', align: 'left' },
    { data: 'Actions', align: 'left' }
  ];

  const columns: Column[] = [
    {
      data: (row: ClientQuizSetRequest) => `${row.client_last_name}, ${row.client_first_name}`,
      align: 'left'
    },
    { data: 'client_email', align: 'left' },
    { data: 'quiz_set_name', align: 'left' },
    {
      data: (row: ClientQuizSetRequest) =>
        row.client_quiz_set_request_is_allowed === 1 ||
        row.client_quiz_set_request_is_allowed === true ? (
          <MChip label="Allowed" color="primary" size="small" />
        ) : (
          <MChip label="Not Allowed" color="error" size="small" />
        ),
      align: 'left'
    },
    {
      data: (row: ClientQuizSetRequest) =>
        row.client_quiz_set_request_is_allowed === 0 ||
        row.client_quiz_set_request_is_allowed === false
          ? RenderActions(row.fk_client_id, row.fk_quiz_set_id)
          : ' ',
      align: 'left'
    }
    // { data: 'quiz_set_name', align: 'left' }
  ];

  return (
    <Card sx={{ mt: 2 }}>
      <CardHeader title="Request List" />
      <TablePrime
        orderBy={{ column: 'client_quiz_set_request_id', direction: 'ASC' }}
        filter={{ keyword: '', columns: [] }}
        headColumns={headColumns}
        columns={columns}
        responseKey="requests"
        fetchUrl="/api/admin/client-quiz-set-request-table"
        forceReload={reloadState.requestExamSetTable}
        forceReloadActionType={{
          reload: 'RELOAD_DONE_REQUEST_EXAM_SET',
          doneReload: 'RELOAD_DONE_REQUEST_EXAM_SET'
        }}
      />
    </Card>
  );
}
