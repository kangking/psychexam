import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { clientLabelsAll } from './profile/detailsConfig';
import { setSentenceCase, setPronounGender } from '../../utils/string';
import { stylesAsAttributes } from './documentStyles';

const sections = (client: any, results: any, images: any) => {
  let sortedResults: any = {};
  let gender = '';

  if (client) {
    // Setting the gender
    gender = client.client_gender;
    // Setting and sorting the results
    if (!_.isEmpty(results)) {
      const obj: any = {};
      results.forEach((item: any) => {
        obj[item.exam.quiz_slug] = { ...item };
      });
      sortedResults = obj;
    }

    const printArrayOfMessages = (messages: { message: string }[]): any[] => {
      const result: any[] = [];
      if (messages.length > 0) {
        messages.forEach((message) => {
          const tempMessage =
            message.message === ''
              ? 'No interpretation available'
              : setSentenceCase(setPronounGender(message.message, gender));
          result.push(
            new TextRun({
              text: tempMessage,
              break: 1
            })
          );
        });
      }
      return result;
    };

    const renderInterpretationTextParagraph = (
      object: any,
      key: string,
      interpKey: string = 'message'
    ) => {
      let message: string | [] | null = null;
      let result: any = null;

      const modifyString = (text: string) => setSentenceCase(setPronounGender(text, gender));

      if (Object.prototype.hasOwnProperty.call(object, key)) {
        message = object[key].interpretations[interpKey];

        if (Array.isArray(message)) {
          result = printArrayOfMessages(sortedResults[key].interpretations[interpKey]);
        }

        if (typeof message === 'string') {
          result = modifyString(object[key].interpretations[interpKey]);
          if (result == '') result = 'No interpretation available.';
        }
      }

      return result;
    };

    const insertValueOfClientTableCell = (item: any) => {
      // FULL NAME
      if (item.label === 'Name') {
        return `${client[item.key[0]] ? setSentenceCase(client[item.key[0]]) : ''}, ${
          client[item.key[1]] ? setSentenceCase(client[item.key[1]]) : ''
        } ${client[item.key[2]] ? setSentenceCase(client[item.key[2]]) : ''}.`;
      }
      // BIRTHDATE
      if (
        item.label === 'Birthdate' ||
        item.label === 'Date of Birth' ||
        item.label === 'Date of Testing'
      ) {
        return client[item.key] ? moment(client[item.key]).format('DD MMMM YYYY') : 'N/A';
      }
      // EMAIL
      if (item.label === 'Email' || item.label === 'Address') {
        return client[item.key] ? client[item.key] : 'N/A';
      }
      // GENDER
      if (item.label === 'Gender') {
        if (client[item.key]) {
          let gender = 'Female';
          if (client[item.key] === 'm' || client[item.key] === 'M') {
            gender = 'Male';
          }
          return gender;
        }
        return 'N/A';
      }
      // DEFAULT
      return client[item.key] ? client[item.key] : 'N/A';
    };

    const setLogo = () => {
      if (images.logo instanceof Promise || null) {
        return new TextRun({ text: '' });
      }
      return new ImageRun({
        data: images.logo,
        transformation: {
          width: 100,
          height: 100
        }
      });
    };

    // WORD DOCUMENT COMPONENT BUILDER

    const ClientDetailsTable = () =>
      new Table({
        width: { size: 3000 },
        columnWidths: [3505, 5505],
        rows: clientLabelsAll.map(
          (item: any) =>
            new TableRow({
              children: [
                new TableCell({
                  ...stylesAsAttributes.tableWidth.labels,
                  borders: stylesAsAttributes.noTableBorders.borders,
                  children: [
                    new Paragraph({
                      text: `${item.label}:`,
                      spacing: { before: 60, after: 120 },
                      style: 'clientTable'
                    })
                  ]
                }),
                new TableCell({
                  ...stylesAsAttributes.tableWidth.values,
                  borders: stylesAsAttributes.noTableBorders.borders,
                  children: [
                    new Paragraph({
                      text: insertValueOfClientTableCell(item),
                      spacing: { before: 60, after: 120 },
                      style: 'clientTable'
                    })
                  ]
                })
              ]
            })
        )
      });

    const TitleHeader = () =>
      new Paragraph({
        style: 'titleHeader',
        alignment: AlignmentType.CENTER,
        children: [
          setLogo(),
          new TextRun({ text: 'PeoplePsych Inc.', break: 1 }),
          new TextRun({ text: '3F Marietta Tower, Quezon Avenue, Iligan CIty 9200', break: 1 }),
          new TextRun({ text: 'Contact nos. 0917-711-0980 / 0999-994-9189', break: 1 }),
          new TextRun({ text: 'DR. CORA D. ESTENZO-LIM, RPsy', break: 1 }),
          new TextRun({ text: 'Licensed Clinical Psychologist', break: 1 })
        ]
      });

    const ReasonForReferral = () => new Paragraph({ text: 'text here...', style: 'normalContent' });

    const TestAdministered = () => new Paragraph({ text: 'text here...', style: 'normalContent' });

    const Findings = () => {
      console.log('prettier sucks');
      return [
        new Paragraph({
          style: 'traitHeader',
          children: [new TextRun('A. INTELLIGENCE')],
          ...stylesAsAttributes.traitHeaders
        }),
        new Paragraph({
          style: 'trait',
          text: renderInterpretationTextParagraph(sortedResults, 'intelligence-qoutient-test')
        }),
        new Paragraph({
          style: 'traitHeader',
          children: [new TextRun('B. PERSONALITY')],
          ...stylesAsAttributes.traitHeaders
        }),
        new Paragraph({
          style: 'personalityHeader',
          text: 'STRENGTHS:',
          ...stylesAsAttributes.personalityHeaders
        }),
        new Paragraph({
          style: 'personality',
          children: renderInterpretationTextParagraph(
            sortedResults,
            'personality-test-a',
            'strengths'
          )
        }),
        new Paragraph({
          style: 'personalityHeader',
          text: 'WEAKNESS/ES:',
          ...stylesAsAttributes.personalityHeaders
        }),
        new Paragraph({
          style: 'personality',
          children: renderInterpretationTextParagraph(
            sortedResults,
            'personality-test-a',
            'weaknesses'
          )
        }),
        new Paragraph({
          style: 'personalityHeader',
          text: 'REDEEMING QUALITIES:',
          ...stylesAsAttributes.personalityHeaders
        }),
        new Paragraph({
          style: 'personality',
          text: renderInterpretationTextParagraph(sortedResults, 'personality-test-b')
        }),
        new Paragraph({
          style: 'personalityHeader',
          text: 'PSYCHOLOGICAL NEEDS/STRATEGIES TO MAXIMIZE POTENTIAL DEVELOPMENT:',
          ...stylesAsAttributes.personalityHeaders
        }),
        new Paragraph({
          style: 'personality',
          text: renderInterpretationTextParagraph(sortedResults, 'personality-test-c')
        })
      ];
    };

    // RENDERING THE DOCUMENT BY SETIONS

    return [
      TitleHeader(),
      new Paragraph({
        children: [new TextRun({ text: 'Psychology Evaluation Report', bold: true })],
        ...stylesAsAttributes.introTitle
      }),
      new Paragraph({
        children: [new TextRun({ text: '(Strictly Confidential)' })],
        ...stylesAsAttributes.introSubtitle,
        style: 'subtitle'
      }),
      new Paragraph({ text: 'I. PERSONAL INFORMATION', ...stylesAsAttributes.numeralHeaders }),
      ClientDetailsTable(),
      new Paragraph({ text: 'II. REASON FOR REFERRAL', ...stylesAsAttributes.numeralHeaders }),
      ReasonForReferral(),
      new Paragraph({ text: 'III. TEST ADMINISTERED', ...stylesAsAttributes.numeralHeaders }),
      TestAdministered(),
      new Paragraph({ text: 'IV. FINDINGS', ...stylesAsAttributes.numeralHeaders }),
      ...Findings()
    ];
  }

  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { sections };
