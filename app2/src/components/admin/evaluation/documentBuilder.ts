import { useEffect, useRef } from 'react';
// material
import { Button } from '@material-ui/core';
// components
import _ from 'lodash';
import { Document } from 'docx';
import { styles, numbering } from './documentStyles';
import { Client } from '../../../@types/client';
import Logo from '../../../assets/logo.png';

import SectionHeader from './templates/sectionHeader';
import SectionPersonalInformation from './templates/sectionPersonalInformation';
import SectionReasonForReferral from './templates/sectionReasonForReferral';
import SectionTestAdministered from './templates/sectionTestAdministered';
import SectionFindings from './templates/sectionFindings';
import SectionSummaryAndRecomendations from './templates/sectionSummaryAndRecomendations';
// ----------------------------------------------------------------------

interface Options {
  client: Client;
  results: {}[];
  style?: {};
}

const loadImage = async (imageFile: RequestInfo) => {
  let image = null;
  image = await fetch(imageFile);
  image = await image.blob();

  return image;
};

export default async function GenerateDoc(options: Options) {
  const { client, results } = options;

  const images = {
    logo: await loadImage(Logo)
  };

  const document = new Document({
    styles,
    numbering,
    sections: [
      {
        properties: {},
        children: [
          ...SectionHeader.compose(client, results, images),
          ...SectionPersonalInformation.compose(client, results, images),
          ...SectionReasonForReferral.compose(client, results, images),
          ...SectionTestAdministered.compose(client, results, images),
          ...SectionFindings.compose(client, results, images, 'IV'),
          ...SectionSummaryAndRecomendations.compose(client, results, images)
          // add more
        ]
      }
    ]
  });

  return document;
}
