// material
import { Button } from '@material-ui/core';
// components
import _ from 'lodash';
import moment from 'moment';
import { useEffect, useRef } from 'react';
import { saveAs } from 'file-saver';
import { Document, Packer } from 'docx';
import axios from '../../../utils/axios';
import { styles, numbering } from './documentStyles';
import { Client } from '../../../@types/client';
import document from './documentBuilder';
import Logo from '../../assets/logo.png';
// ----------------------------------------------------------------------

interface Props {
  client: Client;
  results: any;
  style?: any;
}

const GeneratorButton = (props: Props) => {
  const { client, style, results } = props;
  console.log({ results });
  const handleClick = async () => {
    let fileName = 'report.docx';

    if (client) {
      const { client_last_name, client_id, client_created_at } = client;
      const date = moment(client_created_at).format('DD-MMMM-YYYY');
      fileName = `${client_last_name}-${client_id}-${date}-report.docx`;
    }

    const file = await document({ client, results });

    Packer.toBlob(file).then((blob) => {
      saveAs(blob, fileName);
      console.log('Document created successfully');
    });
  };

  return (
    <>
      <Button style={style} variant="contained" onClick={handleClick}>
        Generate Report
      </Button>
    </>
  );
};

export default GeneratorButton;
