import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  UnderlineType,
  LevelFormat,
  AlignmentType,
  convertInchesToTwip,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

const FONT_1 = 'Source Code Pro';

export const attributes = {
  tabStopLeft: {
    tier1: {
      tabStops: [
        /* {
          type: TabStopType.RIGHT,
          position: TabStopPosition.MAX
        }, */
        {
          type: TabStopType.LEFT,
          position: 200
        },
        {
          type: TabStopType.RIGHT,
          position: 2268
        }
      ]
    }
  }
};

export const styles = {
  default: {
    title: {
      run: {
        size: 34,
        bold: true,
        italics: false,
        font: FONT_1,
        color: '000000'
      },
      paragraph: {
        spacing: {
          after: 120
        }
      }
    },
    heading1: {
      run: {
        size: 36,
        bold: true,
        italics: false,
        font: FONT_1,
        color: '000000'
      },
      paragraph: {
        spacing: {
          after: 400
        }
      }
    },
    heading2: {
      run: {
        size: 32,
        bold: true,
        font: FONT_1,
        color: '000000'
      },
      paragraph: {
        spacing: {
          before: 400,
          after: 120
        }
      }
    },
    heading3: {
      run: {
        size: 30,
        bold: true,
        font: FONT_1,
        color: '000000'
      },
      paragraph: {
        spacing: {
          before: 240,
          after: 120
        }
      }
    },
    heading4: {
      run: {
        size: 28,
        bold: false,
        font: FONT_1,
        color: '000000'
      },
      paragraph: {
        spacing: {
          before: 400,
          after: 400
        }
      }
    },
    listParagraph: {
      run: {
        color: '#FF0000'
      }
    }
  },
  paragraphStyles: [
    // HeaderTitle
    {
      id: 'HeaderTitle',
      name: 'HeaderTitle',
      basedOn: 'Normal',
      next: 'Normal',
      paragraph: {
        spacing: {
          before: 20,
          after: 240
        }
      },
      run: {
        size: 22,
        font: FONT_1,
        color: '000000',
        bold: false,
        italics: true
      }
    },
    // HeaderSubTitle
    {
      id: 'HeaderSubTitle',
      name: 'HeaderSubTitle',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 32,
        font: FONT_1,
        color: '000000',
        bold: false,
        italics: true,
        underline: {}
      },
      paragraph: {
        alignment: AlignmentType.CENTER,
        spacing: {
          line: 276,
          after: 600
        }
      }
    },
    // Personal Info Table
    {
      id: 'PersonalInformationTable',
      name: 'PersonalInformationTable',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 26,
        font: FONT_1,
        color: '000000',
        bold: false
      },
      paragraph: {
        spacing: {
          line: 276
        }
      }
    },
    // Section Title Heading
    {
      id: 'TitleHeadingSection',
      name: 'TitleHeadingSection',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 32,
        font: FONT_1,
        color: '000000',
        bold: true
      },
      paragraph: {
        alignment: AlignmentType.LEFT,
        spacing: {
          line: 276,
          after: 300,
          before: 600
        }
      }
    },
    {
      id: 'SubTitle1HeadingSection',
      name: 'SubTitle1HeadingSection',
      run: {
        size: 32,
        font: FONT_1,
        color: 'FF0000',
        bold: true
      },
      paragraph: {
        alignment: AlignmentType.LEFT,
        spacing: {
          line: 276,
          after: 300,
          before: 600
        }
        // leftTabStop: 200,
        // tabStops: [
        //   {
        //     type: TabStopType.RIGHT,
        //     position: TabStopPosition.MAX
        //   },
        //   {
        //     type: TabStopType.LEFT,
        //     position: 1000
        //   }
        // ]
      }
    },
    {
      id: 'SubTitle2HeadingSection',
      name: 'SubTitle2HeadingSection',
      basedOn: 'Normal',
      next: 'Normal',
      run: {
        size: 26,
        font: FONT_1,
        color: '000000',
        bold: true
      },
      paragraph: {
        alignment: AlignmentType.LEFT,
        spacing: {
          line: 276,
          after: 300,
          before: 600
        }
      }
    },
    {
      id: 'ParagraphContent',
      name: 'ParagraphContent',
      run: {
        size: 24,
        font: FONT_1,
        color: '000000',
        bold: false
      },
      paragraph: {
        alignment: AlignmentType.LEFT,
        spacing: {
          line: 276,
          after: 200,
          before: 200
        }
      }
    },
    {
      id: 'ContentSection',
      name: 'ContentSection',
      basedOn: 'ParagraphContent',
      paragraph: {
        alignment: AlignmentType.LEFT
      }
    },
    {
      id: 'Sub1ContentSection',
      name: 'Sub1ContentSection',
      basedOn: 'ParagraphContent',
      paragraph: {
        alignment: AlignmentType.LEFT
      }
    },
    {
      id: 'Sub2ContentSection',
      name: 'Sub2ContentSection',
      basedOn: 'ParagraphContent',
      paragraph: {
        alignment: AlignmentType.LEFT
      }
    }
  ]
};

export const numbering = {
  config: [
    {
      reference: 'my-crazy-numbering',
      levels: [
        {
          level: 0,
          format: LevelFormat.LOWER_LETTER,
          text: '%1)',
          alignment: AlignmentType.LEFT
        }
      ]
    }
  ]
};
