import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { attributes } from '../documentStyles';

import FindingsIntelligence from './findings/subSectionFindingsIntelligence';
import FindingsPersonality from './findings/subSectionFindingsPersonality';
import { setSentenceCase, setPronounGender } from '../../../../utils/string';

const compose = (
  client: any,
  results: any,
  images: any,
  ordinal: string | number | null
): Paragraph[] => {
  let sortedResults: any = {};
  // eslint-disable-next-line prefer-const
  let gender = '';

  if (client) {
    /**
     * Sorts Results
     */
    if (!_.isEmpty(results)) {
      const obj: any = {};
      results.forEach((item: any) => {
        obj[item.exam.quiz_slug] = { ...item };
      });
      sortedResults = obj;
      console.log(sortedResults);
    }

    /**
     * When paragraphs have more TextRuns, use this
     * @param messages
     * @returns array of modified messages
     */
    const printArrayOfMessages = (messages: { message: string }[]): any[] => {
      const result: any[] = [];
      if (messages.length > 0) {
        messages.forEach((message) => {
          const tempMessage =
            message.message === '' ? 'No interpretation available' : modifyString(message.message);
          result.push(tempMessage);
        });
      }
      return result;
    };

    /**
     * Modifies the gender pronouns of messages
     * @param string
     * @returns modifies strings by gender and etc.
     */
    const modifyString = (text: string) => setSentenceCase(setPronounGender(text, gender));

    /**
     *
     * @param object is the sortedResults
     * @param key key of object
     * @param interpKey is specific key used to hold the value of message
     * @returns either modified array of textruns or string
     */
    const renderInterpretationTextParagraph = (
      object: any,
      key: string,
      interpKey: string = 'message' // strengths | weaknesses | neutrals
    ) => {
      let message: string | [] | null = null;
      let result: any = null;

      if (Object.prototype.hasOwnProperty.call(object, key)) {
        message = object[key].interpretations[interpKey];

        if (Array.isArray(message)) {
          result = printArrayOfMessages(sortedResults[key].interpretations[interpKey]);
        }

        if (typeof message === 'string') {
          result = modifyString(object[key].interpretations[interpKey]);
          if (result === '') result = 'No interpretation available.';
        }
      } else {
        result = 'No interpretation available.';
      }
      return result;
    };

    /**
     * WORD DOCUMENT COMPONENT BUILDER
     */

    /**
     * Builds the Title Heading
     * @returns Paragraph
     */
    const SectionTitleHeading = () =>
      new Paragraph({
        style: 'TitleHeadingSection',
        text: `${ordinal || 'IV'}. FINDINGS`
      });

    /**
     * Builds the Content
     * @returns array of Paragraphs
     */
    const MainContent = () => [
      // ...FindingsIntelligence.compose(
      //   client,
      //   renderInterpretationTextParagraph(sortedResults, 'intelligence-qoutient-test'),
      //   'A'
      // ),
      ...FindingsPersonality.compose(
        client,
        [
          {
            heading: 'STRENGTHS:',
            text: renderInterpretationTextParagraph(
              sortedResults,
              'personality-test-a',
              'strengths'
            )
          },
          {
            heading: 'WEAKNESSES:',
            text: renderInterpretationTextParagraph(
              sortedResults,
              'personality-test-a',
              'weaknesses'
            )
          },
          {
            heading: 'NEUTRALS:',
            text: renderInterpretationTextParagraph(sortedResults, 'personality-test-a', 'neutrals')
          }
          // {
          //   heading: 'REDEEMING QUALITIES:',
          //   text: renderInterpretationTextParagraph(sortedResults, 'personality-test-b')
          // },
          // {
          //   heading: 'PSYCHOLOGICAL NEEDS/STRATEGIES TO MAXIMIZE POTENTIAL DEVELOPMENT:',
          //   text: renderInterpretationTextParagraph(sortedResults, 'personality-test-c')
          // }
        ],
        'A'
      )
    ];

    /**
     * Return everything as Array
     */
    return [SectionTitleHeading(), ...MainContent()];
  }

  /**
   * Returns a default value
   */
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { compose };
