import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { setSentenceCase, setPronounGender } from '../../../../utils/string';

const compose = (client: any, results: any, images: any): Paragraph[] => {
  if (client) {
    const setLogo = () => {
      if (images.logo instanceof Promise || null) {
        return new TextRun({ text: '' });
      }
      return new ImageRun({
        data: images.logo,
        transformation: {
          width: 100,
          height: 100
        }
      });
    };

    // WORD DOCUMENT COMPONENT BUILDER

    const SectionTitleHeading = () =>
      new Paragraph({ style: 'TitleHeadingSection', text: 'III. TESTS ADMINISTERED' });

    const MainContent = () => [
      new Paragraph({
        style: 'ParagraphContent',
        children: [new TextRun({ text: 'Paragraph text content here...', break: 1 })]
      })
    ];

    // RETURNING THE SECTIONS

    return [SectionTitleHeading(), ...MainContent()];
  }

  // RETURNS A DEFAULT VALUE IF DATA IS NOT PROVIDED
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { compose };
