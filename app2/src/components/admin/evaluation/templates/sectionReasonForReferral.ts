import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { setSentenceCase, setPronounGender } from '../../../../utils/string';

const compose = (client: any, results: any, images: any): Paragraph[] => {
  if (client) {
    // WORD DOCUMENT COMPONENT BUILDER

    const SectionTitleHeading = () =>
      new Paragraph({ style: 'TitleHeadingSection', text: 'II. REASONS FOR REFERRAL' });

    const MainContent = () => [
      new Paragraph({
        style: 'ParagraphContent',
        children: [new TextRun({ text: 'Paragraph text content here...', break: 1 })]
      }),
      new Paragraph({
        style: 'ContentSection',
        children: [new TextRun({ text: 'Paragraph text content here...', break: 1 })]
      })
    ];

    // RETURNING THE SECTIONS

    return [SectionTitleHeading(), ...MainContent()];
  }

  // RETURNS A DEFAULT VALUE IF DATA IS NOT PROVIDED
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { compose };
