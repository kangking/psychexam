import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { setSentenceCase, setPronounGender } from '../../../../utils/string';

const compose = (client: any, results: any, images: any): Paragraph[] => {
  if (client) {
    const setLogo = () => {
      if (images.logo instanceof Promise || null) {
        return new TextRun({ text: '' });
      }
      return new ImageRun({
        data: images.logo,
        transformation: {
          width: 100,
          height: 100
        }
      });
    };

    // WORD DOCUMENT COMPONENT BUILDER

    const Header = () =>
      new Paragraph({
        style: 'HeaderTitle',
        alignment: AlignmentType.CENTER,
        children: [
          setLogo(),
          new TextRun({ text: 'PeoplePsych Inc.', break: 1 }),
          new TextRun({ text: '3F Marietta Tower, Quezon Avenue, Iligan CIty 9200', break: 1 }),
          new TextRun({ text: 'Contact nos. 0917-711-0980 / 0999-994-9189', break: 1 }),
          new TextRun({ text: 'DR. CORA D. ESTENZO-LIM, RPsy', break: 1 }),
          new TextRun({ text: 'Licensed Clinical Psychologist', break: 1 })
        ]
      });

    const EvaluationTitle = () =>
      new Paragraph({
        style: 'HeaderSubTitle',
        children: [new TextRun({ text: 'Psychological Evaluation Report', bold: true })]
      });

    // RETURNING THE SECTIONS

    return [Header(), EvaluationTitle()];
  }

  // RETURNS A DEFAULT VALUE IF DATA IS NOT PROVIDED
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { compose };
