import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { clientLabelsAll } from '../../profile/detailsConfig';
import { setSentenceCase, setPronounGender } from '../../../../utils/string';

const compose = (client: any, results: any, images: any): Paragraph[] | Table[] => {
  if (client) {
    // WORD DOCUMENT COMPONENT BUILDER

    const SectionTitleHeading = () =>
      new Paragraph({ style: 'TitleHeadingSection', text: 'I. PERSONAL INFORMATION' });

    const PersonalInfoTable = () =>
      new Table({
        width: { size: 3000 },
        columnWidths: [3505, 5505],
        rows: clientLabelsAll.map(
          (item: any) =>
            new TableRow({
              children: [
                new TableCell({
                  ...inlineStyles.tableWidth.labels,
                  borders: inlineStyles.noTableBorders.borders,
                  children: [
                    new Paragraph({
                      text: `${item.label}:`,
                      spacing: { before: 60, after: 120 },
                      style: 'PersonalInformationTable'
                    })
                  ]
                }),
                new TableCell({
                  ...inlineStyles.tableWidth.labels,
                  borders: inlineStyles.noTableBorders.borders,
                  children: [
                    new Paragraph({
                      text: insertValueOfClientTableCell(item, client),
                      spacing: { before: 60, after: 120 },
                      style: 'PersonalInformationTable'
                    })
                  ]
                })
              ]
            })
        )
      });

    // RETURNING THE SECTIONS

    return [SectionTitleHeading(), PersonalInfoTable()];
  }

  // RETURNS A DEFAULT VALUE IF DATA IS NOT PROVIDED
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

const insertValueOfClientTableCell = (item: any, client: any) => {
  // FULL NAME
  if (item.label === 'Name') {
    return `${client[item.key[0]] ? setSentenceCase(client[item.key[0]]) : ''}, ${
      client[item.key[1]] ? setSentenceCase(client[item.key[1]]) : ''
    } ${client[item.key[2]] ? setSentenceCase(client[item.key[2]]) : ''}.`;
  }
  // BIRTHDATE
  if (
    item.label === 'Birthdate' ||
    item.label === 'Date of Birth' ||
    item.label === 'Date of Testing'
  ) {
    return client[item.key] ? moment(client[item.key]).format('DD MMMM YYYY') : 'N/A';
  }
  // EMAIL
  if (item.label === 'Email' || item.label === 'Address') {
    return client[item.key] ? client[item.key] : 'N/A';
  }
  // GENDER
  if (item.label === 'Gender') {
    if (client[item.key]) {
      let gender = 'Female';
      if (client[item.key] === 'm' || client[item.key] === 'M') {
        gender = 'Male';
      }
      return gender;
    }
    return 'N/A';
  }
  // DEFAULT
  return client[item.key] ? client[item.key] : 'N/A';
};

const inlineStyles = {
  tableWidth: {
    labels: {
      width: {
        size: 50,
        type: WidthType.PERCENTAGE
      }
    },
    values: {
      width: {
        size: 50,
        type: WidthType.PERCENTAGE
      }
    }
  },
  noTableBorders: {
    borders: {
      top: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      },
      bottom: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      },
      left: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      },
      right: {
        style: BorderStyle.DASH_DOT_STROKED,
        size: 0,
        color: 'ffffff'
      }
    }
  }
};

export default { compose };
