import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { attributes } from '../../documentStyles';

const compose = (client: any, value: any, ordinal: string | number | null): Paragraph[] => {
  if (client) {
    // WORD DOCUMENT COMPONENT BUILDER

    const SectionSubTitle1Heading = () =>
      new Paragraph({
        style: 'SubTitle1HeadingSection',
        children: [new TextRun(`${ordinal || 'A'}. INTELLIGENCE`)],
        ...attributes.tabStopLeft.tier1
      });

    const MainContent = () => [
      new Paragraph({
        style: 'Sub1ContentSection',
        text: value
      })
    ];

    // RETURNING THE SECTIONS

    return [SectionSubTitle1Heading(), ...MainContent()];
  }

  // RETURNS A DEFAULT VALUE IF DATA IS NOT PROVIDED
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { compose };
