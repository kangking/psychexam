import _ from 'lodash';
import { useEffect, useRef } from 'react';
import moment from 'moment';
import docx, {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  HeadingLevel,
  AlignmentType,
  BorderStyle,
  WidthType,
  TabStopType,
  TabStopPosition,
  ImageRun
} from 'docx';

import { attributes } from '../../documentStyles';

const compose = (client: any, values: any, ordinal: string | number | null): Paragraph[] => {
  console.log(values);
  if (client) {
    // CREATES PARAG
    const paragraphBuilder = () => {
      const paragraphs: Paragraph[] = [];
      values.forEach((value: any) => {
        paragraphs.push(
          new Paragraph({
            style: 'SubTitle2HeadingSection',
            children: [new TextRun({ text: value.heading })]
          })
        );

        let children = [];

        if (Array.isArray(value.text)) {
          children = value.text.map((textItem: string, index: number) => {
            const textRun: { text: string; break?: number } = { text: textItem, break: 2 };
            if (index === 0) {
              textRun.break = 0;
            }
            // eslint-disable-next-line no-new
            return new TextRun(textRun);
          });
        } else {
          children = [new TextRun({ text: value.text })];
        }

        paragraphs.push(
          new Paragraph({
            style: 'Sub2ContentSection',
            children
          })
        );
      });

      return paragraphs;
    };

    // WORD DOCUMENT COMPONENT BUILDER

    const SectionSubTitle1Heading = () =>
      new Paragraph({
        style: 'SubTitle1HeadingSection',
        children: [new TextRun(`${ordinal || 'B'}. PERSONALITY`)],
        ...attributes.tabStopLeft.tier1
      });

    const MainContent = () => [...paragraphBuilder()];

    // RETURNING THE SECTIONS

    return [SectionSubTitle1Heading(), ...MainContent()];
  }

  // RETURNS A DEFAULT VALUE IF DATA IS NOT PROVIDED
  return [
    new Paragraph({
      children: [
        new TextRun(
          'Client Data and Results are not yet loaded. Please refresh or wait for a few seconds.'
        )
      ]
    })
  ];
};

export default { compose };
