import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
// material
import { Container, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../Page';
import ClientDetails from './profile/adminClientDetails';
import ClientExams from './profile/adminClientExamResults';
import GenerateFullEvaluation from './adminGenerateFullEvaluation';
import axios from '../../utils/axios';
import { Client } from '../../@types/client';

export default function ClientPage() {
  const [client, setClient] = useState<Client | null>(null);
  const clientId: number = Number(useParams().id);

  useEffect(() => {
    const initialize = async () => {
      const response = await axios.get(`/api/admin/client?clientId=${clientId}`);
      setClient(response.data.client.data);
    };
    initialize();
  }, []);

  const ShowData = () => {
    if (client) {
      return (
        <>
          <ClientDetails client={client} />
          <ClientExams client={client} />
        </>
      );
    }
    return null;
  };

  return (
    <Container maxWidth="xl">
      <Typography variant="h6" paragraph>
        Profile
      </Typography>
      {ShowData()}
    </Container>
  );
}
