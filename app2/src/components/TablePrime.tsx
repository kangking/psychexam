import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect, useRef } from 'react';
// material
import {
  Container,
  Typography,
  Box,
  Paper,
  Grid,
  Pagination,
  Button,
  Card,
  CardHeader,
  TablePagination,
  Popper,
  Fade
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
// components
import { useDispatch, useSelector } from 'react-redux';
import { MChip, MButton, MIconButton } from './@material-extend';
import Page from './Page';
import Scrollbar from './Scrollbar';
import { DoneReload } from '../actions/ReloadActions';

import axios from '../utils/axios';

interface OrderBy {
  column: string;
  direction: 'ASC' | 'DESC';
}

interface Filter {
  keyword: string;
  columns: string[];
}

interface Params {
  orderBy: OrderBy;
  limit: number;
  offset: number;
  filter: Filter;
  pagination: number;
}

interface Column {
  data: any;
  align: 'left' | 'center' | 'right' | 'justify' | 'inherit' | undefined;
}

interface Props {
  orderBy: OrderBy;
  filter: Filter;
  filterOptions?: any;
  headColumns: Column[];
  columns: Column[];
  responseKey: string;
  fetchUrl: string;
  forceReload?: boolean;
  forceReloadActionType: { reload: string; doneReload: string };
}

export default function useTable(props: Props) {
  const {
    orderBy,
    filter,
    filterOptions = null,
    headColumns,
    columns,
    fetchUrl,
    responseKey,
    forceReload = false,
    forceReloadActionType = {
      reload: '',
      doneReload: ''
    }
  } = props;

  const dispatch = useDispatch();

  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [pagination, setPagination] = useState(1);
  const [limit, setLimit] = useState(10);
  const totalPages = useRef(0);
  const offset = useRef(0);

  useEffect(() => {
    fetchRows();
  }, [pagination, limit]);

  useEffect(() => {
    if (forceReload) {
      try {
        fetchRows();
        dispatch(
          DoneReload({
            type: forceReloadActionType.doneReload
          })
        );
      } catch (error) {
        alert(`could not load table: ${error}`);
      }
    }
  }, [forceReload]);

  const fetchRows = async () => {
    offset.current = pagination * limit - limit;
    setIsLoading(true);

    const params = {
      orderBy,
      limit: limit * 1,
      offset: offset.current,
      filter,
      pagination,
      options: filterOptions
    };

    await axios
      .get(fetchUrl, { params })
      .then((response) => {
        offset.current = pagination * limit - limit;
        totalPages.current = Math.ceil(response.data[responseKey].totalRows / limit);
        setRows(response.data[responseKey].data);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        throw new Error(error);
      });
  };

  const handlePagination = (event: any, value: number) => {
    setPagination(value);
  };

  const ShowTableHead = () => {
    const headCols = headColumns.map((col: Column, idx: number) => (
      <TableCell key={`head-cell-${idx}`} align={col.align}>
        {col.data}
      </TableCell>
    ));

    return (
      <TableRow hover sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
        {headCols}
      </TableRow>
    );
  };

  const ShowTableBody = () => {
    let bodyCols = null;
    if (rows) {
      bodyCols = rows.map((row, idx) => (
        <TableRow hover key={`row-body-${idx}`}>
          {columns.map((col: Column, idx: number) => (
            <TableCell key={`head-cell-${idx}`} align={col.align}>
              {typeof col.data === 'function' ? col.data(row) : row[col.data]}
            </TableCell>
          ))}
        </TableRow>
      ));
    }

    return bodyCols;
  };

  const ShowTable = () => (
    <>
      <Scrollbar>
        <TableContainer sx={{ minWidth: 800, mt: 3 }}>
          <Table size="small" aria-label="simple table">
            <TableHead>{ShowTableHead()}</TableHead>
            <TableBody>{ShowTableBody()}</TableBody>
          </Table>
        </TableContainer>
      </Scrollbar>
      <Paper sx={{ p: 1, m: 2 }}>
        <Pagination
          count={totalPages.current}
          page={pagination}
          onChange={handlePagination}
          color="primary"
        />
      </Paper>
    </>
  );

  return ShowTable();
}
