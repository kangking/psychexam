import _ from 'lodash';
import moment from 'moment';
// react
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
// material
import {
  Container,
  Typography,
  Card,
  CardHeader,
  CardContent,
  Grid,
  Button,
  Stack
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../Page';
import axios from '../../utils/axios';
import { ExamSetItemAsResponse } from '../../@types/exam';

interface ClientQuizSet {
  fk_quiz_set_id: number;
  fk_client_id: number;
  quiz_set_name: string;
}

interface ClientQuizSetWithQuiz extends ClientQuizSet {
  quiz_set_items: ExamSetItemAsResponse[];
}

interface Checker {
  id: number;
  canTake: boolean;
}

export default function ExamList() {
  const navigate = useNavigate();
  const [examSets, setExamSets] = useState<ClientQuizSetWithQuiz[]>([]);
  const [minuteElapsed, setMinuteElapsed] = useState(0);
  const [checkerArray, setCheckerArray] = useState<Checker[]>([]);

  const initialize = async () => {
    const response = await axios.get('/api/client-quiz-set');
    setExamSets(response.data.clientQuizSets.data);
  };

  useEffect(() => {
    initialize();

    setInterval(() => {
      setMinuteElapsed((v) => v + 1);
    }, 5000);
  }, []);

  useEffect(() => {
    if (!_.isEmpty(examSets)) {
      checkQuizSchedule();
    }
  }, [examSets]);

  useEffect(() => {
    // initialize();
    console.log(minuteElapsed);
    if (!_.isEmpty(examSets)) {
      checkQuizSchedule();
    }
  }, [minuteElapsed]);

  const checkQuizSchedule = async () => {
    if (!_.isEmpty(examSets)) {
      for (let i = 0; i < examSets.length; i++) {
        for (let j = 0; j < examSets[i].quiz_set_items.length; j++) {
          let canTake: any = false;
          try {
            // eslint-disable-next-line no-await-in-loop
            canTake = await fetchQuizSchedule(examSets[i].quiz_set_items[j].quiz_set_quiz_id);
            const foundIndex = checkerArray.findIndex(
              (item: any) => item.id === examSets[i].quiz_set_items[j].quiz_set_quiz_id
            );

            if (foundIndex > -1) {
              setCheckerArray((arr: any) => {
                const temp = [...arr];
                temp[foundIndex].canTake = canTake;
                return temp;
              });
            } else {
              setCheckerArray((arr: any) => [
                ...arr,
                {
                  id: examSets[i].quiz_set_items[j].quiz_set_quiz_id,
                  canTake
                }
              ]);
            }
          } catch (error) {
            console.log(error);
          }
        }
      }
    }
  };

  const fetchQuizSchedule = async (id: number | undefined) => {
    if (id !== undefined) {
      const response = await axios.get(`/api/quiz-schedule?quizSetItemId=${id}`);
      const { success, quizSchedule } = response.data;

      if (success) {
        return quizSchedule.canTake;
      }
    }

    return false;
  };

  const isTakeQuizButtonDisabled = (exam: any) => {
    let canTake: boolean = false;

    if (!_.isEmpty(checkerArray)) {
      const found = checkerArray.find((item: any) => item.id === exam.quiz_set_quiz_id);

      if (found) {
        canTake = found.canTake;
      }
    }
    // console.log(
    //   Boolean(exam.client_answer_sheet_is_done),
    //   !canTake,
    //   Boolean(exam.client_answer_sheet_is_done) || !canTake
    // );
    return Boolean(exam.client_answer_sheet_is_done) || !canTake;
  };

  const ShowExamSets = () => {
    let elementExamSets = null;

    elementExamSets = examSets.map((examSet: ClientQuizSetWithQuiz, index: number) => (
      <Grid container spacing={2} key={`exam-set-${index}`}>
        <Grid item xs={12}>
          <Typography variant="subtitle1" sx={{ mt: 2 }}>
            {examSet.quiz_set_name ? examSet.quiz_set_name : 'No name'}
          </Typography>
        </Grid>
        {ShowExamList(examSet.quiz_set_items)}
      </Grid>
    ));

    return elementExamSets;
  };

  const ShowExamList = (examSetItems: ExamSetItemAsResponse[]) => {
    let result = null;

    if (!_.isEmpty(examSetItems) && !_.isEmpty(checkerArray)) {
      result = examSetItems.map((exam: any, index: number) => (
        <Grid key={`exam-${index}`} item xs={12} sm={6} md={4} lg={4}>
          <Card>
            <CardHeader title={exam.quiz_name} />
            <CardContent>
              <Stack spacing={3}>
                <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
                  <Stack>
                    <Typography variant="overline" color="secondary">
                      Start:
                    </Typography>
                    <Typography variant="subtitle2">
                      {exam.date_time_start
                        ? moment(exam.date_time_start).format('MMM DD, YYYY @HH:mm a')
                        : 'N/A'}
                    </Typography>
                  </Stack>
                  <Stack>
                    <Typography variant="overline" color="secondary">
                      End:
                    </Typography>
                    <Typography variant="subtitle2">
                      {exam.date_time_end
                        ? moment(exam.date_time_end).format('MMM DD, YYYY @HH:mm a')
                        : 'N/A'}
                    </Typography>
                  </Stack>
                </Stack>
              </Stack>
              <Button
                sx={{ mt: 2 }}
                color="primary"
                variant="contained"
                disabled={isTakeQuizButtonDisabled(exam)}
                onClick={() => {
                  navigate(`/exam/${exam.quiz_slug}/${exam.quiz_set_quiz_id}`);
                }}
              >
                {exam.quiz_is_done ? 'Test completed' : 'Take the test'}
              </Button>
            </CardContent>
          </Card>
        </Grid>
      ));
    }

    return result;
  };

  return <>{ShowExamSets()}</>;
}
