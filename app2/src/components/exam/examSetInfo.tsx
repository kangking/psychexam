import _ from 'lodash';
// react
import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// material
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { Container, Typography, Card, CardHeader, CardContent, Stack } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MChip, MButton, MIconButton } from '../@material-extend';
import { DoneReload } from '../../actions/ReloadActions';

// components
import ExamList from './examList';

import axios from '../../utils/axios';
import { ExamSetAsResponse } from '../../@types/exam';

export default function ExamSetInfo() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const examSetId: number = Number(useParams().quizSetId);
  const [examSet, setExamSet] = useState<ExamSetAsResponse | null>(null);
  // const [allowed, setAllowed] = useState(false);
  const [loading, setLoading] = useState(false);

  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  useEffect(() => {
    checkAllowedAccess();
  }, []);

  const checkAllowedAccess = async () => {
    setLoading(true);
    const response = await axios.get(`/api/allow-quiz-set?quizSetId=${examSetId}`);

    if (response.data.allowAccess) {
      fetchExamSet();
    } else {
      enqueueSnackbar(`You are not allowed to take this quiz set.`, {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    }
    setLoading(false);
  };

  const fetchExamSet = async () => {
    setLoading(true);
    const response = await axios.get('/api/client-quiz-set', { params: { quizSetId: examSetId } });
    if (response.data.success) {
      setExamSet(response.data.clientQuizSets.data[0]);
      enqueueSnackbar('Exam set fetched', {
        variant: 'success',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    } else {
      enqueueSnackbar(`Unable to fetch exam set: ${response.data.message}`, {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    }
    setLoading(false);
  };

  const ShowData = () => {
    if (examSet) {
      return (
        <>
          <ExamList />
        </>
      );
    }
    return (
      <>
        <Card>
          <CardContent style={{ justifyContent: 'center' }}>
            <Stack>
              <Typography variant="subtitle1" sx={{ mb: 2 }} style={{ textAlign: 'center' }}>
                You have requested access to this quiz set.
              </Typography>
              <Typography variant="subtitle1" sx={{ mb: 2 }} style={{ textAlign: 'center' }}>
                You are not allowed to view the quiz set at this moment.
              </Typography>
              <Typography variant="subtitle2" sx={{ mb: 2 }} style={{ textAlign: 'center' }}>
                This quiz set may also be empty.
              </Typography>
            </Stack>
            <Stack>
              <MButton
                style={{ justifyContent: 'center' }}
                variant="contained"
                color="primary"
                size="large"
                onClick={() => navigate('/exam-sets')}
              >
                Go back to quiz set list
              </MButton>
            </Stack>
          </CardContent>
        </Card>
      </>
    );
  };

  return <>{ShowData()}</>;
}
