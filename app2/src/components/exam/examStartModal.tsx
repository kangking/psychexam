import { sentenceCase } from 'change-case';
import parse, { HTMLReactParserOptions } from 'html-react-parser';
import { Element } from 'domhandler/lib/node';
import moment from 'moment';

import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';
// material
import { Typography, Button, Modal, Alert } from '@material-ui/core';
import Dialog, { DialogProps } from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
// components
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

// import useTimer from '../../hooks/useTimer';
import { setSentenceCase } from '../../utils/string';

import logoFile from '../assets/logo.png';
// import iqInstruction1 from '../../../public/static/exam-photos/questions/iq-instruction-1.png';
// import iqInstruction2 from '../../../public/static/exam-photos/questions/iq-instruction-2.png';

// ----------------------------------------------------------------------

const useStyles = makeStyles((theme: Theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: 480,
    zIndex: 2
  }
}));

interface Props {
  open: boolean;
  quizSlug?: string | null;
  children?: React.ReactNode;
  duration: number;
  isDone: boolean | null;
  instruction?: string | null;
  handleClose?: any;
  // buttonAlignment?: 'left' | 'center' | 'right';
}

const StartModal = (props: Props) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const examSetId: number = Number(useParams().quizSetId);
  const descriptionElementRef = React.useRef<HTMLElement>(null);

  const ShowMessage = () => {
    let message = '';
    if (props.isDone) {
      message = 'You already have taken this test.';
    } else {
      message =
        props.duration == 0
          ? "There's no time limit for this test."
          : `You have ${moment('2015-01-01')
              .startOf('day')
              .seconds(props.duration)
              .format('HH:mm:ss')} (hours: minutes: seconds) to finish the test.`;
    }
    return message;
  };

  const ShowBackButton = () => (
    <Button
      onClick={(e) => {
        navigate(`/exam-sets/${examSetId}`);
      }}
    >
      Go Back to Quiz List
    </Button>
  );

  // Not even needed
  const ShowInstruction = () => {
    const iqInstruction1 = 'asd';
    const iqInstruction2 = 'dfdfff';
    if (props.instruction) {
      if (props.quizSlug === 'intelligence-qoutient-test111') {
        const html = `<p style="margin-bottom: 8px;">This test consists of 48 problems that must be solved within a __ minute time limit. The items consist of visual patterns that do not require specialized knowledge or mathematical skills. Each correct answer gives one point, and all items are weighted equally. Once the time limit is up, the items will be gone and you shall be scored accordingly based on the number of correct answers. KINDLY EXAMINE THE EXAMPLES BELOW BEFORE YOU PROCEED:</p> </p>In this example, you see a grid with shapes that tend to follow a pattern. After examining the pattern, you can see that the 2ND  figure (Figure B)  is the one which follows the pattern and is marked accordingly.</p> <img id="iqImgInstruction1" src=${{
          iqInstruction1
        }} alt="instruc1"/> <p style="margin-bottom: 8px;">In the second example, you can also see that the 4th figure  (Figure D )is the one that differs from the pattern and is marked accordingly.</p> <img id="iqImgInstruction2" src=${{
          iqInstruction2
        }} alt="instruc2" /> <p style="margin-bottom: 8px;">The same rule apply to the items that follow. After you have figured out the relationship among the items, choose the one that varies from the pattern & mark it accordingly.</p>`;

        const options = {
          replace: (domNode: any) => {
            if (domNode) {
              if (domNode instanceof Element && domNode.attribs) {
                if (domNode.attribs.id === 'iqImgInstruction1') {
                  return (
                    <img
                      id="iqImgInstruction1"
                      src="/static/exam-photos/intelligence-qoutient-test/instructions/iq-instruction-1.PNG"
                    />
                  );
                }
                if (domNode.attribs.id === 'iqImgInstruction2') {
                  return (
                    <img
                      id="iqImgInstruction2"
                      src="/static/exam-photos/intelligence-qoutient-test/instructions/iq-instruction-2.PNG"
                    />
                  );
                }
                return domNode;
              }
              return domNode;
            }
            return <div>he</div>;
          }
        };

        return parse(html, options);
      }
      return parse(props.instruction);
    }
    return '';
  };

  return (
    <>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        scroll="body"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="scroll-dialog-title">Psychology Test</DialogTitle>
        <DialogContent dividers={false}>
          <DialogContentText
            id="scroll-dialog-description"
            ref={descriptionElementRef}
            tabIndex={-1}
          />
          <Typography variant="body1" paragraph id="transition-modal-title">
            {ShowInstruction()}
          </Typography>
          <Alert severity="info">{ShowMessage()}</Alert>
        </DialogContent>
        <DialogActions>
          {ShowBackButton()}
          {props.isDone ? null : props.children}
          {/* <Button onClick={props.handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={props.handleClose} color="primary">
            Subscribe
          </Button> */}
        </DialogActions>
      </Dialog>

      {/* {<Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.open}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={props.open}>
          <div className={classes.paper}>
            <Typography variant="h5" paragraph id="transition-modal-title">
              Psychology Test
            </Typography>
            <Typography variant="body1" paragraph id="transition-modal-title">
              {ShowInstruction()}
            </Typography>
            <Typography variant="h5" style={{ marginBottom: 12 }} id="transition-modal-title">
              {ShowMessage()}
            </Typography>
            {props.isDone ? ShowBackButton() : props.children}
          </div>
        </Fade>
      </Modal>} */}
    </>
  );
};

export default StartModal;
