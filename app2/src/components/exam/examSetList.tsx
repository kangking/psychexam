import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
// material
import { useSnackbar } from 'notistack';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import { Container, Grid, Typography, Card, CardHeader, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { MChip, MButton, MIconButton } from '../@material-extend';
import { DoneReload } from '../../actions/ReloadActions';

// components

import axios from '../../utils/axios';
import { ExamSetAsResponse } from '../../@types/exam';

export default function ExamSetInfo() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const examSetId: number = Number(useParams().examSetId);
  const [examSets, setExamSets] = useState<ExamSetAsResponse[]>([]);

  const reloadState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Reload')) {
      return state.Reload;
    }
    return null;
  });

  useEffect(() => {
    fetchExamList();
  }, []);

  const fetchExamList = async () => {
    const response = await axios.get('/api/quiz-sets');

    if (Object.prototype.hasOwnProperty.call(response, 'data')) {
      if (response.data.success) {
        setExamSets(response.data.quizSets.data);
        enqueueSnackbar('Exam set fetched', {
          variant: 'success',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
      } else {
        enqueueSnackbar(`Unable to fetch exam set: ${response.data.message}`, {
          variant: 'error',
          action: (key) => (
            <MIconButton size="small" onClick={() => closeSnackbar(key)}>
              <Icon icon={closeFill} />
            </MIconButton>
          )
        });
      }
    } else {
      enqueueSnackbar(`Unable to fetch exam set.`, {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    }
  };

  const showActions = (id: number) => (
    <MButton variant="contained" color="primary" onClick={handleClickOpen(id)}>
      Procceed
    </MButton>
  );

  const handleClickOpen = (id: number) => async (event: React.MouseEvent) => {
    const response = await axios.get(`/api/allow-quiz-set?quizSetId=${id}`);

    if (response.data.allowAccess) {
      navigate(`/exam-sets/${id}`);
    } else {
      enqueueSnackbar(`You are not allowed to take this quiz set.`, {
        variant: 'error',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
      enqueueSnackbar(`You have requested access to this exam set.`, {
        variant: 'info',
        action: (key) => (
          <MIconButton size="small" onClick={() => closeSnackbar(key)}>
            <Icon icon={closeFill} />
          </MIconButton>
        )
      });
    }
  };

  const ShowExamSets = () => {
    let elementExamSets = null;

    elementExamSets = examSets.map((examSet: ExamSetAsResponse, index: number) => (
      <Grid container spacing={2} key={`exam-set-${index}`}>
        <Grid item xs={12}>
          <Card sx={{ mb: 2 }}>
            <CardHeader
              sx={{ pb: 3 }}
              title={examSet.quiz_set_name ? examSet.quiz_set_name : 'No name'}
              action={showActions(examSet.quiz_set_id)}
            />
            {/* <CardContent>
              <Typography variant="overline">Date created:</Typography>
              <Typography variant="subtitle1">
                {moment(examSet.quiz_set_created_at).format('MMM d, YYYY')}
              </Typography>
            </CardContent> */}
          </Card>
        </Grid>
      </Grid>
    ));

    return elementExamSets;
  };

  return (
    <>
      <Typography variant="h6" paragraph>
        Exam Set List
      </Typography>
      {ShowExamSets()}
    </>
  );
}
