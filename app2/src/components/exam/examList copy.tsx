import _ from 'lodash';
// react
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
// material
import { Container, Typography, Box, Paper, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// components
import Page from '../Page';
import axios from '../../utils/axios';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(0),
    textAlign: 'left',
    color: theme.palette.text.secondary
  },
  examItem: {
    padding: theme.spacing(3)
  }
}));

export default function ExamList() {
  const classes = useStyles();
  const navigate = useNavigate();
  const [exams, setExams] = useState<any[]>([]);

  useEffect(() => {
    const initialize = async () => {
      const response = await axios.get('/api/quiz');
      setExams(response.data.quiz.data);
    };

    initialize();
  }, []);

  const ShowExamList = () => {
    let result = null;

    if (!_.isEmpty(exams)) {
      result = exams.map((exam: any, index: number) => (
        <Grid key={`exam-${index}`} item xs={12} sm={6} md={4} lg={4} className={classes.root}>
          <Paper variant="outlined" className={classes.examItem}>
            <Box width="100%">
              <Typography variant="h5" paragraph>
                {exam.quiz_name}
              </Typography>

              <Button
                color="primary"
                variant="contained"
                disabled={Boolean(exam.quiz_is_done)}
                onClick={() => {
                  navigate(`/exam/${exam.quiz_slug}`);
                }}
              >
                {exam.quiz_is_done ? 'Test completed' : 'Take the test'}
              </Button>
            </Box>
          </Paper>
        </Grid>
      ));
    }

    return result;
  };

  return (
    <Grid container spacing={2}>
      {ShowExamList()}
    </Grid>
  );
}
