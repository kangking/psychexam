import { sentenceCase } from 'change-case';
import React from 'react';
// material
import { Typography, Button, Modal } from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
// components
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

// import useTimer from '../../hooks/useTimer';
// ----------------------------------------------------------------------

const useStyles = makeStyles((theme: Theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: 480,
    zIndex: 2
  }
}));

interface Props {
  open: boolean;
  handleClose?: any;
}

const StartModal: React.FC<Props> = (props) => {
  const classes = useStyles();

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.open}
        onClose={props.handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={props.open}>
          <div className={classes.paper}>
            <Typography variant="h4" style={{ marginBottom: 12 }} id="transition-modal-title">
              Time is up.
            </Typography>
            <Typography variant="h4" style={{ marginBottom: 12 }} id="transition-modal-title">
              Please wait while your answers are being submitted.
            </Typography>
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default StartModal;
