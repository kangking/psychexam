// material
import {
  Container,
  Grid,
  Box,
  Typography,
  AppBar,
  Tabs,
  Tab,
  Stack,
  Card
} from '@material-ui/core';
// components
import _ from 'lodash';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { experimentalStyled as styled, makeStyles } from '@material-ui/core/styles';

import { useDispatch, useSelector } from 'react-redux';
import { GetResult, GetExam } from '../../actions/ExamActions';

// Hooks
// import useAuth from '../../hooks/useAuth';
// ----------------------------------------------------------------------

const style = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexWrap: 'wrap',
  '& > *': { mx: '8px !important' }
} as const;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  message: {
    lineHeight: '1.8rem',
    marginBottom: theme.spacing(2)
  }
}));

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
};

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

interface Props {
  exam?: any;
}

const ExamPage: React.FC<Props> = (props) => {
  // const { user } = useAuth();
  // const dispatch = useDispatch();
  // const navigate = useNavigate();
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  // const answersState = useSelector((state: any) => {
  //   if (Object.prototype.hasOwnProperty.call(state, 'AnswerSheet')) {
  //     return state.AnswerSheet;
  //   }
  //   return null;
  // });
  const resultState = useSelector((state: any) => {
    if (Object.prototype.hasOwnProperty.call(state, 'Result')) {
      return state.Result;
    }
    return null;
  });

  // React.useEffect(() => {
  //   if (answersState.done) dispatch(GetResult(answersState, user.clientId));
  //   else navigate(`/exam`);
  //   // dispatch(GetResult(answersStateB));
  // }, []);

  const handleChange = (event: any, newValue: any) => {
    setValue(newValue);
  };

  const importantConclusion = () => {
    if (resultState) {
      if (Object.prototype.hasOwnProperty.call(resultState.data, 'traits')) {
        const result = {
          strength: [],
          weakness: [],
          neutral: []
        };
        result.strength = resultState.data.traits.strengths.find((item: any) => {
          if (Object.prototype.hasOwnProperty.call(item, 'priority')) {
            return item.priority == true;
          }
          return item.priority == false;
        });
        result.weakness = resultState.data.traits.weaknesses.find((item: any) => {
          if (Object.prototype.hasOwnProperty.call(item, 'priority')) {
            return item.priority == true;
          }
          return item.priority == false;
        });
        result.neutral = resultState.data.traits.neutrals.find((item: any) => {
          if (Object.prototype.hasOwnProperty.call(item, 'priority')) {
            return item.priority == true;
          }
          return item.priority == false;
        });
        return (
          <>
            <h5>{result.strength}</h5>
            <h5>{result.weakness}</h5>
            <h5>{result.neutral}</h5>
          </>
        );
      }
      return null;
    }
    return null;
  };

  const tabPanels: any[] = [];
  const tabLabels: any[] = [];
  const interPretationItems: any = {};

  const BuildTab = (): void => {
    if (resultState) {
      if (Object.prototype.hasOwnProperty.call(resultState.data, 'interpretations')) {
        const { interpretations } = resultState.data;
        const categoryKeys = Object.keys(interpretations);

        categoryKeys.forEach((category: any, index: number) => {
          interPretationItems[category] = BuildInterpretationItems({
            category,
            interpretations: interpretations[category]
          });
          tabPanels.push(BuildTabPanels({ category, index }));
          tabLabels.push(BuildTabLabels({ category, index }));
        });
      }
    }
  };

  interface BuildTabOptions {
    category: any;
    index?: number;
    interpretations?: any;
  }

  const BuildInterpretationItems = ({ category, interpretations }: BuildTabOptions) => {
    let resultItems = null;
    if (Array.isArray(interpretations)) {
      resultItems = interpretations.map((item: any, index2: number) => {
        const { message } = item;
        return (
          <li key={`item${category}${index2}`}>
            <Typography className={classes.message}>{message}</Typography>
          </li>
        );
      });
      resultItems = <ul>{resultItems}</ul>;
    } else {
      const haha = interpretations.replace('\\n', '\n');
      resultItems = (
        <span>
          <Typography
            className={classes.message}
            variant="body1"
            dangerouslySetInnerHTML={{ __html: haha }}
          />
        </span>
      );
    }
    return resultItems;
  };

  const BuildTabPanels = ({ category, index }: BuildTabOptions) => (
    <TabPanel value={value} index={index} key={`panel${category}${index}`}>
      {interPretationItems[category]}
    </TabPanel>
  );

  const BuildTabLabels = ({ category, index }: BuildTabOptions) => (
    <Tab
      label={category.toLowerCase() === 'message' ? 'Conclusion' : category}
      {...a11yProps(index)}
      key={`label${category}${index}`}
    />
  );

  const ShowTabPanels = () => tabPanels;
  const ShowTabLabels = () => (
    <Tabs
      style={{ marginTop: 12, marginBottom: 12 }}
      value={value}
      onChange={handleChange}
      aria-label="trait tabs"
    >
      {tabLabels}
    </Tabs>
  );

  return (
    <>
      <Box sx={{ mb: 5 }}>
        <Typography variant="h3" align="center" paragraph>
          We have finished analyzing your result.
        </Typography>
        <Typography align="center" variant="subtitle1" sx={{ color: 'text.secondary' }}>
          Below is our interpretation based on your answers.
        </Typography>
      </Box>
      <Container maxWidth="lg">
        <Grid container justifyContent="center" spacing={3}>
          <Grid item xs={12} md={10}>
            <Card sx={style}>
              {BuildTab()}
              {importantConclusion()}
              {ShowTabLabels()}
              {ShowTabPanels()}
            </Card>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default ExamPage;
