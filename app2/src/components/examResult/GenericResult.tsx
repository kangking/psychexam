// material
import { Container, Typography, Grid, Card, Box, Stack } from '@material-ui/core';
import { useParams, useNavigate } from 'react-router-dom';
import { MChip, MButton, MIconButton } from '../@material-extend';
// components

interface Props {
  exam?: any;
}

const style = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexWrap: 'wrap',
  '& > *': { mx: '8px !important' }
} as const;

const GenericResult: React.FC<Props> = (props) => {
  const navigate = useNavigate();
  const examSetId: number = Number(useParams().quizSetId);

  return (
    <Container maxWidth="xl">
      <Container maxWidth="lg">
        <Grid container justifyContent="center" spacing={3}>
          <Grid item xs={12} md={10}>
            <Card sx={style}>
              <Box sx={{ mb: 5, mt: 5 }}>
                <Typography variant="h3" align="center">
                  Thank you for taking the test:
                </Typography>
                <Typography color="primary" variant="h3" align="center" paragraph>
                  {props.exam.data.quiz.data.quiz_name}
                </Typography>
                <Typography align="center" variant="subtitle1" sx={{ color: 'text.secondary' }}>
                  The results will be provided later.
                </Typography>
                <Stack sx={{ mt: 2 }}>
                  <MButton
                    style={{ justifyContent: 'center' }}
                    variant="contained"
                    color="primary"
                    size="large"
                    onClick={() => navigate(`/exam-sets/${examSetId}`)}
                  >
                    Go back
                  </MButton>
                </Stack>
              </Box>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Container>
  );
};

export default GenericResult;
