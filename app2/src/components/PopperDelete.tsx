import _ from 'lodash';
import moment from 'moment';
// react
import { useState, useEffect } from 'react';
// material
import { Typography, Paper, Grid, Button, Fade } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import Popper, { PopperPlacementType } from '@material-ui/core/Popper';

import axios from '../utils/axios';

interface Props {
  anchorEl: HTMLButtonElement | null;
  open: boolean;
  actions: React.ReactNode;
  message?: string;
}

export default function (props: Props) {
  const { anchorEl, open, actions, message } = props;

  return (
    <Popper open={open} anchorEl={anchorEl} placement="bottom-start" transition>
      {({ TransitionProps }) => (
        <Fade {...TransitionProps} timeout={350}>
          <Paper elevation={6} sx={{ p: 2 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} spacing={2}>
                <Typography variant="h5" paragraph>
                  Are you sure you want to delete?
                </Typography>
                <Typography paragraph>{message}</Typography>
              </Grid>
              <Grid item xs={12} spacing={2}>
                {actions}
              </Grid>
            </Grid>
          </Paper>
        </Fade>
      )}
    </Popper>
  );
}
