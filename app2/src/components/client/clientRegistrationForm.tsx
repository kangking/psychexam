import React from 'react';
import ReactDOM from 'react-dom';
import { useNavigate, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useSnackbar } from 'notistack';
import Button from '@material-ui/core/Button';
import {
  Stack,
  TextField,
  MenuItem,
  Checkbox,
  FormControlLabel,
  Alert,
  FormGroup,
  IconButton,
  InputAdornment
} from '@material-ui/core';

import { LoadingButton } from '@material-ui/lab';
import { Icon } from '@iconify/react';
import eyeFill from '@iconify/icons-eva/eye-fill';
import closeFill from '@iconify/icons-eva/close-fill';
import eyeOffFill from '@iconify/icons-eva/eye-off-fill';
import TermsModal from './termsModal';
import { MIconButton } from '../@material-extend';

// hooks
import useAuth from '../../hooks/useAuth';
import useIsMountedRef from '../../hooks/useIsMountedRef';

type InitialValues = {
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  gender: string;
  dateOfBirth: any;
  civilStatus: string;
  religion: string;
  homeAddress: string;
  mobileNumber: string;
  accessCode: string | null;
  password?: string;
  passwordConfirm?: string;
  afterSubmit?: string;
};

const genders = [
  {
    value: 'm',
    label: 'Male'
  },
  {
    value: 'f',
    label: 'Female'
  }
];

const civilStatuses = [
  {
    value: 'single',
    label: 'Single'
  },
  {
    value: 'married',
    label: 'Married'
  },
  {
    value: 'widowed',
    label: 'Widowed'
  }
];

const validationSchema = yup.object({
  firstName: yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('First name required'),
  lastName: yup.string().min(2, 'Too Short!').max(50, 'Too Long!').required('Last name required'),
  middleName: yup
    .string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Middle name required'),
  email: yup.string().email('Enter a valid email').required('Email is required'),
  gender: yup.string().required('Gender required'),
  dateOfBirth: yup.date().required('Birthdate is required'),
  civilStatus: yup.string().required('Status is required'),
  religion: yup.string().required('Please specify religion'),
  homeAddress: yup.string().required('Home address is required'),
  mobileNumber: yup.string().required('Mobile number is required'),
  accessCode: yup.string().required('Access code is required')
});

function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}

const ClientRegistrationForm = (props: any) => {
  const navigate = useNavigate();
  const query = useQuery();
  const [gender, setGender] = React.useState('m');
  const [checkedState, setCheckedState] = React.useState({ agreeToTerms: true });
  const [civilStatus, setCivilStatus] = React.useState('single');
  const [showPassword, setShowPassword] = React.useState(false);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { clientRegister } = useAuth();
  const isMountedRef = useIsMountedRef();
  const accessCodeParam = query.get('accessCode') ? query.get('accessCode') : 'xhn35xqwf';

  const formik = useFormik<InitialValues>({
    initialValues: {
      firstName: 'Cora',
      lastName: 'Lim',
      middleName: 'Corli',
      email: 'corlilim@email.com',
      gender: 'f',
      dateOfBirth: '',
      civilStatus: 'single',
      religion: 'Roman Catholic',
      homeAddress: 'Lugait Somewhere',
      mobileNumber: '091212412',
      accessCode: accessCodeParam,
      password: '',
      passwordConfirm: ''
    },
    validationSchema,
    onSubmit: async (values, { setErrors, setSubmitting }) => {
      if (checkedState.agreeToTerms === false) alert('must agree');
      else {
        try {
          await clientRegister(values);
          enqueueSnackbar('Register success', {
            variant: 'success',
            action: (key) => (
              <MIconButton size="small" onClick={() => closeSnackbar(key)}>
                <Icon icon={closeFill} />
              </MIconButton>
            )
          });
          if (isMountedRef.current) {
            setSubmitting(false);
          }
        } catch (error: any) {
          if (isMountedRef.current) {
            if (Object.prototype.hasOwnProperty.call(error, 'message')) {
              setErrors({ afterSubmit: error.message });
            }
            setSubmitting(false);
          }
        }
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, isValidating, getFieldProps } = formik;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;

    if (name == 'gender') {
      setGender(value);
    } else if (name == 'civilStatus') {
      setCivilStatus(value);
    }
  };

  const handleChangeAgreement = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, checked } = event.target;
    setCheckedState({ ...checkedState, [name]: checked });
  };

  React.useEffect(() => {
    formik.setFieldValue('accessCode', query.get('accessCode'));
  }, [query.get('accessCode')]);

  return (
    <div style={props.style}>
      {props.children}
      <form onSubmit={formik.handleSubmit}>
        <Stack spacing={3}>
          {errors.afterSubmit && <Alert severity="error">{errors.afterSubmit}</Alert>}
          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <TextField
              fullWidth
              label="Last name"
              {...formik.getFieldProps('lastName')}
              error={Boolean(formik.touched.lastName && formik.errors.lastName)}
              helperText={formik.touched.lastName && formik.errors.lastName}
            />
            <TextField
              fullWidth
              label="First name"
              {...formik.getFieldProps('firstName')}
              error={Boolean(formik.touched.firstName && formik.errors.firstName)}
              helperText={formik.touched.firstName && formik.errors.firstName}
            />
            <TextField
              fullWidth
              label="Middle name"
              {...formik.getFieldProps('middleName')}
              error={Boolean(formik.touched.middleName && formik.errors.middleName)}
              helperText={formik.touched.middleName && formik.errors.middleName}
            />
          </Stack>
          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <TextField
              fullWidth
              label="Email"
              {...formik.getFieldProps('email')}
              error={Boolean(formik.touched.email && formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
            />

            <TextField
              fullWidth
              label="Mobile Number"
              {...formik.getFieldProps('mobileNumber')}
              error={Boolean(formik.touched.mobileNumber && formik.errors.mobileNumber)}
              helperText={formik.touched.mobileNumber && formik.errors.mobileNumber}
            />
          </Stack>
          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <TextField
              fullWidth
              label="Birthday"
              type="date"
              {...formik.getFieldProps('dateOfBirth')}
              InputLabelProps={{
                shrink: true
              }}
              error={Boolean(formik.touched.dateOfBirth && formik.errors.dateOfBirth)}
              helperText={formik.touched.dateOfBirth && formik.errors.dateOfBirth}
            />
            <TextField
              fullWidth
              select
              label="Select gender"
              value={gender}
              name="gender"
              onChange={handleChange}
              error={Boolean(formik.touched.gender && formik.errors.gender)}
              helperText={formik.touched.gender && formik.errors.gender}
            >
              {genders.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Stack>
          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <TextField
              fullWidth
              select
              label="Select status"
              value={civilStatus}
              name="civilStatus"
              onChange={handleChange}
              error={Boolean(formik.touched.civilStatus && formik.errors.civilStatus)}
              helperText={formik.touched.civilStatus && formik.errors.civilStatus}
            >
              {civilStatuses.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>

            <TextField
              fullWidth
              label="Religion"
              {...formik.getFieldProps('religion')}
              error={Boolean(formik.touched.religion && formik.errors.religion)}
              helperText={formik.touched.religion && formik.errors.religion}
            />
          </Stack>
          <TextField
            fullWidth
            autoComplete="homeAddress"
            type="text"
            label="Home ddress"
            {...formik.getFieldProps('homeAddress')}
            error={Boolean(formik.touched.homeAddress && formik.errors.homeAddress)}
            helperText={formik.touched.homeAddress && formik.errors.homeAddress}
          />
          <TextField
            fullWidth
            label="Access Code"
            {...formik.getFieldProps('accessCode')}
            error={Boolean(formik.touched.accessCode && formik.errors.accessCode)}
            helperText={formik.touched.accessCode && formik.errors.accessCode}
          />
          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <TextField
              fullWidth
              label="Password"
              {...formik.getFieldProps('password')}
              type={showPassword ? 'text' : 'password'}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton edge="end" onClick={() => setShowPassword((prev) => !prev)}>
                      <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                    </IconButton>
                  </InputAdornment>
                )
              }}
              error={Boolean(formik.touched.password && formik.errors.password)}
              helperText={formik.touched.password && formik.errors.password}
            />
          </Stack>
          <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={checkedState.agreeToTerms}
                  onChange={handleChangeAgreement}
                  name="agreeToTerms"
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                />
              }
              label="Agree to the terms and conditions"
            />
            <TermsModal buttonAlignment="right" />
          </Stack>
          <LoadingButton
            fullWidth
            size="large"
            type="submit"
            variant="contained"
            loading={isSubmitting}
          >
            Register
          </LoadingButton>
        </Stack>
      </form>
    </div>
  );
};

export default ClientRegistrationForm;
