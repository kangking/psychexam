import { sentenceCase } from 'change-case';
import React from 'react';
// material
import { Typography, Button, Modal } from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
// components
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import TermsText from './termsAndCondition';
// ----------------------------------------------------------------------

const useStyles = makeStyles((theme: Theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxWidth: 480,
    zIndex: 2
  }
}));

interface Props {
  buttonAlignment?: 'left' | 'center' | 'right';
}

const TermsConditon: React.FC<Props> = (props) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Button color="primary" style={{ textAlign: props.buttonAlignment }} onClick={handleOpen}>
        Read Terms and Conditions
      </Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <Typography variant="h4" style={{ marginBottom: 12 }} id="transition-modal-title">
              Terms and Condition
            </Typography>
            <TermsText />
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default TermsConditon;
