import React, { useState, useEffect, useRef } from 'react';

const Timer = () => {
  const [duration, setDuration] = useState(1);
  const timer = useRef<any | null>(null);

  const hours = Math.floor(Number(duration) / 3600);
  const minutes = Math.floor(Number(duration) / 60);
  const seconds = Number(duration) % 60;
  const formattedDuration = `${hours} : ${minutes} : ${seconds}`;

  const resetTimer = () => {
    clearInterval(timer.current);
    timer.current = null;
    setDuration(0);
  };

  const stopTimer = () => {
    clearInterval(timer.current);
    timer.current = null;
    setDuration(0);
  };

  const startTimer = (seconds: number) => {
    resetTimer();
    setDuration(seconds);
    timer.current = setInterval(() => {
      setDuration((duration) => duration - 1);
    }, 1000);
  };

  return { duration, startTimer, resetTimer, stopTimer, setDuration };
};

export default Timer;
