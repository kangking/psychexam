import { useContext } from 'react';
import { LeadContext } from '../contexts/LeadContext';

// ----------------------------------------------------------------------

const useLead = () => {
  const context = useContext(LeadContext);

  if (!context) throw new Error('Lead context must be used inside LeadProvider');

  return context;
};

export default useLead;
