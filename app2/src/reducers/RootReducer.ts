import { combineReducers } from 'redux';
import Client from './ClientReducer';
import Exam from './ExamReducer';
import Question from './QuestionReducer';
import Result from './ResultReducer';
import AnswerSheet from './AnswerSheetReducer';
import Reload from './ReloadReducer';

const RootReducer = combineReducers({
  Exam,
  Question,
  Result,
  AnswerSheet,
  Client,
  Reload
});

export default RootReducer;
