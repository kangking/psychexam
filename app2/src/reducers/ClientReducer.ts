interface StateInterface {
  loading: boolean;
  data: any;
  errorMsg: string;
}

interface Client {
  firstName: string;
  lastName: string;
  email: string;
  gender: string;
  dateOfBirth: Date;
  civilStatus: number;
  religion: string;
  homeAddress: string;
  mobileNumber: string;
  accessCode: string;
}

interface ActionOptions {
  data: Client;
  type: string;
}

const initialState = {
  data: {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    gender: 'm',
    dateOfBirth: new Date(),
    civilStatus: 1,
    religion: '',
    homeAddress: '',
    mobileNumber: '',
    accessCode: ''
  },
  loading: false,
  errorMsg: ''
};

const ClientReducer = (state: StateInterface = initialState, action: ActionOptions) => {
  switch (action.type) {
    case 'CLIENT_SET': {
      return {
        ...state
      };
    }

    default:
      return state;
  }
};

export default ClientReducer;
