import { Exam, ExamSet } from '../@types/exam';

interface State {
  loading: boolean;
  data: ExamSet | null;
  errorMsg: string | null;
}

interface Action {
  type: string;
  payload: any;
  examSetId: number;
}

const initialState = {
  loading: false,
  data: null,
  errorMsg: ''
};

const ExamSetReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case 'EXAM_SET_LOADING': {
      return {
        ...state,
        loading: true
      };
    }

    case 'EXAM_SET_FAIL': {
      return {
        ...state,
        loading: false,
        errorMsg: 'unable to get exam set'
      };
    }

    case 'EXAM_SET_SUCCESS': {
      return {
        ...state,
        loading: false,
        data: {
          quizSetId: action.payload.quiz_set_id,
          quizSetName: action.payload.quiz_set_name,
          quizSetItems: [...action.payload.quizItems]
        }
      };
    }

    default: {
      return {
        ...state
      };
    }
  }
};

export default ExamSetReducer;
