const initialState = {
  answers: [],
  quizSlug: null, // quizId for api; not examId
  quizId: null,
  quizSetItemId: null,
  gender: 1,
  isDone: false
};

interface ActionOptions {
  type: string;
  answer: any;
  examId?: number;
  examSlug?: string;
  examSetItemId?: number;
  isDone?: boolean;
}

const AnswerSheetReducer = (state = initialState, action: ActionOptions) => {
  switch (action.type) {
    case 'ANSWER_ADD': {
      const list = [...state.answers, action.answer];
      return {
        ...state,
        answers: list
      };
    }
    case 'ANSWER_SHEET_RESET': {
      return {
        ...state,
        answers: [],
        quizId: action.examId,
        quizSlug: action.examSlug,
        quizSetItemId: action.examSetItemId,
        isDone: false
      };
    }
    case 'ANSWER_SHEET_DONE': {
      return {
        ...state,
        isDone: action.isDone
      };
    }

    default:
      return state;
  }
};

export default AnswerSheetReducer;
