const initialState = {
  loading: false,
  data: {}
};

const ResultReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'RESULT_LOADING': {
      return {
        ...state,
        loading: true,
        errorMsg: ''
      };
    }
    case 'RESULT_FAIL': {
      return {
        ...state,
        loading: false,
        errorMsg: 'unable to get quiz'
      };
    }
    case 'RESULT_SUCCESS': {
      return {
        ...state,
        loading: false,
        data: action.payload,
        errorMsg: ''
      };
    }

    default:
      return state;
  }
};

export default ResultReducer;
