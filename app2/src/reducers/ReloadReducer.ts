const initialState = {
  accessCodesTable: false,
  examSetItemsTable: false,
  requestExamSetTable: false,
  clientTable: false
};

const ReloadReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'RELOAD_ACCESS_CODES_TABLE': {
      return {
        ...state,
        accessCodesTable: action.payload.reload
      };
    }
    case 'RELOAD_DONE_ACCESS_CODES_TABLE': {
      return {
        ...state,
        accessCodesTable: action.payload.reload
      };
    }
    case 'RELOAD_EXAM_SET_ITEMS_TABLE': {
      return {
        ...state,
        examSetItemsTable: action.payload.reload
      };
    }

    /**
     * Exam Set items Table
     */
    case 'RELOAD_DONE_EXAM_SET_ITEMS_TABLE': {
      return {
        ...state,
        examSetItemsTable: action.payload.reload
      };
    }
    case 'RELOAD_REQUEST_EXAM_SET': {
      return {
        ...state,
        requestExamSetTable: action.payload.reload
      };
    }
    case 'RELOAD_DONE_REQUEST_EXAM_SET': {
      return {
        ...state,
        requestExamSetTable: action.payload.reload
      };
    }

    /**
     * Client Table
     */
    case 'RELOAD_CLIENT_TABLE': {
      return {
        ...state,
        clientTable: action.payload.reload
      };
    }
    case 'RELOAD_DONE_CLIENT_TABLE': {
      return {
        ...state,
        clientTable: action.payload.reload
      };
    }

    default:
      return state;
  }
};

export default ReloadReducer;
