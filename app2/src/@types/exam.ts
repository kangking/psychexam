export type ExamAsResponse = {
  quiz_id: number;
  quiz_name: string;
  quiz_instruction?: string;
  quiz_slug: string;
  quiz_duration: number;
  quiz_created_at: number;
  quiz_is_enabled: boolean | number;
};

export type ExamSetAsResponse = {
  quiz_set_id: number;
  quiz_set_name: string;
  quiz_set_created_at: Date;
};

export interface ExamSetItemAsResponse extends ExamAsResponse {
  quiz_set_quiz_id?: number;
  fk_quiz_id?: number;
  fk_quiz_set_id?: number;
  date_time_start?: Date;
  date_time_end?: Date;
}

export type Exam = {
  quizId: number;
  quizName: string;
  quizInstruction?: string;
  quizSlug: string;
  quizCreatedAt: number;
  quizIsEnabled: boolean | number;
};

export type ExamSet = {
  quizSetId: number;
  quizSetName: string;
  quizSetItems: Exam[];
};

// export interface ExamSetItemAsResponse {
//   fk_quiz_id: number;
//   fk_quiz_set_id: number;
//   date_time_start: Date;
//   date_time_end: Date;
// }

export interface ExamSetItemMeta {
  examName: string;
  examSetName: string;
}
