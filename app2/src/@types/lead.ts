export interface LeadSetAsResponse {
  lead_set_id: number;
  lead_set_name: string;
  lead_set_created_at: Date | string;
}

export interface LeadAsResponse {
  lead_set_name?: string; // WTF???
  lead_id: number;
  lead_email: string;
  lead_first_name?: string;
  lead_middle_name?: string;
  lead_last_name?: string;
  lead_created_at: Date | string;
}

export interface LeadSetForm {
  name: string;
}

export interface LeadForm {
  email: string;
  firstName: string;
  middleName: string;
  lastName: string;
}

export type Lead = {
  leadId: number;
  leadEmail: string;
  leadCreatedAt: number;
};

export type LeadSet = {
  leadSetId: number;
  leadSetName: string;
  leadSetItems: Lead[];
};

export interface LeadContextType {
  leadSetTable: LeadSetAsResponse[];
  leadSetAddForm: LeadSetForm | null;
  leadSet: LeadAsResponse;
  isLoadingLeadSetTable: boolean;
  leadTable: LeadAsResponse[];
  leadAddForm: LeadForm | null;
  isLoadingLeadTable: boolean;
  mailBlastResults: any[];
  getLeadSetTable: any;
  getLeadSet: any;
  getLeadTable: any;
  getLead: any;
  sendEmailBlast: any;
  reloadLeadTable: any;
  isReloadingLeadTable: boolean;
}
