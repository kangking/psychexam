export type Client = {
  client_last_name: string;
  client_first_name: string;
  client_middle_name: string;
  client_address_line: string;
  client_birthdate: string;
  client_civil_status: string;
  client_email: string;
  client_gender: string;
  client_id: number;
  client_mobile_number: number;
  client_is_deleted?: number | boolean;
  fk_access_code_id?: number | null;
  client_created_at: string | Date;
};
