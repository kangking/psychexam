interface OrderBy {
  column: string;
  direction: 'ASC' | 'DESC' | 'asc' | 'desc';
}

interface Filter {
  keyword: string;
  columns: string[];
}

export interface TableParams {
  orderBy: OrderBy;
  limit: number;
  offset: number;
  filter: Filter;
  pagination: number;
};
