import axios from '../utils/axios';

export const GetAnswerSheet = (examId: number) => async (dispatch: any) => {
  try {
    dispatch({
      type: 'ANSWER_SHEET_LOADING'
    });

    const response: any = await FetchAnswerSheet(examId);

    dispatch({
      type: 'ANSWER_SHEET_SUCCESS',
      payload: response.data,
      examId
    });
  } catch (e) {
    dispatch({
      type: 'ANSWER_SHEET_FAIL'
    });
  }
};

export const SetAnswer = (answer: any, questionId: number) => async (dispatch: any) => {
  dispatch({
    type: 'QUESTION_SET_ANSWER',
    answer,
    questionId
  });
  dispatch({
    type: 'ANSWER_ADD',
    answer: { value: answer, number: questionId }
  });
};

export const SetDone = (isDone: boolean) => (dispatch: any) => {
  dispatch({
    type: 'ANSWER_SHEET_DONE',
    isDone
  });
};

async function FetchAnswerSheet(examId: number) {
  const url = `/api/client-answer-sheet?quizId=${examId}`;

  const response = await axios.get(url);
  return response;
}
