import axios from '../utils/axios';

export const GetExam = (examSlug: string, examSetItemId: number) => async (dispatch: any) => {
  try {
    dispatch({
      type: 'ANSWER_SHEET_RESET',
      payload: { examSlug, examSetItemId },
      examSlug,
      examSetItemId
    });
    dispatch({
      type: 'EXAM_LOADING'
    });

    const response: any = await FetchQuiz(null, examSlug, examSetItemId);

    console.log(response);

    if (Object.hasOwnProperty.call(response.data, 'success') && response.data.success) {
      dispatch({
        type: 'EXAM_SUCCESS',
        payload: response.data,
        examSlug,
        examSetItemId
      });
    } else {
      dispatch({
        type: 'EXAM_FAIL'
      });
    }
  } catch (e) {
    dispatch({
      type: 'EXAM_FAIL'
    });
  }
};

export const GetResult = (answers: any) => async (dispatch: any) => {
  try {
    dispatch({
      type: 'RESULT_LOADING'
    });

    const payload = answers;
    const response = await axios.post(`/api/quiz-answers`, payload);

    // if (Object.hasOwnProperty.call(response, 'success') && response.data) {
    if (response.data.success) {
      dispatch({
        type: 'RESULT_SUCCESS',
        payload: response.data
      });
    } else {
      dispatch({
        type: 'RESULT_FAIL'
      });
    }
  } catch (e) {
    console.log(e);
    dispatch({
      type: 'RESULT_FAIL'
    });
  }
};

async function FetchQuiz(id: number | null, slug: string, setItemId: number) {
  const url = `/api/quiz?slug=${slug}&quizSetItemId=${setItemId}`;
  // const url = `http://psychapi.kosmokode.com/api/quiz?slug=${slug}`;
  // const url = `http://localhost:5000/api/quiz?slug=${slug}`;

  const response = await axios.get(url);
  // response.data.questions.forEach((question: any, index: number) => {
  //   question.answer = null;
  //   question.question_number2 = index + 1;
  // });

  return response;
}
