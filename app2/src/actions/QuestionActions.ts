export const IncrementQuestionIdx = () => async (dispatch: any) => {
  dispatch({
    type: 'QUESTION_CURRENT_IDX_INCREMENT'
  });
};

export const DecrementQuestionIdx = () => async (dispatch: any) => {
  dispatch({
    type: 'QUESTION_CURRENT_IDX_DECREMENT'
  });
};
