// RELOAD_ACCESS_CODES_TABLE' | RELOAD_DONE_ACCESS_CODES_TABLE
// RELOAD_EXAM_SET_ITEMS_TABLE' | RELOAD_DONE_EXAM_SET_ITEMS_TABLE

interface Action {
  type: string;
}

export const DoReload = (action: Action) => (dispatch: any) => {
  console.log('trying to reload', action.type);
  dispatch({
    type: action.type,
    payload: { reload: true }
  });
};

export const DoneReload = (action: Action) => (dispatch: any) => {
  dispatch({
    type: action.type,
    payload: { reload: false }
  });
};
