export const SetClient = (client: any) => async (dispatch: any) => {
  const payload = {
    gender: client.client_gender
  };

  dispatch({
    type: 'CLIENT_SET',
    payload
  });
};

export const ResetClient = (client: any) => async (dispatch: any) => {
  const payload = {
    gender: 'm'
  };

  dispatch({
    type: 'CLIENT_SET',
    payload
  });
};
