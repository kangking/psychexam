import store from '../Store';

const sentence = require('to-sentence-case');

const setPronounGender = (value: string, gender: string | null | undefined) => {
  let text = value.toLowerCase();
  const state = store.getState();

  if (gender === 'ff') {
    text = text
      .replaceAll('he/she', 'she')
      .replaceAll('his/her', 'her')
      .replaceAll('him/her', 'her')
      .replaceAll('himself/herself', 'herself');
    text = text
      .replaceAll('he', 'she')
      .replaceAll('his', 'her')
      .replaceAll('him', 'her')
      .replaceAll('himself', 'herself');
    return text;
  }

  if (gender === 'mm') {
    text = text
      .replaceAll('he/she', 'he')
      .replaceAll('his/her', 'his')
      .replaceAll('him/her', 'him')
      .replaceAll('himself/herself', 'himself');
    text = text
      .replaceAll('she', 'he')
      .replaceAll('her', 'his')
      .replaceAll('her', 'him')
      .replaceAll('herself', 'himself');
    return text;
  }

  return text;
};

const setSentenceCase = (text: string) => sentence(text);

export { setPronounGender, setSentenceCase };
